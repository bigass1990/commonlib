网址：
https://blog.csdn.net/DovSnier/article/details/107156612


本地仓库强制覆盖远端仓库，不管远端仓库是否有内容。


查看本地仓库是否与远端仓库关联。  
git branch -a  

 
// 如果有本地与远程关联，保留(多仓库关联)/不保留，看实际需要  
// 此处我选择不保留，即单仓库关联  
git remote remove origin
 
// 添加本地仓库与远程仓库关联  
git remote add origin XXX.git
 
// 强制推送到远程仓库，且覆盖远程代码库  
git push -f --set-upstream origin master:master