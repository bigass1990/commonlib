#pragma once

#define	FILE_UTILS_API __declspec(dllimport)

#include "../Core/FileLib/BinaryArchive.h"
#include "../Core/FileLib/FileUtils.h"

#ifndef NO_INCLUDE_PUGIXML
#include "../Core/FileLib/pugixml/pugixml.hpp"
#endif

#pragma comment(lib, "FileLib.lib")
#pragma message("    自动连接到 FileLib.lib")