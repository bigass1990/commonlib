#pragma once


#define __T(str) L##str
#define __TU8(str) u8##str
#define __TU16(str) u##str
#define __TU32(str) U##str

#ifndef SAFE_DELETE
#define SAFE_DELETE(p)       { if(p) { delete (p);     (p)=NULL; } }
#endif

#ifndef SAFE_DELETE_ARRAY
#define SAFE_DELETE_ARRAY(p) { if(p) { delete[] (p);   (p)=NULL; } }
#endif


#if defined(__cplusplus)
#if __cplusplus >= 201703L
namespace FileSystem = std::filesystem;
#else
#define _SILENCE_EXPERIMENTAL_FILESYSTEM_DEPRECATION_WARNING
#include <experimental/filesystem>
namespace FileSystem = std::experimental::filesystem::v1;
#endif
#endif

#include <cstdint>
#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <iosfwd>
#include <memory>
#include <vector>
#include <map>
#include <unordered_map>
#include <list>
#include <functional>