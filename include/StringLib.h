#pragma once

#define	STRING_LIB_API __declspec(dllimport)

#include "../Core/StringLib/CodeConvert.h"
#include "../Core/StringLib/StringUtils.h"
#include "../Core/StringLib/CodeEncoding.h"
#include "../Core/StringLib/EnumUtils.h"

#pragma comment(lib, "StringLib.lib")
#pragma message("    自动连接到 StringLib.lib")