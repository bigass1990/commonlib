#pragma once

#define	OpenGL_api __declspec(dllimport)

#include <..\\app\\OpenglLib\GLUtils.h>
#include <..\\app\\OpenglLib\Primitive.h>
#include <..\\app\\OpenglLib\ShaderProgram.h>
#include <..\\app\\OpenglLib\Texture.h>
#include <..\\app\\OpenglLib\VertexBufObj.h>

#pragma comment(lib, "OpenglLib.lib")
#pragma message("    自动连接到 OpenglLib.lib")