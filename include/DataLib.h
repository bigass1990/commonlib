#pragma once

#define	Data_API __declspec(dllimport)

#include "../Core/DataLib/signalSlot.h"
#include "../Core/DataLib/Singleton.h"
#include "../Core/DataLib/messageBus.h"
#include "../Core/DataLib/ThreadUtils.h"
#include "../Core/DataLib/ThreadAutoJoin.h"
#include "../Core/DataLib/joining_thread.h"

#pragma comment(lib, "DataLib.lib")
#pragma message("    自动连接到 DataLib.lib")