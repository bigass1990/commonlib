#pragma once

#ifdef WINDOWSLIB_EXPORTS
#define WINDOWSLIB_API __declspec(dllexport)
#else
#define WINDOWSLIB_API __declspec(dllimport)
#endif


#include "../Core/WindowsLib/RegisterOper.h"
#include "../Core/WindowsLib/Sys.h"
#include "../Core/WindowsLib/WinUtils.h"

#pragma comment(lib, "WindowsLib.lib")
#pragma message("    自动连接到 WindowsLib.lib")