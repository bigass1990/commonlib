#pragma once

#define	ENCRYPT_ALGORITHM_API __declspec(dllimport)

#include "../Core/EncryptAlgorithm/aes.h"
#include "../Core/EncryptAlgorithm/aes_encryptor.h"

#pragma comment(lib, "EncryptAlgorithm.lib")
#pragma message("    自动连接到 EncryptAlgorithm.lib")