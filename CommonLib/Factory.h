#pragma once
#include <map>
#include <string>
#include <functional>
#include <stdexcept>
#include <memory>

// https://github.com/qicosmos/cosmos/tree/master/self-register-factory

class Product
{
public:
	virtual ~Product() {}

	virtual void func() {}
};

#define REGISTER_PRODUCT_NAME(T) reg_product_##T##_
#define REGISTER_PRODUCT(T, key, ...) static Factory::Register_t<T> REGISTER_PRODUCT_NAME(T)(key, ##__VA_ARGS__);

// c++11 自动注册工厂类
class Factory
{
public:
	static Factory& get()
	{
		// c++11中，静态局部变量的初始化是线程安全的。
		static Factory instance;
		return instance;
	}

	template <class T>
	struct Register_t
	{
		Register_t(const std::string& key)
		{
			// c++11中，内部类可以通过外部类的实例访问外部类的私有成员。
			Factory::get().map_.emplace(key, [] {return new T(); });
		}

		template<typename ... Args>
		Register_t(const std::string& key, Args... args)
		{
			Factory::get().map_.emplace(key, [&] {return new T(args...); });
		}
	};

	static Product* create(const std::string& key)
	{
		if (map_.find(key) == map_.end())
		{
			throw std::invalid_argument("this key is not exist!");
		}

		return (Product*)(map_[key]());
	}

	static std::unique_ptr<Product> createUnique(const std::string& key)
	{
		return std::unique_ptr<Product>(create(key));
	}

	static std::shared_ptr<Product> createShare(const std::string& key)
	{
		return std::shared_ptr<Product>(create(key));
	}

private:

	Factory() {}
	Factory(const Factory&) = delete;
	Factory(Factory&&) = delete;

	static std::map<std::string, std::function<void*()>> map_;
};

