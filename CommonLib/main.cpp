#include "pch.h"


using namespace std;

struct TData
{
	TData() {}

	int8_t		i8;
	uint8_t		u8;
	int16_t		i16;
	uint16_t	u16;
	int32_t		i32;
	uint32_t	u32;
	int64_t		i64;
	uint64_t	u64;
	char		a ;
	wchar_t		b ;
	char as[5] = { "ab张" };
	wchar_t bs[5]{ L"张abc" };
	std::string str;
	std::wstring wstr;
	float f;
	double d;
	std::map<std::wstring, std::wstring> m_mapStr;
	std::vector<std::wstring> m_vecStr;


	virtual bool Do() { return true; }

	bool ReadData(FileUtils::CBinaryArchive* ar)
	{
		size_t size = 0;
		ar->ReadUInt8(u8);
		ar->ReadInt8(i8);
		ar->ReadUInt16(u16);
		ar->ReadInt16(i16);
		ar->ReadUInt32(u32);
		ar->ReadInt32(i32);
		ar->ReadInt64(i64);
		ar->ReadUInt64(u64);
		ar->ReadChar(a);
		ar->ReadChar(b);
		ar->ReadChar(as, size);
		ar->ReadChar(bs, size);
		ar->ReadString(str);
		ar->ReadWString(wstr);
		ar->ReadFloat(f);
		ar->ReadDouble(d);
		ar->ReadData(m_mapStr);
		ar->ReadData(m_vecStr);
		return true;
	}

	bool WriteData(const FileUtils::CBinaryArchive* ar) const
	{
		ar->WriteUInt8(u8);
		ar->WriteInt8(i8);
		ar->WriteUInt16(u16);
		ar->WriteInt16(i16);
		ar->WriteUInt32(u32);
		ar->WriteInt32(i32);
		ar->WriteInt64(i64);
		ar->WriteUInt64(u64);
		ar->WriteChar(a);
		ar->WriteChar(b);
		ar->WriteChar(as, 5);
		ar->WriteChar(bs, 5);
		ar->WriteString(str);
		ar->WriteWString(wstr);
		ar->WriteFloat(f);
		ar->WriteDouble(d);
		ar->WriteData(m_mapStr);
		ar->WriteData(m_vecStr);
		return true;
	}

	void init()
	{
		i8 = 8;
		u8 = 16;
		i16 = 16;
		u16 = 32;
		i32 = 32;
		u32 = 64;
		i64 = 64;
		u64 = 128;
		a = 'a';
		b = L'b';
		str = { "asdf 比目鱼" };
		 wstr = { L"opq 比目鱼" };
		 f = -12.5;
		 d = 54.3;
		 m_mapStr.insert(std::make_pair<wstring, wstring>(L"1", L"2"));
		 m_mapStr.insert(std::make_pair<wstring, wstring>(L"3", L"4"));
		 m_mapStr.insert(std::make_pair<wstring, wstring>(L"5", L"6"));
		 m_mapStr.insert(std::make_pair<wstring, wstring>(L"7", L"8"));
		 m_vecStr.push_back(L"1");
		 m_vecStr.push_back(L"2");
		 m_vecStr.push_back(L"3");
		 m_vecStr.push_back(L"4");
	}
};

int test_archive_write()
{
	TData dt;
	dt.init();
	FileUtils::CBinaryArchive ac;
	/*ac.WriteInt8(dt.i8);
	ac.WriteUInt8(dt.u8);
	ac.WriteInt16(dt.i16);
	ac.WriteUInt16(dt.u16);
	ac.WriteInt32(dt.i32);
	ac.WriteUInt32(dt.u32);
	ac.WriteInt64(dt.i64);
	ac.WriteUInt64(dt.u64);
	ac.WriteChar(dt.a);
	ac.WriteChar(dt.b);
	ac.WriteString(dt.str);
	ac.WriteWString(dt.wstr);
	ac.WriteFloat(dt.f);
	ac.WriteDouble(dt.d);
	ac.WriteChar(dt.as, 5);
	ac.WriteChar(dt.bs, 5);*/

	//dt.WriteData(&ac);
	ac.WriteData<TData>(dt);
	std::ofstream fs("d:\\a", std::ios_base::binary);
	fs << ac.ToBuf();
	fs.close();
	return 0;
}

int test_archive_read()
{
	FileUtils::CBinaryArchive ac;
	{
		std::ifstream fs("d:\\a", std::ios_base::binary);

		ac.FromBuf(fs.rdbuf());
		fs.close();
	}

	TData dt;
	
	/*ac.ReadInt8(dt.i8);
	ac.ReadUInt8(dt.u8);
	ac.ReadInt16(dt.i16);
	ac.ReadUInt16(dt.u16);
	ac.ReadInt32(dt.i32);
	ac.ReadUInt32(dt.u32);
	ac.ReadInt64(dt.i64);
	ac.ReadUInt64(dt.u64);
	ac.ReadChar(dt.a);
	ac.ReadChar(dt.b);
	ac.ReadString(dt.str);
	ac.ReadWString(dt.wstr);
	ac.ReadFloat(dt.f);
	ac.ReadDouble(dt.d);
	size_t len = 0;
	ac.ReadChar(dt.as, len);
	ac.ReadChar(dt.bs, len);*/
	
	//dt.ReadData(&ac);
	ac.ReadData<TData>(dt);
	return 0;
}

int test_register()
{
	// 验证失败 2021.3.22
	sys::RegCurrentUser(L"c:\\window\\notepad.exe", L"notepad");
	return 0;
}

int test_Trim()
{
	std::string a("abc");
	std::string b = StringUtils::LTrim(a);
	std::string c = StringUtils::RTrim(a);
	std::string d = StringUtils::Trim(a);
	cout << b << " " << b.size() << endl;
	cout << c << " " << c.size() << endl;
	cout << d << " " << d.size() << endl;
	return 1;
}

int test_autoStream()
{
	AutoClosePtr<std::ifstream> stream(std::ifstream("c:\\a.txt"));
	if (stream->is_open())
	{
	}

	return 0;
}

int test_strFormat()
{
	cout << str_format("中国，世界。abc:%f", 10.2) << endl;
 	std::wstring s = wstr_format(L"中国，世界。cdb:%f", 20.4);
 	wcout.imbue(std::locale("chs"));
 	
	wcout << L"你好:" << s ;
 	return 0;
}

int test_strConvert()
{
	std::wstring wstr = CodeConvert::ansi_to_wstr("比目鱼bimuyu");
	if (wstr.empty())
	{
		return 0;
	}
	return 1;
}

int test_FormatDouble()
{
	std::wcout << StringUtils::FromDoubleW(123.456, 4);
	return 0;
}

int test_strIsNum()
{
	std::cout << "123.123 ：" << StringUtils::isnum(L"123.123") << std::endl;
	std::cout << "123 ：" << StringUtils::isnum(L"123") << std::endl;
	std::cout << "123.a ：" << StringUtils::isnum(L"123.a") << std::endl;
	std::cout << "123.f ：" << StringUtils::isnum(L"123.f") << std::endl;
	std::cout << "123f ：" << StringUtils::isnum(L"123f") << std::endl;
	std::cout << "ab ：" << StringUtils::isnum(L"ab") << std::endl;
	std::cout << "1e-1 ：" << StringUtils::isnum(L"1e-1") << std::endl;
	std::cout << "-123.123 ：" << StringUtils::isnum(L"-123.123") << std::endl;

	return 0;
}

int test_EnumToInt()
{
	enum class color : char
	{
		red=100,
		green,
		black,
	};

	color col = color::red;
	int c = EnumToInt(col);

	std::cout << "color: " << c << std::endl;
	return 0;
}

int test_guid()
{
	size_t count = 10000000;
	std::vector<std::string> arr(count);
	bool bcontain = false;
	for (size_t i = 0; i < count; ++i)
	{
		arr[i] = MathUtils::uuid();
	}
	
	if (MathUtils::GetDuplicatedData(arr) == arr.end())
	{
		std::cout << "测试：" << count << " 次，未有重复！" << std::endl;
	}
	else
	{
		std::cout << "测试：" << count << " 次，存在有重复！" << std::endl;
	}
	return 0;
}

int test_DBG_HELP()
{
	std::string str = WinUtils::GetUnDecorateSymbolName(typeid(TData).name());
	std::cout << "dbg help：" << typeid(TData).name() << std::endl;
	std::cout << "dbg help：" << str << std::endl;
	std::cout << "dbg help：" << typeid(std::string).name() << std::endl;
	std::cout << "dbg help：" << WinUtils::GetUnDecorateSymbolName(typeid(std::string).name()) << std::endl;
	
	return 0;
}

void func_testSlot(const std::string& str)
{
	std::cout << "func_testSlot" << std::endl;
}
int test_signalSlot()
{
	testSignal tsignal;
	testSlot tslot;

	connect(&tsignal, m_s1, std::bind(&testSlot::func1, &tslot));
	connect(&tsignal, m_s2, std::bind(&testSlot::func2, &tslot, std::placeholders::_1));
	connect(&tsignal, m_s3, std::bind(&testSlot::func3, &tslot, std::placeholders::_1, std::placeholders::_2));
	connect(&tsignal, m_s2, std::bind(func_testSlot, std::placeholders::_1));
	connect(&tsignal, m_s2, [](const std::string& str)
		{
			std::cout << "lambda str:" << str << std::endl;
		});

	tsignal.start();
	return 0;
}


int main()
{
	return test_signalSlot();
	return test_DBG_HELP();
	return test_guid();
	//return test_EnumToInt();
	//return test_strIsNum();
	//return test_FormatDouble();
	return test_archive_read();
	return test_archive_write();

	return test_register();

	return test_Trim();

	//return test_strFormat();

	std::string file("d:\\a\\a.txt");
	Encoding encod = FileUtils::GetFileEncoding(file);
	return 0;
	std::string xmlPath = FileUtils::GetRootDir() + "测试 1.xml";
	pugi::xml_document xmlDoc;
	pugi::xml_parse_result rel = xmlDoc.load_file(xmlPath.c_str());
	return 0;

	std::shared_ptr<MyHello> pMyHello = CreateShared<MyHello>("MyHello", int32_t(20));

	std::cout << FileUtils::GetRootDir() << std::endl;
	std::wcout << FileUtils::GetRootDirW() << std::endl;
	return 0;

	unsigned char key[] = "db069397ce8cfafa";
	AesEncryptor aes(key);

	aes.DecryptTxtFile("C:\\vadio1\\d470268313fc4fccdf4688ebc34a49adc55f24cc\\Y2hlbmppbmdjb25n0",
		"C:\\vadio1\\a.mp4");
}