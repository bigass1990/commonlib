#pragma once
#include <memory>
#include <string>
#include <functional>
#include <unordered_map>
#include <stdint.h>

// C++反射机制：可变参数模板实现C++反射
// https://www.cnblogs.com/bwar/p/9304261.html

class MyObject : public std::enable_shared_from_this<MyObject>
{
public:
	virtual ~MyObject() {}
protected:
private:
};

template<typename ...Targs>
class MyFactory
{
public:

	static MyFactory* Instance()
	{
		static MyFactory<Targs...> inst;
		return &inst;
	}

	virtual ~MyFactory() {}

	// 将“实例创建方法（DynamicCreator的CreateObject方法）”注册到ActorFactory，
	// 注册的同时赋予这个方法一个名字“类名”，后续可以通过“类名”获得该类的“实例创建方法”。
	// 这个实例创建方法实质上是个函数指针，在C++11里std::function的可读性比函数指针更好，所以用了std::function。
	bool Regist(const std::string& strTypeName, std::function<MyObject* (Targs&&... args)> pFunc)
	{
		if (pFunc == nullptr)
		{
			return false;
		}
		auto rel = m_mapNameToCreateFunc.insert(std::make_pair(strTypeName, pFunc));
		return rel.second;
	}

	// 传入“类名”和参数创建类实例，方法内部通过“类名”从m_mapCreateFunction获得了
	// 对应的“实例创建方法（DynamicCreator的CreateObject方法）”完成实例创建操作。
	MyObject* Create(const std::string& strTypeName, Targs... args)
	{
		auto iter = m_mapNameToCreateFunc.find(strTypeName);
		if (iter == m_mapNameToCreateFunc.end())
		{
			return nullptr;
		}

		return (iter->second(std::forward<Targs>(args)...));
	}

private:
	MyFactory(){}
	std::unordered_map<std::string, std::function<MyObject* (Targs&&...)>> m_mapNameToCreateFunc;
};

/// <summary>
/// 动态创建的类的基类。
/// </summary>
/// <typeparam name="T">要动态创建的类</typeparam>
/// <typeparam name="...Targs">要动态创建的类的构造函数参数列表</typeparam>
template<typename T, typename ...Targs>
class DynamicCreator
{
public:
	struct Register
	{
		Register()
		{
#ifdef __GNUC__
			const char* szDemangleName = abi::__cxz_demangle(typeid(T).name(), nullptr, nullptr, nullptr);
			MyFactory<Targs...>::Instance()->Regist(szDemangleName, CreateObject);
			free(szDemangleName);
#elif _MSC_VER
			//注意：这里不同编译器typeid(T).name()返回的字符串不一样，需要针对编译器写对应的实现
			// 如果表达式的类型是类类型且至少包含有一个虚函数，则typeid操作符返回表达式的动态类型，
			// 需要在运行时计算；否则，typeid操作符返回表达式的静态类型，在编译时就可以计算。
			std::string name(typeid(T).name());
			std::string className = name.substr(name.find_first_not_of("class "));
			MyFactory<Targs...>::Instance()->Regist(className.c_str(), CreateObject);
#else
			szDemangleName = typeid(T).name();
#error "未实现的编译器类型"
#endif
		}

		inline void do_nothing() const {}
	};

	DynamicCreator()
	{
		// 这里的函数调用虽无实际内容，却是在调用动态创建函数前完成m_regisger实例创建的关键
		m_regisger.do_nothing();
	}

	virtual ~DynamicCreator() {}

private:
	static T* CreateObject(Targs&&... args)
	{
		T* pT = nullptr;
		try
		{
			pT = new T(std::forward<Targs>(args)...);
		}
		catch (std::bad_alloc&)
		{
			return nullptr;
		}
		catch (const std::exception&)
		{
			return nullptr;
		}

		return pT;
	}

	static Register m_regisger;
};

template<typename T, typename ...Targs>
typename DynamicCreator<T, Targs...>::Register DynamicCreator<T, Targs...>::m_regisger;

// 动态创建实例样例 CmdHelllo
class MyHello : public MyObject, public DynamicCreator<MyHello, int32_t>
{
public:
	MyHello(int32_t cmd) {}

	virtual ~MyHello() {}
	virtual bool Init() { return false; }

protected:
private:
};

template<typename ...Targs>
MyObject* MakeSharedObject(const std::string& strTypeName, Targs... args)
{
	MyObject* pObj = MyFactory<Targs...>::Instance()->Create(strTypeName, std::forward<Targs>(args)...);
	return pObj;
}

//std::shared_ptr<MyObject> pobj(MakeSharedObject("MyHello", int32_t(20)));

//std::shared_ptr<MyHello> pHello(dynamic_cast<MyHello*>(MakeSharedObject("MyHello", int32_t(20))));

template<typename T, typename...Targs>
std::shared_ptr<T> CreateShared(const std::string& strTypeName, Targs... args)
{
	return std::shared_ptr<T>(dynamic_cast<T*>(MyFactory<Targs...>::Instance()->Create(strTypeName, std::forward<Targs>(args)...)));
}

