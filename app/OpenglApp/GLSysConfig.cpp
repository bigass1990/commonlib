#include "pch.h"
#include "GLSysConfig.h"

static std::shared_ptr<pugi::xml_document> s_xmlDoc = nullptr;

GLSysConfig::GLSysConfig()
{
	s_xmlDoc = std::make_shared<pugi::xml_document>();
	std::wstring xmlFile(std::move(FileUtils::GetRootDirW()));
	xmlFile += L"glConfig\\SysConfig.xml";
	s_xmlDoc->load_file(xmlFile.c_str());
}

GLSysConfig* GLSysConfig::Instance()
{
	static GLSysConfig cfg;
	return &cfg;
}

GLSysConfig::~GLSysConfig()
{
	if (s_xmlDoc)
	{
		s_xmlDoc->reset();
	}

}

uint GLSysConfig::GetMajorVer() const
{
	auto sys = s_xmlDoc->child("Opengl").child("sys");
	uint major = sys.attribute("major").as_uint();
	return major;
}

uint GLSysConfig::GetMinorVer() const
{
	auto sys = s_xmlDoc->child("Opengl").child("sys");
	uint minor = sys.attribute("minor").as_uint();
	return minor;
}

uint GLSysConfig::GetWndWidth() const
{
	auto sys = s_xmlDoc->child("Opengl").child("mainwindow");
	uint height = sys.attribute("height").as_uint();
	return height;
}

uint GLSysConfig::GetWndHeight() const
{
	auto sys = s_xmlDoc->child("Opengl").child("mainwindow");
	uint width = sys.attribute("width").as_uint();
	return width;
}

std::string GLSysConfig::GetWndTitle() const
{
	auto sys = s_xmlDoc->child("Opengl").child("mainwindow");
	std::string title = sys.attribute("title").as_string();
	return title;
}
