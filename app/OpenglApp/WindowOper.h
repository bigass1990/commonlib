#pragma once


class WindowOper
{
public:
	WindowOper(GLFWwindow* pWnd);

public:

	void ProcessUI();

public:

	// whenever the window size changed (by OS or user resize) this callback function executes
	static void WndSizeCallback(GLFWwindow* window, int width, int height);

private:
	GLFWwindow* m_pWnd;
};

