#pragma once

class GraphOper
{
public:
	GraphOper(GLFWwindow* pWnd);

public:

	void InitVertex();

	void Draw(double currentTime);


private:
	GLFWwindow* m_pWnd;
};

