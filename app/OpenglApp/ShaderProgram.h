#pragma once

/// <summary>
/// 着色器类型
/// </summary>
enum class ShaderType : unsigned int
{
	Vertex = GL_VERTEX_SHADER,
	Fragment = GL_FRAGMENT_SHADER,
	TessControl = GL_TESS_CONTROL_SHADER,
	TessEvaluation = GL_TESS_EVALUATION_SHADER,
	Geometry = GL_GEOMETRY_SHADER,
	Compute = GL_COMPUTE_SHADER,
};

class ShaderObj
{
public:

	/// <summary>
	/// 编译着色器源码。
	/// </summary>
	/// <param name="shaderObjId">着色器对象ID</param>
	/// <param name="type">着色器类型</param>
	/// <param name="shaderSource">着色器字符串</param>
	/// <param name="pErrorLog">编译日志</param>
	/// <returns>编译是否成功</returns>
	static bool CompileShaderSource(unsigned int shaderObjId, ShaderType type, const std::string& shaderSource,
		std::string* pErrorLog = nullptr);

	/// <summary>
	/// 生成着色器对象，并读取编译代码。
	/// </summary>
	/// <param name="type"></param>
	/// <param name="shaderFile"></param>
	/// <param name="pErrorLog"></param>
	/// <returns>返回着色器对象handleID。如果为0，则发生错误。</returns>
	static unsigned int GetCompiledShader(ShaderType type, const std::string& shaderFile,
		std::string* pErrorLog = nullptr);

public:

	// 判断是否是一个着色器对象。
	static bool IsValid(GLuint shaderHandle);

	/// 根据着色器句柄，获取着色器类型
	static ShaderType GetType(GLuint shaderHandle);

	/// 返回着色器是否被删除。
	static bool IsDeleted(GLuint shaderHandle);

	// 删除着色器对象。如果当前已经链接到一个或多个激活的程序上，
	// 它将被标识为“可删除”，当对应着色器程序不再使用，就自动删除。
	static void Delete(GLuint shaderHandle);
	static void Delete(std::vector<GLuint> shaderIds);

	/// 返回是否编译成功。
	static bool IsCompiled(GLuint shaderHandle);

	/// 返回日志长度。
	static int GetLogLength(GLuint shaderHandle);

	/// 获取日志。
	static bool GetLogInfo(GLuint shaderHandle, std::string& logInfo);

	/// 返回源码长度。
	static int GetSourceLength(GLuint shaderHandle);
};

/// <summary>
/// 着色器程序：
/// 1. 创建一个着色器对象。
/// 2. 将着色器源代码编译为对象。
/// 3. 验证着色器的编译是否成功。
/// 4. 创建一个着色器程序。
/// 5. 将着色器对象关联到着色器程序。
/// 6. 连接着色器。
/// 7. 判断着色器连接过程是否成功。
/// 8. 使用着色器来处理顶点和片元。
/// </summary>
class ShaderProgram
{
public:
	/// <summary>
	/// 连接着色器程序。
	/// </summary>
	/// <param name="programId">着色器程序ID</param>
	/// <param name="shaderObjId">着色器对象ID</param>
	/// <param name="pErrorLog">连接日志</param>
	/// <returns>连接是否成功</returns>
	static bool LinkShaderToProgram(unsigned int programId, const std::vector<unsigned int>& shaderObjIds, std::string* pErrorLog = nullptr);

	static unsigned int Get(const std::vector<unsigned int>& shaderObjIds, std::string* pErrorLog = nullptr);

	static void SetCurrent(unsigned int programHandle);

	static void ClearCurrent();

public:

	// 判断是否是一个着色器程序。
	static bool IsValid(GLuint programHandle);

	// 返回是否已删除。
	static bool IsDeleted(GLuint programHandle);

	// 删除着色器程序。如果程序正在被某个环境使用，等它空闲再删除。
	static void Delete(GLuint programHandle);

	/// 返回是否编译成功。
	static bool IsLinked(GLuint programHandle);

	/// 返回日志长度。
	static int GetLogLength(GLuint programHandle);

	// 获取日志。
	static int GetLogInfo(GLuint programHandle, std::string& logInfo);

	// 返回附加到程序的着色器数量。
	static int GetAttechedShaderSize(GLuint programHandle);

	// 返回活动属性的最大长度。
	static int GetActiveAttirbuteMaxLength(GLuint programHandle);

	// 返回活动属性的数量。
	static int GetActiveAttirbuteSize(GLuint programHandle);

	// 返回活动状态的统一变量的数量。
	static int GetAcviteUniformSize(GLuint programHandle);

	// 返回活动状态的统一变量的名称最大长度。
	static int GetAcviteUniformMaxLength(GLuint programHandle);

protected:
	

	
};

