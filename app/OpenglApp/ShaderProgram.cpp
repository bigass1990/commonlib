#include "pch.h"
#include "ShaderProgram.h"
#include "GLUtils.h"


bool ShaderObj::CompileShaderSource(unsigned int objId,
	ShaderType type, const std::string& shaderSource,
	std::string* pErrorLog/* = nullptr*/)
{
	const char* buf = &*shaderSource.begin();
	size_t size = shaderSource.size();
	glShaderSource(objId, 1, &buf, (GLint*)&size);

	glCompileShader(objId);

	if (pErrorLog)
	{
		GLint logSize = 0;
		glGetShaderiv(objId, GL_INFO_LOG_LENGTH, &logSize);
		if (logSize > 0)
		{
			pErrorLog->reserve(logSize);
			glGetShaderInfoLog(objId, logSize, &logSize, &*pErrorLog->begin());
		}
	}

	GLint cmpl = 0;
	glGetShaderiv(objId, GL_COMPILE_STATUS, &cmpl);
	if (cmpl != GL_TRUE)
	{
		return false;
	}

	return true;
}


unsigned int ShaderObj::GetCompiledShader(ShaderType type, const std::string& shaderFile, std::string* pErrorLog /*= nullptr*/)
{
	GLenum shadertype = static_cast<GLenum>(type);
	GLuint objId = glCreateShader(shadertype); // 返回非零值，如果为0则说明发生错误。
	if (objId == 0)
		return 0;

	std::string source;
	if (!GLUtils::ReadShaderSource(shaderFile.c_str(), source)
		|| !CompileShaderSource(objId, type, source, pErrorLog))
		return 0;

	return objId;
}

bool ShaderObj::IsValid(GLuint shaderHandle)
{
	return glIsShader(shaderHandle);
}

ShaderType ShaderObj::GetType(GLuint shaderHandle)
{
	GLint type;
	glGetShaderiv(shaderHandle, GL_SHADER_TYPE, &type);

	return static_cast<ShaderType>(type);
}

bool ShaderObj::IsDeleted(GLuint shaderHandle)
{
	GLint value;
	glGetShaderiv(shaderHandle, GL_DELETE_STATUS, &value);
	return value == GL_TRUE;
}

void ShaderObj::Delete(GLuint shaderHandle)
{
	glDeleteShader(shaderHandle);
}

void ShaderObj::Delete(std::vector<GLuint> shaderIds)
{
	for (GLuint id : shaderIds)
	{
		glDeleteShader(id);
	}
}

bool ShaderObj::IsCompiled(GLuint shaderHandle)
{
	GLint value;
	glGetShaderiv(shaderHandle, GL_COMPILE_STATUS, &value);
	return value == GL_TRUE;
}

int ShaderObj::GetLogLength(GLuint shaderHandle)
{
	GLint value;
	glGetShaderiv(shaderHandle, GL_INFO_LOG_LENGTH, &value);
	return value;
}

bool ShaderObj::GetLogInfo(GLuint shaderHandle, std::string& logInfo)
{
	GLint length = GetLogLength(shaderHandle);
	if (length > 0)
	{
		logInfo.assign(length, 0);
		glGetShaderInfoLog(shaderHandle, length, NULL, &*logInfo.begin());
		return true;
	}
	return false;
}

int ShaderObj::GetSourceLength(GLuint shaderHandle)
{
	GLint value;
	glGetShaderiv(shaderHandle, GL_SHADER_SOURCE_LENGTH, &value);
	return value;
}

///////////////////////////////////////////////////////////////////

bool ShaderProgram::IsDeleted(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_DELETE_STATUS, &value);

	return value == GL_TRUE;
}

void ShaderProgram::Delete(GLuint programHandle)
{
	glDeleteProgram(programHandle);
}

bool ShaderProgram::IsLinked(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_LINK_STATUS, &value);

	return value == GL_TRUE;
}

int ShaderProgram::GetLogLength(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_INFO_LOG_LENGTH, &value);

	return value;

}

int ShaderProgram::GetLogInfo(GLuint programHandle, std::string& logInfo)
{
	GLint length = GetLogLength(programHandle);
	if (length > 0)
	{
		logInfo.assign(length, 0);
		glGetProgramInfoLog(programHandle, length, NULL, &*logInfo.begin());
		return true;
	}
	return false;
}

int ShaderProgram::GetAttechedShaderSize(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_ATTACHED_SHADERS, &value);

	return value;
}

int ShaderProgram::GetActiveAttirbuteMaxLength(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &value);

	return value;
}

int ShaderProgram::GetActiveAttirbuteSize(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_ACTIVE_ATTRIBUTES, &value);

	return value;
}

int ShaderProgram::GetAcviteUniformSize(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_ACTIVE_UNIFORMS, &value);

	return value;
}

int ShaderProgram::GetAcviteUniformMaxLength(GLuint programHandle)
{
	GLint value = 0;
	glGetProgramiv(programHandle, GL_ACTIVE_UNIFORM_MAX_LENGTH, &value);

	return value;
}

bool ShaderProgram::LinkShaderToProgram(unsigned int progId, 
	const std::vector<unsigned int>& shaderObjIds,
	std::string* pErrorLog/* = nullptr*/)
{
// 	GLuint progId = glCreateProgram();
// 	if (progId == 0)
// 	{
// 		return 0;
// 	}

	for (auto iter = shaderObjIds.cbegin(); iter != shaderObjIds.end(); ++iter)
	{
		glAttachShader(progId, *iter);
	}
	
	glLinkProgram(progId);

	if (pErrorLog)
	{
		GLint logSize = 0;
		glGetShaderiv(progId, GL_INFO_LOG_LENGTH, &logSize);
		if (logSize > 0)
		{
			pErrorLog->reserve(logSize);
			glGetProgramInfoLog(progId, logSize, &logSize, &*pErrorLog->begin());
		}
	}

	GLint progRel = 0;
	glGetProgramiv(progId, GL_LINK_STATUS, &progRel);
	if (progRel != GL_TRUE)
	{
		return false;
	}

	return true;
}

unsigned int ShaderProgram::Get(const std::vector<unsigned int>& shaderObjIds,
	std::string* pErrorLog /*= nullptr*/)
{
	GLuint program = glCreateProgram();
	if (program == 0)
		return 0;

	if (!LinkShaderToProgram(program, shaderObjIds, pErrorLog))
	{
		return 0;
	}

	return program;
}

void ShaderProgram::SetCurrent(unsigned int programHandle)
{
	glUseProgram(programHandle);
}

void ShaderProgram::ClearCurrent()
{
	glUseProgram(0);
}

bool ShaderProgram::IsValid(GLuint programHandle)
{
	return glIsProgram(programHandle);
}
