#include "pch.h"
#include "WindowOper.h"

WindowOper::WindowOper(GLFWwindow* pWnd)
	:m_pWnd(pWnd)
{

}

void WindowOper::ProcessUI()
{
	if (glfwGetKey(m_pWnd, GLFW_KEY_ESCAPE) == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(m_pWnd, true);
	}
}

void WindowOper::WndSizeCallback(GLFWwindow* window, int width, int height)
{
	// make sure the viewport matches the new window dimensions; note that width and 
	// height will be significantly larger than specified on retina displays.
	glViewport(0, 0, width, height);
}
