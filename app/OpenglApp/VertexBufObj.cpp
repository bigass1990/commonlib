#include "pch.h"
#include "VertexBufObj.h"

bool VBO::Create(BufferType type, unsigned int bufCount)
{
	try
	{
		std::unique_ptr<GLuint[]> pBufIds(new GLuint[bufCount]);
		glGenBuffers(bufCount, pBufIds.get());

		for (unsigned int i = 0; i < bufCount; ++i)
		{
			m_vboIds.push_back(pBufIds[i]);
		}
	}
	catch (const std::exception&)
	{
		return false;
	}
	return true;
}

void VBO::Bind(BufferType type, unsigned int bufId)
{
	glBindBuffer(static_cast<GLenum>(type), bufId);
}

void VBO::UnBind(BufferType type)
{
	glBindBuffer(static_cast<GLenum>(type), 0);
}

bool VBO::IsBuffer(BufferType type)
{
	return glIsBuffer(static_cast<GLenum>(type)) == GL_TRUE;
}

void VBO::Delete(unsigned int bufId)
{
	glDeleteBuffers(1, &bufId);
}

void VBO::Delete(std::vector<unsigned int> bufIds)
{
	glDeleteBuffers((GLsizei)bufIds.size(), bufIds.data());
}

void VBO::SetData(BufferType bufType, unsigned int byteCount,
	const void* pData, DataUsageMode drawType)
{
	glBufferData(static_cast<GLenum>(bufType), (GLsizeiptr)byteCount,
		(const GLvoid*)pData, static_cast<GLenum>(drawType));
}

bool VAO::Create(unsigned int arrCount /*= 1*/)
{
	std::unique_ptr<GLuint[]> pArrays(new GLuint[arrCount]);
	glGenVertexArrays(arrCount, pArrays.get());
	
	m_vaoIds.reserve(arrCount);
	for (unsigned int i = 0; i < arrCount; ++i)
	{
		m_vaoIds.push_back(pArrays[i]);
	}
	return true;
}

void VAO::Bind(unsigned int arrayId)
{
	glBindVertexArray(arrayId);
}

void VAO::UnBind()
{
	glBindVertexArray(0);
}

void VAO::Enable(unsigned int arrayId)
{
	glEnableVertexAttribArray(arrayId);
}

void VAO::Disable(unsigned int vaoId)
{
	glDisableVertexAttribArray(vaoId);
}

void VAO::Delete(unsigned int vaoId)
{
	glDeleteVertexArrays(1, &vaoId);
}

void VAO::Delete(std::vector<unsigned int> vaoIds)
{
	glDeleteVertexArrays((GLsizei)vaoIds.size(), vaoIds.data());
}

bool VAO::IsVAO(unsigned int vaoId)
{
	return glIsVertexArray(vaoId) == GL_TRUE;
}

bool GLUniform::SetValue(GLuint programId, const std::string& uniformName, GLfloat value)
{
	// 获取统一变量offset的句柄。
	GLint offsetLoc = glGetUniformLocation(programId, uniformName.c_str());
	if (offsetLoc == -1)
	{
		return false;
	}
	// 将x值传递给offset变量。
	glProgramUniform1f(programId, offsetLoc, value);

	return true;
}
