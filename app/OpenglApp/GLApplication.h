#pragma once

class WindowOper;
class GraphOper;

class GLApplication
{
public:
	GLApplication();
	GLApplication(const GLApplication&) = delete;
	GLApplication(GLApplication&&) = delete;

	~GLApplication();

public:
	GLFWwindow* Wnd() const { return m_pWnd; }

	int ShowWnd();

private:

	GLFWwindow* m_pWnd;
	WindowOper* m_pWndOper;

	GraphOper* m_pGraphOper;
};

