#pragma once

class GLSysConfig
{
protected:
	GLSysConfig();

public:
	static GLSysConfig* Instance();

	~GLSysConfig();

public:
	// OpenGL主版本号、次版本号。
	uint GetMajorVer() const;
	uint GetMinorVer() const;

	// 主窗口宽高。
	uint GetWndWidth() const;
	uint GetWndHeight() const;

	// 主窗口标题。utf8编码。
	std::string GetWndTitle() const;

};

#define GlSysCfg GLSysConfig::Instance()

