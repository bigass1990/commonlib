#include "pch.h"
#include "GLUtils.h"
#include <fstream>

GLError GLUtils::GetGLError()
{
	GLenum error = GL_NO_ERROR;
	while ((error = glGetError()) != GL_NO_ERROR)
	{
		break;
	}
	return static_cast<GLError>(error);
}

void GLUtils::SetPointSize(float size)
{
	glPointSize(size);
}

void GLUtils::SetLineWidth(float width)
{
	glLineWidth(width);
}

bool GLUtils::ReadShaderSource(const char* filePath, std::string& content)
{
	std::ifstream fileStream(filePath);
	std::string line = "";

	while (!fileStream.eof())
	{
		getline(fileStream, line);
		content.append(line + "\n");
	}
	fileStream.close();

	return true;
}