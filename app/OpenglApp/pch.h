﻿// pch.h: 这是预编译标头文件。
// 下方列出的文件仅编译一次，提高了将来生成的生成性能。
// 这还将影响 IntelliSense 性能，包括代码完成和许多代码浏览功能。
// 但是，如果此处列出的文件中的任何一个在生成之间有更新，它们全部都将被重新编译。
// 请勿在此处添加要频繁更新的文件，这将使得性能优势无效。

#ifndef PCH_H
#define PCH_H

// 添加要在此处预编译的标头
#include <windows.h>
#include "common.h"
#include "stringlib.h"

#include "filelib.h"

#include "glad\glad.h"
#include "GLFW\glfw3.h"
#include "glm.hpp"
#include "type_ptr.hpp"
#include "matrix_transform.hpp"

#pragma comment(lib, "opengl32.lib")
#pragma message("    自动连接到 opengl32.lib")

#pragma comment(lib, "glfw3.lib")
#pragma message("    自动连接到 glfw3.lib")

#pragma comment(lib, "glm_static.lib")
#pragma message("    自动连接到 glm_static.lib")

// 虽然调试的时候还是会有cmd弹出。
// 但是直接运行生成的.exe文件时，就不会有cmd窗口弹出。
#pragma comment(linker,"/subsystem:\"windows\" /entry:\"mainCRTStartup\"")

using uint = unsigned int;

#endif //PCH_H
