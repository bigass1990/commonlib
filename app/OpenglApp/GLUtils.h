#pragma once

enum class GLError
{
	NoError = GL_NO_ERROR, // （0）当前无错误值 
	InvalidEnum = GL_INVALID_ENUM, // （1280）仅当使用非法枚举参数时，如果使用该参数有指定环境，则返回 GL_INVALID_OPERATION  
	InvalidValue = GL_INVALID_VALUE, //（1281）仅当使用非法值参数时，如果使用该参数有指定环境，则返回 GL_INVALID_OPERATION
	InvalidOper = GL_INVALID_OPERATION, //（1282）命令的状态集合对于指定的参数非法。
	StackOverflow = GL_STACK_OVERFLOW, //（1283）压栈操作超出堆栈大小。
	StackUnderflow = GL_STACK_UNDERFLOW, //（1284）出栈操作达到堆栈底部。
	OutOfMemory = GL_OUT_OF_MEMORY, //（1285）不能分配足够内存时。
	InvalidFrameBufOper = GL_INVALID_FRAMEBUFFER_OPERATION, //（1286）当操作未准备好的真缓存时。
	ContextLost = GL_CONTEXT_LOST, //（1287）由于显卡重置导致 OpenGL context 丢失。
};

enum class FaceType
{
	FrontAndBack = GL_FRONT_AND_BACK,

};


class GLUtils
{
public:
	static GLError GetGLError();
	
	/// <summary>
	/// 设置固定的像素大小。如果没有开启GL_PROGRAM_POINT_SIZE，则它将被用于设置点的大小。
	/// 如果开启了GL_PROGRAM_POINT_SIZE，可以在着色器中使用内置变量gl_PointSize改变点大小。
	/// 默认点大小为1.0
	/// </summary>
	/// <param name="size"></param>
	/// <returns></returns>
	static void SetPointSize(float size);

	/// <summary>
	/// 这只线段的固定宽度。默认值为1.0
	/// </summary>
	/// <param name="width"></param>
	/// <returns></returns>
	static void SetLineWidth(float width);

	// 从文件中读取着色器代码
	static bool ReadShaderSource(const char* filePath, std::string& content);
};

