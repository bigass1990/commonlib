#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

// 纹理环绕方式
enum class TexWrap
{
	Repeat = GL_REPEAT, // 对纹理的默认行为。重复纹理图像。
	MirroredRepeat = GL_MIRRORED_REPEAT, // 和GL_REPEAT一样，但每次重复图片是镜像放置的。
	ClampToEdge = GL_CLAMP_TO_EDGE, // 纹理坐标会被约束在0到1之间，超出的部分会重复纹理坐标的边缘，产生一种边缘被拉伸的效果。
	ClampToBorder = GL_CLAMP_TO_BORDER, // 超出的坐标为用户指定的边缘颜色。
};

// 纹理坐标轴
enum class TexWrapCoord
{
	S = GL_TEXTURE_WRAP_S,
	T = GL_TEXTURE_WRAP_T,
};

// 纹理过滤
enum class TexFilter
{
	Nearest = GL_NEAREST, // 邻近过滤，Nearest Neighbor Filtering.OpenGL默认的纹理过滤方式.
						  // 产生了颗粒状的图案，我们能够清晰看到组成纹理的像素.
	Linear = GL_LINEAR, // 线性过滤，(Bi)linear Filtering.
						// 产生更平滑的图案，很难看出单个的纹理像素。GL_LINEAR可以产生更真实的输出
};

// 当进行放大(Magnify)和缩小(Minify)操作的时候可以设置纹理过滤的选项，
// 比如你可以在纹理被缩小的时候使用邻近过滤，被放大时使用线性过滤。
enum class TexOper
{
	Min = GL_TEXTURE_MIN_FILTER,
	Mag = GL_TEXTURE_MAG_FILTER,
};

enum class TextureType
{
	Tx1D = GL_TEXTURE_1D,
	Tx1DArray = GL_TEXTURE_1D_ARRAY,
	Tx2D = GL_TEXTURE_2D,
	Tx2DArray = GL_TEXTURE_2D_ARRAY,
	Tx2DMultiSample = GL_TEXTURE_2D_MULTISAMPLE,
	Tx2DMultiSampleArray = GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
	Tx3D = GL_TEXTURE_3D,
	TxCube = GL_TEXTURE_CUBE_MAP,
	TxCubeArray = GL_TEXTURE_CUBE_MAP_ARRAY,
	TxRectangle = GL_TEXTURE_RECTANGLE,
	TxBuffer = GL_TEXTURE_BUFFER,
};

struct Texture1D
{
	int width;
};

struct Texture2D : public Texture1D
{
	int height;
};

struct Texture3D :public Texture2D
{
	int depth;
};

class Texture
{
public:
	void Create(TextureType type);
	void Create(TextureType type, unsigned int size);

	bool SetImage(const std::wstring& imageFile);

	void Delete(unsigned int id);
	void Delete(std::vector<unsigned int> ids);

	static bool IsTexture(unsigned int id);

private:
	std::vector<unsigned int> m_Ids;
};

#pragma warning(pop)