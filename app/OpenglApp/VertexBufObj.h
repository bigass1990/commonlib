#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

/// <summary>
/// OpenGL使用的数据类型
/// </summary>
enum class GLDataType
{
	Byte = GL_BYTE,
	UByte = GL_UNSIGNED_BYTE,
	Short = GL_SHORT,
	UShort = GL_UNSIGNED_SHORT,
	Int = GL_INT,
	UInt = GL_UNSIGNED_INT,
	Fixed = GL_FIXED,
	HalfFloat = GL_HALF_FLOAT,
	Float = GL_FLOAT,
	Double = GL_DOUBLE,
	Int_2_10_10_10_REV = GL_INT_2_10_10_10_REV,// GLuint（压缩数据类型）
	Unsigned_Int_2_10_10_10_REV = GL_UNSIGNED_BYTE_2_3_3_REV,// GLuint（压缩数据类型）
};

/// <summary>
/// 缓冲区类型
/// </summary>
enum class BufferType
{
	Array = GL_ARRAY_BUFFER,				// 表示顶点数据
	Index = GL_ELEMENT_ARRAY_BUFFER,		// 表示索引数据
	AtomicCounter = GL_ATOMIC_COUNTER_BUFFER,
	ElementArray = GL_ELEMENT_ARRAY_BUFFER,
	PixelPack = GL_PIXEL_PACK_BUFFER,		// 表示从OpenGL获取的的像素数据
	PixelUnpack = GL_PIXEL_UNPACK_BUFFER,	// 表示传递给OpenGL的像素数据
	
	// 用于拷贝缓存之间的数据，并且不会引起OpenGL状态变化，也不会产生任何特殊的OpenGL调用。
	CopyRead = GL_COPY_READ_BUFFER, 
	CopyWrite = GL_COPY_WRITE_BUFFER,

	ShaderStorage = GL_SHADER_STORAGE_BUFFER,

	// 如果采取简介绘制方法，该缓存用于存储绘制命令的参数
	DrawIndirect = GL_DRAW_INDIRECT_BUFFER,

	// 可以在管线的定点处理结束时（即经过顶点着色，可能还有几何着色阶段），
	// 将经过变换的顶点重新捕获，并将部分属性写入到缓存对象中。
	TransformFeedback = GL_TRANSFORM_FEEDBACK_BUFFER,

	// 用于创建uniform缓存对象的缓存数据。
	Uniform = GL_UNIFORM_BUFFER,

	// 直接绑定到纹理对象的缓存，可以直接在着色器中读取它们的数据。
	Texture = GL_TEXTURE_BUFFER,
};

/// <summary>
/// 数据预期使用模式
/// Stream-数据将被修改一次，最多使用几次。
/// Static-数据将被修改一次，多次使用。
/// Dynamic-数据将被反复修改，多次使用。
/// Draw-数据由应用程序修改，并用于GL drawing和image specification commmands的源。
/// Read-
/// </summary>
enum class DataUsageMode
{
	StaticDraw = GL_STATIC_DRAW,
	StaticRead = GL_STATIC_READ,
	StaticCopy = GL_STATIC_READ,
	DynamicDraw = GL_DYNAMIC_DRAW,
	DynamicRead = GL_STATIC_READ,
	DynamicCopy = GL_STATIC_READ,
	StreamDraw = GL_STREAM_DRAW,
	StreamRead = GL_STREAM_READ,
	StreamCopy = GL_STREAM_COPY,

};

// 顶点缓冲对象 VertexBufferObj
// 是OpenGL服务端分配和管理的一块内存区域，
// 并且几乎所有传入OpenGL的数据都是存储在缓存对象当中的。
class VBO
{
public:

	// 
	bool Create(BufferType type, unsigned int bufCount = 1);

public:
	static void Bind(BufferType type, unsigned int bufId);

	static void UnBind(BufferType type);

	static bool IsBuffer(BufferType type);

	static void Delete(unsigned int bufId);

	static void Delete(std::vector<unsigned int> bufIds);
	
	// 为buffer创建新的数据存储。
	void SetData(BufferType bufType, unsigned int byteCount,
		const void* pData, DataUsageMode drawType);

private:
	std::vector<unsigned int> m_vboIds;
	BufferType m_eBufType;
};

// 顶点数组对象 VertexArrayObj
// OpenGL要求至少创建一个vao
class VAO
{
public:

	bool Create(unsigned int arrCount = 1);

	/// <summary>
	/// 将指定的vao绑定到OpenGL环境以便使用。
	/// </summary>
	/// <param name="vaoId"></param>
	static void Bind(unsigned int vaoId);

	static void UnBind();

	static void Enable(unsigned int vaoId);

	static void Disable(unsigned int vaoId);

	static void Delete(unsigned int vaoId);

	static void Delete(std::vector<unsigned int> vaoIds);

	/// <summary>
	/// 返回是否是一个没有被删除的vao对象。
	/// </summary>
	/// <param name="vaoId"></param>
	/// <returns></returns>
	static bool IsVAO(unsigned int vaoId);

protected:
private:
	std::vector<unsigned int> m_vaoIds;

};

class GLUniform
{
public:

	/// <summary>
	/// 设置统一变量值
	/// </summary>
	/// <param name="programId">统一变量所在程序id/hander</param>
	/// <param name="uniformName">统一变量名称，不能包含white space。</param>
	/// <param name="value">要指定的统一变量的值</param>
	/// <returns>如果统一变量名获取失败，返回false。否则返回true。</returns>
	static bool SetValue(GLuint programId, const std::string& uniformName, GLfloat value);
};

#pragma warning(pop)