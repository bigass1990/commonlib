#include "pch.h"
#include "Texture.h"

void Texture::Create(TextureType type)
{
	unsigned int texId;
	glGenTextures(1, &texId);
	m_Ids.push_back(texId);

	glBindTexture(static_cast<GLenum>(type), texId);
}

void Texture::Create(TextureType type, unsigned int size)
{
	m_Ids.assign(size, 0);
	glGenTextures(size, &*m_Ids.begin());

	for (auto id : m_Ids)
	{
		glBindTexture(static_cast<GLenum>(type), id);
	}
}
