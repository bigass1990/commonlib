#include "pch.h"
#include "GLApplication.h"
#include "GLSysConfig.h"
#include "WindowOper.h"
#include "GraphOper.h"

GLApplication::GLApplication()
	:m_pWnd(nullptr), m_pWndOper(nullptr), m_pGraphOper(nullptr)
{
}

GLApplication::~GLApplication()
{
	if (m_pWndOper)
	{
		delete m_pWndOper;
		m_pWndOper = nullptr;
	}
	if (m_pGraphOper)
	{
		delete m_pGraphOper;
		m_pGraphOper = nullptr;
	}
	if (m_pWnd)
	{
		glfwDestroyWindow(m_pWnd);
		glfwTerminate();
	}
}

int GLApplication::ShowWnd()
{
	if (!m_pWnd)
	{
		if (!glfwInit())
			exit(EXIT_FAILURE);

		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, GlSysCfg->GetMajorVer());
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, GlSysCfg->GetMinorVer());
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

		std::string title(std::move(GlSysCfg->GetWndTitle()));
		m_pWnd = glfwCreateWindow(GlSysCfg->GetWndWidth(),
			GlSysCfg->GetWndHeight(), title.c_str(), nullptr, nullptr);
		
		glfwMakeContextCurrent(m_pWnd);

		m_pWndOper = new WindowOper(m_pWnd);
		m_pGraphOper = new GraphOper(m_pWnd);

		glfwSetFramebufferSizeCallback(m_pWnd, WindowOper::WndSizeCallback);

		/* opengl只是一个标准/规范，具体的实现是由驱动开发商针对特定显卡实现的。
		*  由于OpenGL驱动版本众多，它大多数函数的位置都无法在编译时确定下来。需要在
		*  运行时查询。开发者需要在运行时获取函数地址并保存在一个函数指针中供以后使用。
		*  取得地址的方法因平台而异。GLAD就是这样一个获取OpenGL函数地址的库。
		 */
		if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
			exit(EXIT_FAILURE);

		glfwSwapInterval(1);

		m_pGraphOper->InitVertex();

		// 消息循环
		while (!glfwWindowShouldClose(m_pWnd))
		{
			m_pWndOper->ProcessUI();

			m_pGraphOper->Draw(glfwGetTime());

			glfwSwapBuffers(m_pWnd);
			glfwPollEvents();
		}

		return 1;
	}

	return 0;
}
