#include "pch.h"
#include "GraphOper.h"
#include "ShaderProgram.h"
//#include <matrix_clip_space.inl>
#include <type_ptr.hpp>
#include <matrix_clip_space.hpp>


constexpr unsigned int numVaos = 1;
constexpr unsigned int numVbos = 2;
GLuint renderingProgram = 0;
GLuint vao[numVaos] = { 0 };
GLuint vbo[numVbos] = { 0 };
float cameraX, cameraY, cameraZ;
float cubeLocX, cubeLocY, cubeLocZ;

GLuint mvLoc, projLoc;
int width, height;
float aspect;
glm::mat4 pMat, vMat, mMat, mvMat;

float x = 0.0f; // 三角形在x轴的位置
float inc = 0.1f; // 移动三角形的偏移量

void setupVertices()
{
	// 36个顶点，12个三角形，组成了放置在原点处的2x2x2立方体
	float vertexPos[108] =
	{
		-1, 1, -1, -1, -1, -1, 1, -1, -1,
		1, -1, -1, 1, 1, -1, -1, 1, -1,
		1, -1, -1, 1, -1, 1, 1, 1, -1,
		1, -1, 1, 1, 1, 1, 1, 1, -1,
		1, -1, 1, -1, -1, 1, 1, 1, 1,
		-1, -1, -1, -1, -1, 1, 1, 1, 1,
		-1, -1, -1, -1, 1, 1, 1, 1, 1,
		-1, -1, 1, -1, -1, -1, -1,1, 1,
		-1, -1, 1, 1, 1, -1, -1, 1, 1,
		1, -1, -1, -1, -1, -1, 1, -1, 1,
		-1, 1, -1, 1, 1, -1, 1, 1, 1,
		1, 1, 1, -1, 1, 1, -1, 1, -1,
	};

	glGenVertexArrays(1, vao);
	glBindVertexArray(vao[0]);
	glGenBuffers(numVbos, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexPos), vertexPos, GL_STATIC_DRAW);
}

GLuint CreateShaderProgram()
{
	std::string vertexFile = FileUtils::GetRootDir() + "glsl\\vertex.glsl";
	std::string fragFile = FileUtils::GetRootDir() + "glsl\\fragment.glsl";
	std::string vertexLog, fragLog;
	GLuint vid = ShaderObj::GetCompiledShader(ShaderType::Vertex, vertexFile, &vertexLog);
	GLuint fid = ShaderObj::GetCompiledShader(ShaderType::Fragment, fragFile, &fragLog);

	std::vector<GLuint> shaderIds{ vid, fid };
	GLuint program = glCreateProgram();
	std::string progLog;
	ShaderProgram::LinkShaderToProgram(program, shaderIds, &progLog);

	// 把着色器对象链接到程序对象后，就可以删除着色器对象了。
	ShaderObj::Delete(shaderIds);

	return program;
}

GraphOper::GraphOper(GLFWwindow* pWnd)
	: m_pWnd(pWnd)
{

}

void GraphOper::InitVertex()
{
	renderingProgram = CreateShaderProgram();
	
	cameraX = 0;
	cameraY = 0;
	cameraZ = 8;
	cubeLocX = 0;
	cubeLocY = -2;
	cubeLocZ = 0; // 沿y轴下移以展示透视
	setupVertices();
}

void GraphOper::Draw(double currentTime)
{
	float color = (float)(currentTime - (int)currentTime);
	glClearColor(color, color, color, 1);
	//glClearColor(1, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	glUseProgram(renderingProgram);

	mvLoc = glGetUniformLocation(renderingProgram, "mv_matrix");
	projLoc = glGetUniformLocation(renderingProgram, "proj_matrixx");

	// 构建透视矩阵
	glfwGetFramebufferSize(m_pWnd, &width, &height);
	aspect = (float)(width / height);
	//pMat = glm::perspective(1.0472, aspect, 0.1, 1000);


	x += inc; // 切换至让三角形向右移动。
	if (x > 1.0f)
	{
		inc = -0.01f;
	}
	if (x < -1.0f)
	{
		inc = 0.01f;
	}
	// inc = x > 1.0f ? -0.01f : 0.01f;

	 // 获取统一变量offset的句柄。
	GLuint offsetLoc = glGetUniformLocation(renderingProgram, "offset");

	// 将x值传递给offset变量。
	glProgramUniform1f(renderingProgram, offsetLoc, x);

	glDrawArrays(GL_TRIANGLES, 0, 3);
}
