#pragma once

struct m3u8Info;

namespace MergeUtils
{
	// 生成合并后的m3u8文件。
	bool MergeM3U8(
		const m3u8Info& minfo,
		const std::wstring& outDirectory);
};


class CMergeM3U8
{
public:
	CMergeM3U8();
	CMergeM3U8(const std::vector<m3u8Info>& vecM3u8,
		const std::wstring& outDirectory);

	void operator()(int& succeedCount);

	const std::vector<bool>& Results() const;

private:
	std::vector<m3u8Info> vecM3u8;
	std::wstring outDirectory;

	std::vector<bool> vecResult;
};