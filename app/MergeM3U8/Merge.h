#pragma once
#include "pch.h"

constexpr unsigned int ThreadCount = 8;

struct m3u8Info 
{
	m3u8Info(const FileSystem::path& _file, const FileSystem::path& _dir, bool _hasKey)
		:file(_file), directory(_dir), bHasKey(_hasKey)
	{

	}

	FileSystem::path file;
	FileSystem::path directory;
	bool bHasKey;
	std::string key;
};

class Merge
{
public:
	Merge(const std::wstring& path);

	int Do(std::wstring& strError);

private:
	std::wstring m_path;
	std::wstring m_outDirectory; // ����ļ�Ŀ¼
};

