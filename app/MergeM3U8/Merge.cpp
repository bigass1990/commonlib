#include "pch.h"
#include "Merge.h"
#include "MergeUtils.h"
#include <thread>


// 从m3u8文件中查找到其对应的文件夹名称
bool GetM3U8Direction(const std::wstring& path, std::wstring& directory, bool& bHasKey)
{
	std::wifstream fileStream(path);
	fileStream.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));
	std::wstring line;
	bool bFindEXTINF = false;
	while (std::getline(fileStream, line))
	{
		if (line.find(__T("EXT-X-KEY")) != std::wstring::npos)
		{
			// 有加密
			// aes-128加密
			if (line.find(__T("AES-128")) != std::wstring::npos
				&& (line.find(__T("k0")) != std::wstring::npos
					|| line.find(__T("kye0")) != std::wstring::npos))
			{
				// 目前仅支持aes128加密，并且密钥文件为k0
				bHasKey = true;
				continue;
			}
		}

		if (bFindEXTINF)
		{
			size_t index = line.rfind(L"\\");
			if (index != std::wstring::npos)
			{
				std::wstring subLine = line.substr(0, index);
				index = subLine.rfind(L"\\");
				if (index != std::wstring::npos)
				{
					directory = subLine.substr(index);
					return true;
				}
			}
			else
			{
				index = line.rfind(__T("/"));
				if (index != std::wstring::npos)
				{
					std::wstring subLine = line.substr(0, index);
					index = subLine.rfind(L"/");
					if (index != std::wstring::npos)
					{
						directory = subLine.substr(index + 1);
						return true;
					}
				}
			}
		}

		if (line.find(L"EXTINF") != std::wstring::npos)
		{
			bFindEXTINF = true;
		}
	}

	return false;
}



Merge::Merge(const std::wstring& path)
{
	m_path = path;
	m_outDirectory = __T("c:\\MergeM3U8\\");
}

int Merge::Do(std::wstring& strError)
{
	FileSystem::path path = m_path;
	if (!FileSystem::exists(path) || !FileSystem::is_directory(path))
	{
		return 0;
	}

	// 存储m3u8文件，存储目录
	std::vector<std::wstring> m3u8Files, m3u8Direcorys;
	{
		// 遍历指定文件夹下的所有文件、文件夹，获取其中的m3u8文件和文件夹。
		FileSystem::directory_iterator iter(path);
		for (iter = FileSystem::begin(iter); iter != FileSystem::end(iter); ++iter)
		{
			const FileSystem::path& fPath = iter->path();
			if (FileSystem::is_regular_file(fPath))
			{
				if (fPath.extension().wstring().compare(__T(".m3u8")) == 0)
				{
					m3u8Files.push_back(fPath);
				}
			}
			else if (FileSystem::is_directory(fPath))
			{
				m3u8Direcorys.push_back(fPath);
			}
		}

		if (m3u8Files.empty() || m3u8Direcorys.empty())
		{
			return 0;
		}
	}

	// 获取m3u8文件与其对应文件夹的映射
	std::vector<m3u8Info> vecM3u8;
	for (auto iterFile = m3u8Files.cbegin(); iterFile != m3u8Files.cend();)
	{
		std::wstring dir; // m3u8对应的视频片段所在文件夹
		bool bHasKey = false;
		if (!GetM3U8Direction(*iterFile, dir, bHasKey)
			|| bHasKey)
		{
			++iterFile;
			continue;
		}

		auto iterDir = std::find_if(m3u8Direcorys.cbegin(), m3u8Direcorys.cend(),
			[&](const std::wstring& floder)
			{
				return floder.find(dir) != std::wstring::npos;
			}
		);

		if (iterDir != m3u8Direcorys.cend())
		{
			vecM3u8.push_back(m3u8Info(*iterFile, *iterDir, bHasKey));

			m3u8Direcorys.erase(iterDir);
			iterFile = m3u8Files.erase(iterFile);
		}
		else
		{
			++iterFile;
		}
	}

	if (vecM3u8.empty())
	{
		strError += __T("未发现合法m3u8文件");
		return 0;
	}

	if (!vecM3u8.empty() && !FileSystem::exists(m_outDirectory))
	{
		FileSystem::create_directories(m_outDirectory);
	}

	unsigned int validFileCount = vecM3u8.size();
	std::cout << "发现m3u8文件：" << validFileCount << "个。" << std::endl;

	// 单线程IO。
	{
		CMergeM3U8 pMerge(vecM3u8, m_outDirectory);
		int succeed = 0;
		pMerge.operator()(succeed);
		return succeed;
	}
	unsigned int tCount = ThreadCount/*std::thread::hardware_concurrency()*/;
	/*if (tCount > ThreadCount)
	{
		tCount = ThreadCount;
	}*/
	unsigned int count1 = vecM3u8.size() % tCount;
	unsigned int count = (vecM3u8.size() - count1) / tCount;

	std::vector<std::vector<m3u8Info>> vecM3u8s;
	for (unsigned int i = 0; i < tCount; ++i)
	{
		std::vector<m3u8Info> vec;
		for (unsigned int j = i * count; j < (i + 1) * count; ++j)
		{
			vec.push_back(vecM3u8[j]);
		}
		vecM3u8s.push_back(vec);
	}

	if (count == 0)
	{
		for (unsigned int j = 0; j < count1; ++j)
		{
			std::vector<m3u8Info> vec;
			vec.push_back(vecM3u8[j]);
			vecM3u8s.push_back(vec);
		}
	}
	else
	{
		int i = 0;
		for (unsigned int j = count * tCount; j < count * tCount + count1; ++j)
		{
			vecM3u8s[i].push_back(vecM3u8[j]);
			++i;
		}
	}

	// 合并
	unsigned int mergedCount = 0;
	std::vector<std::thread> vecThread;
	std::vector<CMergeM3U8*> vecMerge;
	int succeed = 0;
	for (unsigned int i = 0; i < vecM3u8s.size(); ++i)
	{
		CMergeM3U8* pMerge = new CMergeM3U8(vecM3u8s[i], m_outDirectory);
		
		std::thread pThread(std::ref(*pMerge), std::ref(succeed));

		vecMerge.push_back(pMerge);
		vecThread.push_back(std::move(pThread));
	}

	std::for_each(vecThread.begin(), vecThread.end(), std::mem_fn(&std::thread::join)); // 对每个线程调用join()

	for (unsigned int i = 0; i < vecMerge.size(); ++i)
	{
		for (unsigned int j = 0; j < vecMerge[i]->Results().size(); ++j)
		{
			if (vecMerge[i]->Results().at(j))
			{
				mergedCount++;
			}
		}
	}

	if (mergedCount > 0)
	{
		if (mergedCount < validFileCount)
			std::cout << "转换失败 " << validFileCount - mergedCount << " 个。" << std::endl;

		std::cout << "转换成功并删除" << mergedCount << " 个。" << std::endl;
	}

	return mergedCount;
}
