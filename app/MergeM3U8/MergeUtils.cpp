#include "pch.h"
#include "MergeUtils.h"
#include "Merge.h"

namespace MergeUtils
{
	bool CompPath(const std::wstring& file1, const std::wstring& file2)
	{
		FileSystem::path p1 = file1;
		FileSystem::path p2 = file2;
		std::wstring s1 = p1.filename().wstring();
		std::wstring s2 = p2.filename().wstring();
		if (StringUtils::isnum(s1) && StringUtils::isnum(s2))
		{
			double d1 = stof(s1);
			double d2 = stof(s2);
			return d1 < d2;
		}
		else
		{
			int index1 = StringUtils::RFindFirstNotNum(s1);
			int index2 = StringUtils::RFindFirstNotNum(s2);
			if (index1 >= 0 && index1 < (int)s1.size() - 1
				&& index2 >= 0 && index2 < (int)s2.size() - 1)
			{
				double d1 = stof(s1.substr(index1 + 1));
				double d2 = stof(s2.substr(index2 + 1));
				return d1 < d2;
			}
		}
		return false;
	}

	struct mergeFile
	{
		FileSystem::path file;
		FileSystem::path directory;
		bool isEncrypt; // 是否加密
	};

	bool GetM3U8_Key(FileSystem::path dir, std::string& key)
	{
		FileSystem::path tmpDir = dir;
		FileSystem::path keydir = tmpDir /= __T("k0");
		if (!FileSystem::exists(keydir))
		{
			tmpDir = dir;
			keydir = tmpDir /= __T("kye0");
			if (!FileSystem::exists(keydir))
			{
				return false;
			}
		}
		std::ifstream in(CodeConvert::wstr_to_utf8(keydir), std::ios::in);
		if (in.is_open())
		{
			std::getline(in, key);
			return true;
		}
		return false;
	}

	// 合并
	bool MergeM3U8(
		const m3u8Info& minfo,
		const std::wstring& outDirectory)
	{
		// 获取m3u8视频文件夹下的所有文件
		std::vector<std::wstring> filesInDir;
		FileSystem::directory_iterator iter(minfo.directory);
		for (FileSystem::begin(iter); iter != FileSystem::end(iter); ++iter)
		{
			std::wstring extName = iter->path().extension().wstring();
			if (!extName.empty() /*extName.compare(__T(".m3u8")) == 0*/)
				continue;
			std::wstring name = iter->path().filename().wstring();
			if (name.compare(__T("k0")) == 0
				|| name.compare(__T("kye0")) == 0)
			{
				// 应该是密钥文件
				//return false;
				continue; // 后面处理加密
			}

			filesInDir.push_back(iter->path().wstring());
		}
		if (filesInDir.empty())
			return false;

		// 排序

		std::sort(filesInDir.begin(), filesInDir.end(), CompPath);

		// 输出文件
		std::wstring outFile = outDirectory + minfo.file.stem().wstring() + L".mp4";

		// 合并
		if (minfo.bHasKey)
		{
			std::string key;
			if (GetM3U8_Key(minfo.directory, key) && key.size() == 16)
			{
				unsigned char* pcrypt = (unsigned char*)(key.c_str());
				

				std::ofstream wfile(outFile, std::ios_base::out | std::ios_base::binary);
				if (!wfile.is_open())
				{
					std::cout << "输出文件创建失败" << std::endl;
				}

				for (auto iterMerge = filesInDir.cbegin(); iterMerge != filesInDir.cend(); ++iterMerge)
				{
					AesEncryptor cryptor(pcrypt);
					cryptor.DecryptTxtFile(CodeConvert::wstr_to_utf8(*iterMerge).c_str(), wfile);
				}

				wfile.close();
			}
		}
		else
		{
			std::wofstream writeFile(outFile, std::ios_base::out | std::ios_base::binary);
			if (!writeFile.is_open())
			{
				std::cout << "输出文件创建失败" << std::endl;
			}

			for (auto iterMerge = filesInDir.cbegin(); iterMerge != filesInDir.cend(); ++iterMerge)
			{
				std::wifstream fileStream(*iterMerge, std::ios_base::in | std::ios_base::binary);
				if (!fileStream.is_open())
				{
					continue;
				}
				writeFile << fileStream.rdbuf();
			}

			writeFile.close();
		}

		return true;
	}
}

CMergeM3U8::CMergeM3U8()
{
}

CMergeM3U8::CMergeM3U8(const std::vector<m3u8Info>& _vecM3u8,
	const std::wstring& _outDirectory)
	:vecM3u8(_vecM3u8), outDirectory(_outDirectory)
{
}

void CMergeM3U8::operator()(int& succeedCount)
{
	size_t count = vecM3u8.size();

	for (size_t i = 0; i < count; ++i)
	{
		bool rel = MergeUtils::MergeM3U8(vecM3u8[i], outDirectory);
		vecResult.push_back(rel);
		if (rel)
		{
			++succeedCount;
			FileSystem::remove(vecM3u8[i].file);
			FileSystem::remove_all(vecM3u8[i].directory);
		}
	}
}


const std::vector<bool>& CMergeM3U8::Results() const
{
	return vecResult;
}
