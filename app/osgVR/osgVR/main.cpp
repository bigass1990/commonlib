/**********************************************************
*Write by FlySky
*zzuxp@163.com  http://www.OsgChina.org   
**********************************************************/

#include <osgViewer/Viewer>

#include <osg/Node>
#include <osg/Geode>
#include <osg/Group>

#include <shdeprecated.h>

#include <osgDB/ReadFile>
#include <osgDB/WriteFile>

#include <osgUtil/Optimizer>

// 创建一个四边形节点
osg::ref_ptr<osg::Node> CreateQuad()
{	
	// 顶点
	osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array();
	v->push_back(osg::Vec3(0, 0, 0));
	v->push_back(osg::Vec3(1, 0, 0));
	v->push_back(osg::Vec3(1, 0, 1));
	v->push_back(osg::Vec3(0, 0, 1));

	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
	geom->setVertexArray(v.get());

	// 纹理坐标
	osg::ref_ptr<osg::Vec2Array> vt = new osg::Vec2Array();
	vt->push_back(osg::Vec2(0, 0));
	vt->push_back(osg::Vec2(1, 0));
	vt->push_back(osg::Vec2(1, 1));
	vt->push_back(osg::Vec2(0, 1));
	geom->setTexCoordArray(0, vt.get());

	// 颜色数组
	osg::ref_ptr<osg::Vec4Array> vc = new osg::Vec4Array();
	vc->push_back(osg::Vec4(1, 0, 0, 1));
	vc->push_back(osg::Vec4(0, 1, 0, 1));
	vc->push_back(osg::Vec4(0, 0, 1, 1));
	vc->push_back(osg::Vec4(1, 1, 0, 1));
	geom->setColorArray(vc.get());
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	// 法线数组
	osg::ref_ptr<osg::Vec3Array> nc = new osg::Vec3Array;
	nc->push_back(osg::Vec3(0, -1, 0));
	geom->setNormalArray(nc.get());
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);

	// 添加图元，绘制基元为四边形
	geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4));

	osg::ref_ptr<osg::Geode> geode = new osg::Geode();
	geode->addDrawable(geom.get());

	return geode;
}

// 创建一个四边形节点，通过索引.
osg::ref_ptr<osg::Node> CreateQuadIndex()
{
	// 顶点
	osg::ref_ptr<osg::Vec3Array> v = new osg::Vec3Array();
	v->push_back(osg::Vec3(0, 0, 0));
	v->push_back(osg::Vec3(1, 0, 0));
	v->push_back(osg::Vec3(1, 0, 1));
	v->push_back(osg::Vec3(0, 0, 1));
	v->push_back(osg::Vec3(0, -1, 0));

	osg::ref_ptr<osg::Geometry> geom = new osg::Geometry();
	geom->setVertexArray(v.get());

	// 创建四边形顶点索引数组。指定绘图基元为四边形。
	osg::ref_ptr<osg::DrawElementsUInt> quad = new osg::DrawElementsUInt(
		osg::PrimitiveSet::QUADS, 0);
	quad->push_back(0);
	quad->push_back(1);
	quad->push_back(2);
	quad->push_back(3);
	geom->addPrimitiveSet(quad.get());

	// 创建三角形顶点索引数组，指定绘图基元为三角形
	osg::ref_ptr<osg::DrawElementsUInt> tri = new osg::DrawElementsUInt(
		osg::PrimitiveSet::TRIANGLES, 0);
	tri->push_back(4);
	tri->push_back(0);
	tri->push_back(3);
	geom->addPrimitiveSet(tri.get());

	// 纹理坐标
// 	osg::ref_ptr<osg::Vec2Array> vt = new osg::Vec2Array();
// 	vt->push_back(osg::Vec2(0, 0));
// 	vt->push_back(osg::Vec2(1, 0));
// 	vt->push_back(osg::Vec2(1, 1));
// 	vt->push_back(osg::Vec2(0, 1));
// 	geom->setTexCoordArray(0, vt.get());

	// 颜色数组
	osg::ref_ptr<osg::Vec4Array> vc = new osg::Vec4Array();
	vc->push_back(osg::Vec4(1, 0, 0, 1));
	vc->push_back(osg::Vec4(0, 1, 0, 1));
	vc->push_back(osg::Vec4(0, 0, 1, 1));
	vc->push_back(osg::Vec4(1, 1, 0, 1));

	// 颜色索引数组
	osg::TemplateIndexArray<unsigned int, osg::Array::UInt64ArrayType, 4, 4>* colorIndex
		= new osg::TemplateIndexArray<unsigned int, osg::Array::UInt64ArrayType, 4, 4>();
	colorIndex->push_back(0);
	colorIndex->push_back(1);
	colorIndex->push_back(2);
	colorIndex->push_back(3);
	colorIndex->push_back(2);

	geom->setColorArray(vc.get());
	//geom->setColorIndices
	geom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

	// 法线数组
	osg::ref_ptr<osg::Vec3Array> nc = new osg::Vec3Array;
	nc->push_back(osg::Vec3(0, -1, 0));
	geom->setNormalArray(nc.get());
	geom->setNormalBinding(osg::Geometry::BIND_OVERALL);

	// 添加图元，绘制基元为四边形
	//geom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::QUADS, 0, 4));

	osg::ref_ptr<osg::Geode> geode = new osg::Geode();
	geode->addDrawable(geom.get());

	return geode;
}

int main()
{

	//创建Viewer对象，场景浏览器
	osg::ref_ptr<osgViewer::Viewer> viewer = new osgViewer::Viewer();

	//创建场景组节点
	osg::ref_ptr<osg::Group> root = new osg::Group();

	//创建一个节点,读取牛的模型
	//osg::ref_ptr<osg::Node> node = osgDB::readNodeFile("cow.osg");

	//添加到场景
	//root->addChild(node.get());
	root->addChild(CreateQuadIndex());

	//优化场景数据
	osgUtil::Optimizer optimizer ;
	optimizer.optimize(root.get()) ;

	//设置场景数据
	viewer->setSceneData(root.get());

	//初始化并创建窗口
	viewer->realize();

	//开始渲染
	viewer->run();

	return 0 ;
}