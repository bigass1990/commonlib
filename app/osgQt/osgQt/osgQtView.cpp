#include "stdafx.h"
#include "osgQtView.h"
#include <osg/MatrixTransform>
#include <osgDB/ReadFile>
#include <osgGA/MultiTouchTrackballManipulator>
#include <osgGA/StateSetManipulator>
#include <osgViewer/ViewerEventHandlers>
#include <osgUtil/Optimizer>

// https://www.itdaan.com/blog/2015/07/18/4b6287b10b736a7d55fc259bb9872768.html
// https://www.codenong.com/cs105360604/
osgQtView::osgQtView(QWidget*parent)
	: QOpenGLWidget(parent)
{
	QSurfaceFormat fm = QSurfaceFormat::defaultFormat();
	fm.setRenderableType(QSurfaceFormat::OpenGL);
	fm.setSamples(16);
	
	initOsg();

	setMouseTracking(true);
	setFocusPolicy(Qt::StrongFocus);
}

osgQtView::~osgQtView()
{
}

bool osgQtView::event(QEvent* event)
{
	switch (event->type())
	{
	case QEvent::TouchBegin:
	case QEvent::TouchEnd:
	case QEvent::TouchUpdate:
	{
		QTouchEvent* pTouchEvent = static_cast<QTouchEvent*>(event);
		QList<QTouchEvent::TouchPoint> touchPts = pTouchEvent->touchPoints();
		unsigned int id = 0;
		unsigned int tapCount = touchPts.size();

		osg::ref_ptr<osgGA::GUIEventAdapter> osgEvent;
		osgGA::GUIEventAdapter::TouchPhase phase = osgGA::GUIEventAdapter::TOUCH_UNKNOWN;
		for (auto& touchPt : touchPts)
		{
			if (!osgEvent)
			{
				if (event->type() == QEvent::TouchBegin)
				{
					phase = osgGA::GUIEventAdapter::TOUCH_BEGAN;
					osgEvent = m_window->getEventQueue()->touchBegan(id,
						osgGA::GUIEventAdapter::TOUCH_BEGAN,
						touchPt.pos().x(), touchPt.pos().y());
				}
				else if (event->type() == QEvent::TouchEnd)
				{
					phase = osgGA::GUIEventAdapter::TOUCH_ENDED;
					osgEvent = m_window->getEventQueue()->touchEnded(id,
						osgGA::GUIEventAdapter::TOUCH_ENDED,
						touchPt.pos().x(), touchPt.pos().y(),
						tapCount);
				}
				else if (event->type() == QEvent::TouchUpdate)
				{
					phase = osgGA::GUIEventAdapter::TOUCH_MOVED;
					osgEvent = m_window->getEventQueue()->touchMoved(id,
						osgGA::GUIEventAdapter::TOUCH_MOVED,
						touchPt.pos().x(), touchPt.pos().y());
				}
			}
			else
			{
				osgEvent->addTouchPoint(id, osgGA::GUIEventAdapter::TOUCH_ENDED,
					touchPt.pos().x(), touchPt.pos().y());
				osgEvent->addTouchPoint(id, phase, touchPt.pos().x(), touchPt.pos().y());
			}
			++id;
		}
		break;
	}
	default:
		break;
	}
	return QOpenGLWidget::event(event);
}

void osgQtView::setKeyboardModifiers(QInputEvent* event)
{
	int modkey = event->modifiers() & (Qt::ShiftModifier
		| Qt::ControlModifier | Qt::AltModifier);
	unsigned int mask = 0;
	if (modkey & Qt::ShiftModifier)
	{
		mask |= osgGA::GUIEventAdapter::MODKEY_SHIFT;
	}
	if (modkey & Qt::ControlModifier)
	{
		mask |= osgGA::GUIEventAdapter::MODKEY_CTRL;
	}
	if (modkey & Qt::AltModifier)
	{
		mask |= osgGA::GUIEventAdapter::MODKEY_ALT;
	}
	m_window->getEventQueue()->getCurrentEventState()->setModKeyMask(modkey);
	update();
}

void osgQtView::keyPressEvent(QKeyEvent* event)
{
	setKeyboardModifiers(event);
	m_window->getEventQueue()->keyPress(event->key());
	QOpenGLWidget::keyPressEvent(event);
	update();
}

void osgQtView::keyReleaseEvent(QKeyEvent* event)
{
	setKeyboardModifiers(event);
	m_window->getEventQueue()->keyPress(event->key());
	QOpenGLWidget::keyPressEvent(event);
	update();
}

void osgQtView::mousePressEvent(QMouseEvent* event)
{
	int button = 0;
	switch (event->button())
	{
	case Qt::LeftButton:
		button = 1;
		break;
	case Qt::MidButton:
		button = 2;
		break;
	case Qt::RightButton:
		button = 3;
		break;
	case Qt::NoButton:
		button = 0;
		break;
	default:
		button = 0;
		break;
	}
	setKeyboardModifiers(event);
	m_window->getEventQueue()->mouseButtonPress(event->x(), event->y(), button);
	update();
}

void osgQtView::mouseReleaseEvent(QMouseEvent* event)
{
	int button = 0;
	switch (event->button())
	{
	case Qt::LeftButton:
		button = 1;
		break;
	case Qt::MidButton:
		button = 2;
		break;
	case Qt::RightButton:
		button = 3;
		break;
	case Qt::NoButton:
		button = 0;
		break;
	default:
		button = 0;
		break;
	}
	setKeyboardModifiers(event);
	m_window->getEventQueue()->mouseButtonPress(event->x(), event->y(), button);
	QOpenGLWidget::mouseReleaseEvent(event);
	update();
}

void osgQtView::mouseDoubleClickEvent(QMouseEvent* event)
{
	int button = 0;
	switch (event->button())
	{
	case Qt::LeftButton:
		button = 1;
		break;
	case Qt::MidButton:
		button = 2;
		break;
	case Qt::RightButton:
		button = 3;
		break;
	case Qt::NoButton:
		button = 0;
		break;
	default:
		button = 0;
		break;
	}
	setKeyboardModifiers(event);
	m_window->getEventQueue()->mouseDoubleButtonPress(event->x(), event->y(), button);
	QOpenGLWidget::mouseDoubleClickEvent(event);
	update();
}

void osgQtView::mouseMoveEvent(QMouseEvent* event)
{
	setKeyboardModifiers(event);
	m_window->getEventQueue()->mouseMotion(event->x(), event->y());
	QOpenGLWidget::mouseMoveEvent(event);
	update();
}

void osgQtView::wheelEvent(QWheelEvent* event)
{
	setKeyboardModifiers(event);
	auto scroll = event->orientation() == Qt::Vertical ?
		(event->delta() > 0 ? osgGA::GUIEventAdapter::SCROLL_UP : osgGA::GUIEventAdapter::SCROLL_DOWN)
		:
		(event->delta() > 0 ? osgGA::GUIEventAdapter::SCROLL_LEFT : osgGA::GUIEventAdapter::SCROLL_RIGHT)
		;
	m_window->getEventQueue()->mouseScroll(scroll);
	QOpenGLWidget::wheelEvent(event);
}

void osgQtView::resizeEvent(QResizeEvent* e)
{
	auto& size = e->size();
	m_window->resized(x(), y(), size.width(), size.height());
	m_window->getEventQueue()->windowResize(x(), y(), size.width(), size.height());
	m_window->requestRedraw();

	QOpenGLWidget::resizeEvent(e);
}

void osgQtView::moveEvent(QMoveEvent* event)
{
	auto& pos = event->pos();
	m_window->resized(pos.x(), pos.y(), width(), height());
	m_window->getEventQueue()->windowResize(pos.x(), pos.y(), width(), height());

	QOpenGLWidget::moveEvent(event);
}

void osgQtView::timerEvent(QTimerEvent* e)
{
	update();
}

osgViewer::Viewer* osgQtView::getOsgViewer()
{
	return this;
}

osg::Group* osgQtView::getRoot()
{
	return m_rootGroup;
}

void osgQtView::paintGL()
{
	frame();
}

void osgQtView::initOsg()
{
	m_rootGroup = new osg::Group;
	//m_rootGroup->setName("Root");

	{
		osg::ref_ptr<osg::GraphicsContext::Traits> traits = new osg::GraphicsContext::Traits();
		traits->windowDecoration = true;
		traits->x = 0;
		traits->y = 0;
		traits->width = width();
		traits->height = height();
		traits->doubleBuffer = true;
		traits->sharedContext = 0;

		m_window = new osgViewer::GraphicsWindowEmbedded(traits);

		auto camera = getCamera();
		camera->setGraphicsContext(m_window);
		camera->setViewport(0, 0, traits->width, traits->height);
		camera->setClearMask(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		camera->setProjectionMatrixAsPerspective(
			30.0f, double(traits->width) / double(traits->height),
			1.0f, 10000.0f);
		camera->setClearColor(osg::Vec4(0.3, 0.3, 0.6, 0.1));
	}
	
	setCameraManipulator(new osgGA::TrackballManipulator());
	addEventHandler(new osgViewer::StatsHandler);
	addEventHandler(new osgViewer::ThreadingHandler);
	addEventHandler(new osgViewer::HelpHandler);
	addEventHandler(new osgGA::StateSetManipulator(getCamera()->getOrCreateStateSet()));
	setThreadingModel(osgViewer::ViewerBase::SingleThreaded);

	//��ȡţ��ģ��
	std::string modulefile = CodeConvert::wstr_to_utf8(appService::ins()->ResourcePath() + L"osg\\cow.osg");
	osg::ref_ptr<osg::Node> cow = osgDB::readNodeFile(modulefile);
	m_rootGroup->addChild(cow);

	m_rootGroup->getOrCreateStateSet()->setMode(GL_LIGHTING, osg::StateAttribute::ON);
	m_rootGroup->getOrCreateStateSet()->setMode(GL_DEPTH_TEST, osg::StateAttribute::ON);

	osgUtil::Optimizer opt;
	opt.optimize(m_rootGroup);

	setSceneData(m_rootGroup);
	realize();

	startTimer(10);
}
