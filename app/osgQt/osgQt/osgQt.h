#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_osgQt.h"

class osgQt : public QMainWindow
{
    Q_OBJECT

public:
    osgQt(QWidget *parent = Q_NULLPTR);

private:
    Ui::osgQtClass ui;
};
