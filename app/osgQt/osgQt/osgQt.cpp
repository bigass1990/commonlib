#include "stdafx.h"
#include "osgQt.h"

#include "osgQtView.h"

osgQt::osgQt(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    setCentralWidget(new osgQtView(this));
}
