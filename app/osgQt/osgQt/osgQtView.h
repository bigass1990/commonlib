#pragma once

#include <QOpenGLWidget>
#include <osgViewer/Viewer>
#include "qevent.h"

// https://www.itdaan.com/blog/2015/07/18/4b6287b10b736a7d55fc259bb9872768.html
// https://www.codenong.com/cs105360604/
class osgQtView : public QOpenGLWidget, public osgViewer::Viewer
{
	Q_OBJECT

public:
	osgQtView(QWidget *parent);
	~osgQtView();

	void initOsg();

public:
	bool event(QEvent* event) override;

	void setKeyboardModifiers(QInputEvent* event);

	void keyPressEvent(QKeyEvent* event) override;
	void keyReleaseEvent(QKeyEvent* event) override;
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void mouseDoubleClickEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void wheelEvent(QWheelEvent* event) override;
	void resizeEvent(QResizeEvent* e) override;
	void moveEvent(QMoveEvent* event) override;
	void timerEvent(QTimerEvent* e) override;

public:
	osgViewer::Viewer* getOsgViewer();
	osg::Group* getRoot();

protected:
	virtual void paintGL() override;

private:
	osg::ref_ptr<osg::Group> m_rootGroup;
	osgViewer::GraphicsWindow* m_window=nullptr;
};
