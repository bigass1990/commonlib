#pragma once

#include <string>

using u8_string = std::string;
using loc_string = std::string;
using w_string = std::wstring;

#ifdef STRINGDLL_EXPORTS
#define STRING_LIB_API __declspec (dllexport)
#else
#define STRING_LIB_API __declspec (dllimport)
#endif // STRINGDLL_EXPORTS


#include "..\stringDll\CodeConvert.h"
#include "..\stringDll\CodeEncoding.h"
#include "..\stringDll\StringUtils.h"



#pragma comment(lib, "stringDll.lib")