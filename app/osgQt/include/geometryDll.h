#pragma once

#ifdef GEOMETRYDLL_EXPORTS
#define Geometry_LIB_API __declspec (dllexport)
#else
#define Geometry_LIB_API __declspec (dllimport)
#endif // GEOMETRYDLL_EXPORTS

#include "..\geometryDll\geUtils.h"
#include "..\geometryDll\geColor.h"
#include "..\geometryDll\geMaterial.h"

#include "..\geometryDll\gePoint3d.h"
#include "..\geometryDll\geVector3d.h"
#include "..\geometryDll\geArc3d.h"
#include "..\geometryDll\geCircle3d.h"
#include "..\geometryDll\geLine3d.h"
#include "..\geometryDll\geLineSeg3d.h"
#include "..\geometryDll\geMatrix3d.h"
#include "..\geometryDll\gePlane.h"


#pragma comment(lib, "geometryDll.lib")