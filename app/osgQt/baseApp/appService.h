#pragma once


class Base_App_Export appService
{
public:
	static appService* ins();

	/// @brief 获取程序执行路径。例如：D:\osgQt\bin\
	/// @return 
	std::wstring ExePath() const;

	/// @brief 获取资源路径。例如：D:\osgQt\bin\resource\
	/// @return 
	std::wstring ResourcePath() const;

protected:
	appService();
};

