#include "pch.h"
#include "appService.h"


static std::wstring s_appExePath;

appService* appService::ins()
{
	static appService app;
	return &app;
}

std::wstring appService::ExePath() const
{
	return s_appExePath;
}

std::wstring appService::ResourcePath() const
{
	return s_appExePath + L"resource\\";
}

appService::appService()
{
	// 获取可执行路径
	try
	{
		wchar_t fullpath[MAX_PATH]{0};
		GetModuleFileNameW(nullptr, fullpath, MAX_PATH);
		std::wstring tmppath = fullpath;
		size_t pos =  tmppath.find_last_of(L'\\', tmppath.size());
		s_appExePath = tmppath.substr(0, pos + 1);
	}
	catch (std::exception e)
	{
		std::string err = e.what();
		err += " appService get module path fails!";
		throw std::exception(err.c_str());
	}
}
