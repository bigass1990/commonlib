#pragma once


#include <memory>
#include <vector>

const std::string emptyStr = "";
const std::string emptyStrU8 = u8"";
const std::wstring emptyWStr = L"";

using stringArray = std::vector<std::string>;
using wstringArray = std::vector<std::wstring>;

namespace StringUtils
{
	/// <summary>
	/// 字符串缓冲区。
	/// </summary>
	struct STRING_LIB_API Buffer
	{
		Buffer()
			:m_pBuf(nullptr), m_size(0)
		{}

		Buffer(char* buf, size_t size)
			:m_pBuf(buf), m_size(size)
		{}

		Buffer(Buffer&& buf) noexcept
		{
			m_pBuf = buf.m_pBuf;
			m_size = buf.m_size;
			buf.m_pBuf = nullptr;
			buf.m_size = 0;
		}

		~Buffer()
		{
			if (m_pBuf)
			{
				delete m_pBuf;
				m_pBuf = nullptr;
			}
		}

		char* m_pBuf;
		size_t m_size;

	private:
		Buffer& operator=(const Buffer&) { return *this; }
		Buffer(const Buffer& b) :m_pBuf(b.m_pBuf), m_size(b.m_size) {}
	};
}

namespace StringUtils
{
	// 判断字符串是否为数字
	STRING_LIB_API bool isnum(std::wstring s);

	/// <summary>
	/// 将double值按指定小数位数转换为字符串。
	/// </summary>
	/// <param name="data"></param>
	/// <param name="precison">小数点后的位数。</param>
	/// <returns>如果小数点后的位为0，则去掉。</returns>
	STRING_LIB_API std::string FromDouble(double data, unsigned int precison = 2);
	STRING_LIB_API std::wstring FromDoubleW(double data, unsigned int precison = 2);

	// 从右侧查找字符串中第一个不是数字的位置索引
	STRING_LIB_API int RFindFirstNotNum(const std::wstring& str);

	STRING_LIB_API inline std::string Format(const char* format, ...);

	/// <summary>
	/// 返回字符串str的数据。
	/// </summary>
	/// <param name="str"></param>
	/// <param name="size">返回字符串数组的大小。str.size() + 1</param>
	/// <returns>返回字符串数组指针。调用者管理内存。</returns>
	STRING_LIB_API char* StringData(const std::string& str, size_t& size);

	STRING_LIB_API bool StringData(const std::string& str, Buffer& buf);

	/// <summary>
	/// 转换大小写。
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	STRING_LIB_API std::string ToLower(const std::string& str);
	STRING_LIB_API std::string ToUpper(const std::string& str);
	STRING_LIB_API std::wstring ToLower(const std::wstring& str);
	STRING_LIB_API std::wstring ToUpper(const std::wstring& str);

	// 对比字符是否相等。（不管大小写）
	STRING_LIB_API bool CompareNoCase(const std::string& str1, const std::string& str2);
	STRING_LIB_API bool CompareNoCase(const std::wstring& str1, const std::wstring& str2);

	STRING_LIB_API size_t FindNoCase(const std::string& str, const std::string& sub);
	STRING_LIB_API size_t FindNoCase(const std::wstring& str, const std::wstring& sub);

	// 封装OutputDebugString
	// 定义 _DEBUG 宏，才有效。
	STRING_LIB_API void OutputDbgString(const char* pOutput);
	STRING_LIB_API void OutputDbgString(const wchar_t* pOutput);

	/// <summary>
	/// 分割字符串
	/// </summary>
	/// <param name="src">要分割的字符串</param>
	/// <param name="ch">分隔符</param>
	/// <returns>返回分割后的字符串数组</returns>
	STRING_LIB_API stringArray Split(const std::string& src, char ch);
	STRING_LIB_API wstringArray Split(const std::wstring& src, wchar_t ch);

	/// <summary>
	/// 裁剪str空格。
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	STRING_LIB_API std::string Trim(const std::string& str);
	STRING_LIB_API std::string LTrim(const std::string& str);
	STRING_LIB_API std::string RTrim(const std::string& str);

	STRING_LIB_API std::wstring Trim(const std::wstring& str);
	STRING_LIB_API std::wstring LTrim(const std::wstring& str);
	STRING_LIB_API std::wstring RTrim(const std::wstring& str);

	/// <summary>
	/// 返回utf8编码的字符串的字符数。
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	STRING_LIB_API size_t Length_utf8(const std::string& str);
};


template<typename ... Args>
static std::string str_format(const std::string& format, Args ... args)
{
	// std::snprintf 若成功则为会写入充分大缓冲区的字符数（不包含空终止字符）
	auto size_buf = std::snprintf(nullptr, 0, format.c_str(), args ...) + 1;
	std::unique_ptr<char[]> buf(new(std::nothrow) char[size_buf]);

	if (!buf)
		return emptyStr;

	std::snprintf(buf.get(), size_buf, format.c_str(), args ...);
	return std::string(buf.get(), buf.get() + size_buf - 1);
}

template<typename ... Args>
static std::wstring wstr_format(const std::wstring& format, Args ... args)
{
	// std::swprintf 若成功则为写入的宽字符数（不计终止空宽字符）
	auto size_buf = std::swprintf(nullptr, 0, format.c_str(), args ...) + 1;
	std::unique_ptr<wchar_t[]> buf(new(std::nothrow) wchar_t[size_buf]);

	if (!buf)
		return emptyWStr;

	std::swprintf(buf.get(), size_buf, format.c_str(), args ...);
	return std::wstring(buf.get(), buf.get() + size_buf - 1);
}

template<typename ... Args>
void DbgView(const std::string& format, Args ... args)
{
	std::string dgb = str_format(format, args...);

	StringUtils::OutputDbgString(dgb.c_str());
}

template<typename ... Args>
void DbgView(const std::wstring& format, Args ... args)
{
	std::wstring dgb = wstr_format(format, args...);

	StringUtils::OutputDbgString(dgb.c_str());
}