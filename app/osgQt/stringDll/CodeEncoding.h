#pragma once


enum class Encoding : unsigned int
{
	Undefined	= 0b0000000000000000,
	ASCII		= 0b0000000000010000, // 
	GBK			= 0b0000000000100000, // 
	Ansi		= 0b0000000011110000, // GBK�ȸ����Զ�����չASCII

	UTF8		= 0b0000000100010000, // utf8 without bom
	UTF8WithBOM = 0b0000001000000000, // utf8 with bom
	UTF16LE		= 0b0000010000000000, // utf16 С����
	UTF16BE		= 0b0000100000000000,
	UTF32LE		= 0b0001000000000000,
	UTF32BE		= 0b0010000000000000,
	Unicode		= 0b1111111100010000, // Unicode
};

class STRING_LIB_API CodeEncoding
{
public:
	/// <summary>
	/// ����ַ����ı��롣
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	static Encoding Check(const std::string& str);

	static Encoding BOMCheck(const unsigned char* bom, size_t size);

	/// <summary>
	/// �ж��Ƿ�Ϊutf8���롣
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	static bool IsUtf8(const std::string& str);

	/// <summary>
	/// �ж��Ƿ�ʱGBK���롣
	/// </summary>
	/// <param name="str"></param>
	/// <returns></returns>
	static bool IsGBK(const std::string& str, bool* pAllAscii = nullptr);
};

