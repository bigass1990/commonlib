#include "pch.h"
#include "objDatabase.h"

jm::objDatabase::objDatabase()
{
	
}

jm::objDatabase::objDatabase(const jm::objectId& dbId)
{
	m_id = std::make_shared<jm::objectId>(dbId);
}

jm::objectId jm::objDatabase::getId() const
{
	return *m_id;
}

void jm::objDatabase::startCommit(const w_string& commitName)
{
	m_startCommit.store(true);

	commitRecord record;
	record.m_name = commitName;
	m_subRedos.push_back(std::move(record));
}

void jm::objDatabase::endCommit()
{
	m_startCommit.store(false);
}

jm::objectId jm::objDatabase::addObj(objectBasePtr obj)
{
	if (!m_startCommit.load())
	{
		throw std::exception("not start a commit");
	}

	jm::objectId newId = jm::objectId::create();
	{
		auto iter = m_objIdToPtr.find(newId);
		while (iter != m_objIdToPtr.end())
		{
			newId = jm::objectId::create();
			iter = m_objIdToPtr.find(newId);
		}
	}
	
	{
		obj->setDb(m_id);
		obj->setId(newId);

		auto& record = m_subRedos.back();
		record.m_obj.insert_or_assign(newId, std::make_pair(obj, commitRecord::add));

		std::unique_lock ulock(m_objIdToPtr_s_mutex);
		m_objIdToPtr.insert_or_assign(newId, obj);
		
	}
	
	return newId;
}

jm::objectBaseMSPtr jm::objDatabase::open(const jm::objectId& objId, OpenType type)
{
	// 关闭时，复制一份添加到redo列表。并将复制出来的对象指针替换这里的。

	objectBasePtr tmpObjPtr;
	{
		std::shared_lock slock(m_objIdToPtr_s_mutex);
		auto iter = m_objIdToPtr.find(objId);
		if (iter != m_objIdToPtr.end())
		{
			tmpObjPtr = iter->second;
			slock.unlock();

			if (tmpObjPtr->open(type))
			{
				return objectBaseMSPtr(tmpObjPtr, type);
			}
		}
	}
	return objectBaseMSPtr(std::make_shared<objectBase>(), type);
}

bool jm::objDatabase::eraseObj(const jm::objectId& objId)
{
	auto iter = m_objIdToPtr.begin();
	{
		std::shared_lock slock(m_objIdToPtr_s_mutex);
		iter = m_objIdToPtr.find(objId);
	}
	
	if (iter != m_objIdToPtr.end())
	{
		auto& record = m_subRedos.back();
		record.m_obj.insert_or_assign(objId, std::make_pair(iter->second, commitRecord::del));

		std::unique_lock ulock(m_objIdToPtr_s_mutex);
		m_objIdToPtr.erase(iter);
	}
	return true;
}

