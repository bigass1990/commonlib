#include "pch.h"
#include "modelStream.h"

jm::dbResult jm::modelStream::LineSeg(const gePoint3d& start, const gePoint3d& end)
{
	return dbResult();
}

jm::dbResult jm::modelStream::Arc(const gePoint3d& center, const geVector3d& refDirOnArcPlane, const geVector3d& normalOfArcPlane, double radius, double startAngle, double endAngle)
{
	return dbResult();
}

jm::dbResult jm::modelStream::Circle(const gePoint3d& center, const geVector3d& normalOfCirclePlane, double radius)
{
	return dbResult();
}
