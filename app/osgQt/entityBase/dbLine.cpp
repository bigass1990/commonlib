#include "pch.h"
#include "dbLine.h"

jm::dbLine::dbLine(const gePoint3d& start, const gePoint3d& end)
	:m_startPt(start), m_endPt(end)
{
}

jm::gePoint3d jm::dbLine::startPt() const
{
	return m_startPt;
}

void jm::dbLine::setStartPt(const gePoint3d& pt)
{
	m_startPt = pt;
}

jm::gePoint3d jm::dbLine::endPt() const
{
	return m_endPt;
}

void jm::dbLine::setEndPt(const gePoint3d& pt)
{
	m_endPt = pt;
}

jm::dbResult jm::dbLine::transformBy(const geMatrix3d& xform)
{
	m_startPt.transformBy(xform);
	m_endPt.transformBy(xform);

	return dbResult::ok;
}

void jm::dbLine::updateModel()
{
}

jm::dbResult jm::dbLine::getGrips(gripAttris& gs) const
{
	gripAttri sg, eg;
	sg.m_pt = startPt();
	eg.m_pt = endPt();

	gs.push_back(sg);
	gs.push_back(eg);

	return dbResult::ok;
}

void jm::dbLine::draw(modelStream* pStream)
{
	pStream->LineSeg(startPt(), endPt());
}
