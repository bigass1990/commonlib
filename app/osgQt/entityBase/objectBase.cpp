#include "pch.h"
#include "objectBase.h"



jm::objectId jm::objectBase::getId() const
{
    return m_objId;
}

void jm::objectBase::setId(const objectId& objId)
{
    m_objId = objId;
}

void jm::objectBase::setDb(objectIdSPtr dbId)
{
    m_dbId = dbId;
}

jm::objectIdSPtr jm::objectBase::getDb() const
{
    if (m_dbId.expired())
    {
        return m_dbId.lock();
    }
    return objectIdSPtr();
}

bool jm::objectBase::open(OpenType type)
{
    if (type == OpenType::read)
    {
        m_s_mutex.lock_shared();
    }
    else if (type == OpenType::write)
    {
        m_s_mutex.lock();
    }
    else
    {
        return false;
    }
    return true;
}

void jm::objectBase::close(OpenType type)
{
    if (type == OpenType::read)
    {
        m_s_mutex.unlock_shared();
    }
    else if (type == OpenType::write)
    {
        m_s_mutex.unlock();
    }
}


