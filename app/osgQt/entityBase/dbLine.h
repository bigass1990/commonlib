#pragma once
#include "entityBase.h"


namespace jm
{
	/// @brief 表示一条线段。
	class Entity_Export dbLine : public entityBase
	{
	public:
		dbLine() = default;
		dbLine(const gePoint3d& start, const gePoint3d& end);

	public:

		/// @brief 线段起点。
		/// @return 
		gePoint3d startPt() const;
		void setStartPt(const gePoint3d& pt);

		/// @brief 线段终点
		/// @return 
		gePoint3d endPt() const;
		void setEndPt(const gePoint3d& pt);

	public:
		virtual dbResult transformBy(const geMatrix3d& xform) override;

		virtual void updateModel() override;

		virtual dbResult getGrips(gripAttris& gs) const override;

	protected:
		virtual void draw(modelStream* pStream) override;

	protected:
		gePoint3d m_startPt;
		gePoint3d m_endPt;
	};
	using dbLinePtr = std::shared_ptr<dbLine>;
}