#pragma once

namespace jm
{

/// @brief 模型流
class Entity_Export modelStream
{
public:

	/// @brief 线段
	/// @param start 起点
	/// @param end 终点。
	dbResult LineSeg(const gePoint3d& start, const gePoint3d& end);

	/// @brief 圆弧
	/// @param center 圆心
	/// @param refDirOnArcPlane 圆弧所在平面，圆弧起点的参考方向。
	/// @param normalOfArcPlane 圆弧所在平面的法向量。应该于refDirOnArcPlane垂直。
	/// @param radius 半径
	/// @param startAngle 起始角度。弧度。右手系，相对于refDirOnArcPlane的夹角。
	/// @param endAngle 结束角度。弧度。右手系，相对于refDirOnArcPlane的夹角。
	dbResult Arc(const gePoint3d& center, const geVector3d& refDirOnArcPlane, const geVector3d& normalOfArcPlane, double radius, double startAngle, double endAngle);

	/// @brief 圆
	/// @param center 圆心
	/// @param normalOfCirclePlane 圆所在平面的法向量。
	/// @param radius 半径。
	/// @return 
	dbResult Circle(const gePoint3d& center, const geVector3d& normalOfCirclePlane, double radius);

protected:
	osg::ref_ptr<osg::Drawable> m_model;
};

}

