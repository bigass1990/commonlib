#pragma once


namespace jm
{


struct Entity_Export gripAttri
{
	gePoint3d m_pt;
	gePoint3d m_normal;

	geColor m_color = geColor::green;
	bool m_edit = true; // �Ƿ������༭��
};

using gripAttris = std::vector<gripAttri>;

}
