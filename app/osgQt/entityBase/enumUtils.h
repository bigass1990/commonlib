#pragma once

namespace jm
{

enum class dbResult : int
{
	error            = 0,
	ok               = 1,

	invalidInput     = 100,
	outOfRange       = 101,

	nullEntityPtr    = 200,
	nullObjectPtr    = 201,
	nullPtr          = 202,

	notImplementYet  = 10000,

};

}