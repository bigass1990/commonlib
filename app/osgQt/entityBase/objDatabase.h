#pragma once

#include "objectBase.h"
#include "objectId.h"


namespace jm
{

/// @brief 表示一条commit记录
struct commitRecord
{
	enum status
	{
		add,
		modify,
		del,
	};
	std::map<jm::objectId, std::pair<objectBasePtr, status>> m_obj;
	w_string m_name;
};

enum class objType : unsigned int
{
	entity      = 1 << 0, // 实体--有几何模型。
	layer       = 1 << 1, // 图层--配置，无几何模型。
	style       = 1 << 2, // 样式--配置，无几何模型。

};

/// @brief 对象数据库。可以序列化、undo和redo
/// startCommit和endCommit成对出现。一次commit的所有增删改是一个commitRecord。
class Entity_Export objDatabase
{
public:
	objDatabase();
	objDatabase(const jm::objectId& dbId);

	jm::objectId getId() const;

public:
	
	/// @brief 启动一次提交。
	/// @param commitName 提交名称，可能要显示在undo、redo列表。可以重名，仅限20个字符。
	void startCommit(const w_string& commitName);

	/// @brief 结束一次提交。
	void endCommit();

	/// @brief 向数据库中添加对象。必须已经开始一个commit，否则 throw std::excettion。
	/// @param  
	/// @return 
	objectId addObj(objectBasePtr) noexcept(false);

	/// @brief 打开指定的对象。如果是写打开，必须已经开始一个commit，否则 throw std::excettion。
	/// @param objId 要打开的对象的id。
	/// @param type 打开方式。
	/// @return 返回定制指针，析构时，调用close。也可以由调用者手工调用close。
	objectBaseMSPtr open(const jm::objectId& objId, OpenType type);

	/// @brief 删除指定对象。必须已经开始一个commit，否则 throw std::excettion。
	/// @param objId 
	/// @return 
	bool eraseObj(const jm::objectId& objId);

public:
	/// @brief 保存数据。
	/// @param path 要保存的路径。
	/// @return 
	bool save(const std::wstring& path);

	bool load(const std::wstring& path);

protected:

	jm::objectIdSPtr m_id; // 数据库id。


	std::map<objectId, objectBasePtr> m_objIdToPtr; // 存储对象指针。
	std::shared_mutex m_objIdToPtr_s_mutex; // 存储对象指针的互斥量。

	std::atomic_bool m_startCommit;
	std::deque<commitRecord> m_subRedos; // 操作记录。
	std::deque<commitRecord> m_subUndos; // 撤销记录。
	
};
using objDatabasePtr = std::shared_ptr<objDatabase>;

}