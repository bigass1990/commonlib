#pragma once

namespace jm
{

	class objectId
	{
	public:
		objectId() = default;
		objectId(const u8_string& id) noexcept;
		objectId(const objectId& id) noexcept;
		objectId(objectId&& id) noexcept;

		objectId& operator=(const objectId& id) noexcept;
		objectId& operator=(objectId&& id) noexcept;

		bool operator<(const objectId& id) const noexcept;

	public:
		bool isValid() const;

		static objectId create() noexcept;

	protected:
		u8_string m_id;
	};

	using objectIdSPtr = std::shared_ptr<objectId>;
}