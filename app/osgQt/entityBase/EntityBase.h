#pragma once
#include "objectBase.h"
#include "gripAttri.h"


namespace jm
{
class modelStream;

	/// @brief 实体（有几何模型）基类。如果存储在数据库（objDatabase）中，将会保存时将会序列化。
	class Entity_Export entityBase : public objectBase
	{
	public:
		entityBase();
		
	public:

		virtual void close(OpenType type) override;

		/// @brief 矩阵变换。
		/// @param xform 
		/// @return 
		virtual dbResult transformBy(const geMatrix3d& xform) = 0;

		/// @brief 更新模型。实体被写打开后，关闭时会调用该函数。
		virtual void updateModel();

		/// @brief 获取实体夹点。
		/// @param gs 夹点数据。
		/// @return 
		virtual dbResult getGrips(gripAttris& gs) const;

	protected:
		/// @brief 绘制模型。
		virtual void draw(modelStream* pStream) = 0;

	protected:
		osg::ref_ptr<osg::Node> m_node;

	};
	using entityBasePtr = std::shared_ptr<entityBase>;

}