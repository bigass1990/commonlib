#include "pch.h"
#include "geArc3d.h"

namespace jm
{
geArc3d::geArc3d(const gePoint3d& center, double radius, double startAngle, double endAngle, const geVector3d& ref, const geVector3d& normal)
	:m_center(center), m_radius(radius), m_startAngle(startAngle), m_endAngle(endAngle), m_ref(ref), m_normal(normal)
{
}

void geArc3d::set(const gePoint3d& center, double radius, double startAngle, double endAngle, const geVector3d& ref, const geVector3d& normal)
{
	m_center = center;
	m_radius = radius;
	m_startAngle = startAngle;
	m_endAngle = endAngle;
	m_ref = ref;
	m_normal = normal;
}

gePoint3d geArc3d::getCenter() const
{
	return m_center;
}

void geArc3d::setCenter(const gePoint3d& center)
{
	m_center = center;
}

double geArc3d::getRadius() const
{
	return m_radius;
}

void geArc3d::setRadius(double radius)
{
	m_radius = radius;
}

double geArc3d::getStartAngle() const
{
	return m_startAngle;
}

void geArc3d::setStartAngle(double startAngle)
{
	m_startAngle = startAngle;
}

double geArc3d::getEndAngle() const
{
	return m_endAngle;
}

void geArc3d::setEndAngle(double endAngle)
{
	m_endAngle = endAngle;
}

geVector3d geArc3d::getRef() const
{
	return m_ref;
}

void geArc3d::setRef(const geVector3d& ref)
{
	m_ref = ref;
}

geVector3d geArc3d::getNormal() const
{
	return m_normal;
}

void geArc3d::setNormal(const geVector3d& normal)
{
	m_normal = normal;
}

geArc3d& geArc3d::transformBy(const geMatrix3d& xform)
{
	// TODO: �ڴ˴����� return ���
	m_center.transformBy(xform);
	m_ref.transformBy(xform);
	m_normal.transformBy(xform);
	return *this;
}

}