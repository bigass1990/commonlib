#pragma once
#include "geEntity.h"

namespace jm
{
class Geometry_LIB_API geLine3d : public geEntity
{
public:
	geLine3d() = default;
	geLine3d(const gePoint3d& origin, const geVector3d& dir) : m_origin(origin), m_dir(dir) {}

	gePoint3d getOrigin() const;
	void setOrigin(const gePoint3d& origin);

	geVector3d getDir() const;
	void setDir(const geVector3d& dir);

public:
	geLine3d& transformBy(const geMatrix3d& xform);


protected:
	gePoint3d m_origin;
	geVector3d m_dir;
};

using geLines = std::vector<geLine3d>;
}