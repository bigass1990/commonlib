#include "pch.h"
#include "geColor.h"


namespace jm
{
geColor geColor::red{ 255,0,0,1 };
geColor geColor::green{ 0,255,0,1 };
geColor geColor::blue{ 0,0,255,1 };
geColor geColor::white{ 255,255,255,1 };
geColor geColor::black{ 0,0,0,1 };
}