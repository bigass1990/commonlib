#include "pch.h"
#include "geLineSeg3d.h"


jm::gePoint3d jm::geLineSeg3d::getStartPt() const
{
	return m_startPt;
}

void jm::geLineSeg3d::setStartPt(const gePoint3d& start)
{
	m_startPt = start;
}

jm::gePoint3d jm::geLineSeg3d::getEndPt() const
{
	return m_endPt;
}

void jm::geLineSeg3d::setEndPt(const gePoint3d& end)
{
	m_endPt = end;
}

double jm::geLineSeg3d::length() const
{
	return m_startPt.distanceTo(m_endPt);
}

jm::geLineSeg3d& jm::geLineSeg3d::transformBy(const geMatrix3d& xform)
{
	// TODO: �ڴ˴����� return ���
	m_startPt.transformBy(xform);
	m_endPt.transformBy(xform);
	return *this;
}
