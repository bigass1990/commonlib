#include "pch.h"
#include "gePlane.h"

namespace jm
{

gePlane::gePlane(const gePoint3d& origin, const geVector3d& normal)
	: m_origin(origin), m_normal(normal.normal())
{
}

gePoint3d gePlane::getOrigin() const
{
	return m_origin;
}

void gePlane::setOrigin(const gePoint3d& origin)
{
	m_origin = origin;
}

geVector3d gePlane::getNormal() const
{
	return m_normal;
}

void gePlane::setNormal(const geVector3d& normal)
{
	m_normal = normal;
}

bool gePlane::isOn(const gePoint3d& pt, double prec/* = jm::precision*/)
{
	return jm::equalToZero(distance(pt), prec);
}

double gePlane::distance(const gePoint3d& pt)
{
	double d = -m_normal.dotProduct(m_origin.asVector());
	return m_normal.dotProduct(pt.asVector()) + d;
}

gePoint3d gePlane::closestPointTo(const gePoint3d& pt) const
{
	return gePoint3d();
}

gePlane& gePlane::transformBy(const geMatrix3d& xform)
{
	// TODO: �ڴ˴����� return ���
	m_origin.transformBy(xform);
	m_normal.transformBy(xform);
	return *this;
}

}