#pragma once

#include "geEntity.h"

namespace jm
{

/// @brief 表示圆弧。
class Geometry_LIB_API geArc3d : public geEntity
{
public:
	geArc3d() = default;
	geArc3d(const gePoint3d& center, double radius, double startAngle, double endAngle,
		const geVector3d& ref = geVector3d::kX, const geVector3d& normal = geVector3d::kZ);

	/// @brief 设置圆弧参数
	/// @param center 圆心
	/// @param radius 半径
	/// @param startAngle 圆弧起点到参考方向ref的角度。弧度。[0,2Π]
	/// @param endAngle 圆弧终点点到参考方向ref的角度。弧度。[0,2Π]
	/// @param ref 圆弧角度参考方向。
	/// @param normal 圆弧所在平面的法向量，应该与ref垂直。
	void set(const gePoint3d& center, double radius, double startAngle, double endAngle,
		const geVector3d& ref = geVector3d::kX, const geVector3d& normal = geVector3d::kZ);

	gePoint3d getCenter() const;
	void setCenter(const gePoint3d& center);

	double getRadius() const;
	void setRadius(double radius);

	double getStartAngle() const;
	void setStartAngle(double startAngle);

	double getEndAngle() const;
	void setEndAngle(double endAngle);

	geVector3d getRef() const;
	void setRef(const geVector3d& ref);

	geVector3d getNormal() const;
	void setNormal(const geVector3d& normal);

public:
	geArc3d& transformBy(const geMatrix3d& xform);

protected:
	gePoint3d m_center;
	double m_radius = 0.0;
	double m_startAngle = 0.0;
	double m_endAngle = 0.0;
	geVector3d m_normal;
	geVector3d m_ref;
};

using geArcs = std::vector<geArc3d>;
}