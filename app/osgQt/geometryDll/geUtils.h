#pragma once

namespace jm
{

Geometry_LIB_API extern const double PI; // PI = 3.14159265358979;

Geometry_LIB_API inline double DegreeToRadian(double degree) { return degree / 180.0 * PI; }
Geometry_LIB_API inline double RadianToDegree(double radian) { return radian / PI * 180.0; }
Geometry_LIB_API inline double DToR(double degree) { return degree / 180.0 * PI; }
Geometry_LIB_API inline double RToD(double radian) { return radian / PI * 180.0; }

Geometry_LIB_API extern const double precision; // precision = 1e-6

Geometry_LIB_API bool equal(double v1, double v2, double prec = precision);
Geometry_LIB_API bool equalToZero(double v1, double prec = precision);

Geometry_LIB_API bool less(double v1, double v2, double prec = precision);
Geometry_LIB_API bool lessOrEqual(double v1, double v2, double prec = precision);
Geometry_LIB_API bool lessOrEuqalToZero(double v1, double prec = precision);

Geometry_LIB_API bool large(double v1, double v2, double prec = precision);
Geometry_LIB_API bool largeOrEqual(double v1, double v2, double prec = precision);
Geometry_LIB_API bool largeOrEuqalToZero(double v1, double prec = precision);
}