#pragma once


namespace jm
{

struct Geometry_LIB_API geColor
{
	unsigned char r = 0; // 红色分量，0~255
	unsigned char g = 0; // 绿色分量，0~255
	unsigned char b = 0; // 蓝色分量，0~255
	unsigned char a = 1; // 透明度，0~1

	static geColor red;     // 红
	static geColor green;   // 绿
	static geColor blue;    // 蓝
	static geColor white;   // 白
	static geColor black;   // 黑
};

}
