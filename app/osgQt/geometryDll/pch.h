﻿// pch.h: 这是预编译标头文件。
// 下方列出的文件仅编译一次，提高了将来生成的生成性能。
// 这还将影响 IntelliSense 性能，包括代码完成和许多代码浏览功能。
// 但是，如果此处列出的文件中的任何一个在生成之间有更新，它们全部都将被重新编译。
// 请勿在此处添加要频繁更新的文件，这将使得性能优势无效。

#ifndef PCH_H
#define PCH_H

// 添加要在此处预编译的标头
#include "framework.h"




#include "common.h"


#include "..\..\..\commonlib\sdk\opengl\glm\glm0.9.9.9\glm\glm.hpp" // glm
#include "..\..\..\commonlib\sdk\opengl\glm\glm0.9.9.9\glm\ext.hpp" // glm

#ifdef GEOMETRYDLL_EXPORTS
#define Geometry_LIB_API __declspec (dllexport)
#else
#define Geometry_LIB_API __declspec (dllimport)
#endif // GEOMETRYDLL_EXPORTS

#include "geUtils.h"
#include "geEntity.h"
#include "geMatrix3d.h"
#include "gePoint3d.h"
#include "geVector3d.h"

#endif //PCH_H
