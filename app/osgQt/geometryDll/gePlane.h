#pragma once
#include "geEntity.h"
namespace jm
{
class Geometry_LIB_API gePlane : public geEntity
{
public:
	gePlane() = default;
	gePlane(const gePoint3d& origin, const geVector3d& normal);

	gePoint3d getOrigin() const;
	void setOrigin(const gePoint3d& origin);

	geVector3d getNormal() const;
	void setNormal(const geVector3d& normal);


public:

	/// @brief 判断点是否在平面上
	/// @param pt 
	/// @param prec 精度
	/// @return 
	bool isOn(const gePoint3d& pt, double prec = jm::precision);

	/// @brief 返回点到平面的距离
	/// @param pt 
	/// @return 返回值几何意义：等于零表示点在平面上；大于零表示点在平面法线方向一侧；小于零表示点在平面法线反向一侧。
	double distance(const gePoint3d& pt);

	/// @brief 获取指定点到平面的最近点。
	/// @param pt 
	/// @return 
	gePoint3d closestPointTo(const gePoint3d& pt) const;

	gePlane& transformBy(const geMatrix3d& xform);

protected:
	gePoint3d m_origin;
	geVector3d m_normal{ 0.0, 0.0 ,1.0 };
};

using gePlanes = std::vector<gePlane>;
}