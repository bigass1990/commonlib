#pragma once
#include "geEntity.h"
namespace jm
{
class Geometry_LIB_API geCircle3d : public geEntity
{
public:
	geCircle3d() = default;
	geCircle3d(const gePoint3d& center, double radius, const geVector3d& normal = geVector3d::kZ)
		: m_center(center), m_radius(radius), m_normal(normal) { }

	void set(const gePoint3d& center, double radius, const geVector3d& normal = geVector3d::kZ);

	gePoint3d getCenter() const;
	void setCenter(const gePoint3d& center);

	double getRadius() const;
	void setRadius(double radius);

	geVector3d getNormal() const;
	void setNormal(const geVector3d& normal);

public:
	geCircle3d& transformBy(const geMatrix3d& xform);

protected:
	gePoint3d m_center;
	double m_radius = 0.0;
	geVector3d m_normal;
};

using geCircles = std::vector<geCircle3d>;
}