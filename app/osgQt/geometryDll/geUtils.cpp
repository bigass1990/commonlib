#include "pch.h"
#include "geUtils.h"

namespace jm
{
	const double PI = 3.14159265358979;
	const double precision = 1e-6;

bool equal(double v1, double v2, double prec)
{
	return std::abs(v1 - v2) < prec;
}

bool equalToZero(double v1, double prec)
{
	return equal(v1, 0.0, prec);
}

bool less(double v1, double v2, double prec)
{
	if (equal(v1, v2, prec))
	{
		return false;
	}
	return v1 < v2;
}

bool lessOrEqual(double v1, double v2, double prec)
{
	if (equal(v1, v2, prec))
	{
		return true;
	}
	return v1 < v2;
}

bool lessOrEuqalToZero(double v1, double prec)
{
	if (equal(v1, 0.0, prec))
	{
		return true;
	}
	return v1 < 0.0;
}

bool large(double v1, double v2, double prec)
{
	if (equal(v1, v2, prec))
	{
		return false;
	}
	return v1 > v2;
}

bool largeOrEqual(double v1, double v2, double prec)
{
	if (equal(v1, v2, prec))
	{
		return true;
	}
	return v1 > v2;
}

bool largeOrEuqalToZero(double v1, double prec)
{
	if (equal(v1, 0.0, prec))
	{
		return true;
	}
	return v1 > 0.0;
}

}