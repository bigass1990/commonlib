#pragma once
#include "geEntity.h"

namespace jm
{
class Geometry_LIB_API geLineSeg3d : public geEntity
{
public:
	geLineSeg3d() = default;
	geLineSeg3d(const gePoint3d& start, const gePoint3d& end) : m_startPt(start), m_endPt(end) {}

	gePoint3d getStartPt() const;
	void setStartPt(const gePoint3d& start);

	gePoint3d getEndPt() const;
	void setEndPt(const gePoint3d& end);

public:

	double length() const;

	geLineSeg3d& transformBy(const geMatrix3d& xform);

protected:
	gePoint3d m_startPt;
	gePoint3d m_endPt;
};

using geLineSegs = std::vector<geLineSeg3d>;
}