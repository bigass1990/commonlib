#include "pch.h"
#include "geCircle3d.h"

namespace jm
{
void geCircle3d::set(const gePoint3d& center, double radius, const geVector3d& normal)
{
	m_center = center;
	m_radius = radius;
	m_normal = normal;
}

gePoint3d geCircle3d::getCenter() const
{
	return m_center;
}

void geCircle3d::setCenter(const gePoint3d& center)
{
	m_center = center;
}

double geCircle3d::getRadius() const
{
	return m_radius;
}

void geCircle3d::setRadius(double radius)
{
	m_radius = radius;
}

geVector3d geCircle3d::getNormal() const
{
	return m_normal;
}

void geCircle3d::setNormal(const geVector3d& normal)
{
	m_normal = normal;
}

geCircle3d& geCircle3d::transformBy(const geMatrix3d& xform)
{
	// TODO: �ڴ˴����� return ���
	m_center.transformBy(xform);
	m_normal.transformBy(xform);

	return *this;
}

}