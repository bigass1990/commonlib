#include "pch.h"
#include "gePoint3d.h"

namespace jm
{
double gePoint3d::distanceTo(const gePoint3d& pt) const
{
    return glm::distance(glm::vec3(pt.x, pt.y, pt.z), glm::vec3(this->x, this->y, this->z));
}
geVector3d gePoint3d::asVector() const
{
    return geVector3d(x, y, z);
}
gePoint3d& gePoint3d::transformBy(const geMatrix3d& xform)
{
    *this = geMatrix3d::preMult(xform, *this);
    return *this;
}

void gePoint3d::set(double _x, double _y, double _z)
{
    x = _x;
    y = _y;
    z = _z;
}

}