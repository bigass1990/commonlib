#pragma once

namespace jm
{
class geVector3d;
class gePoint3d;

class Geometry_LIB_API geMatrix3d
{
public:
	geMatrix3d(); // 默认是单位矩阵。单位矩阵通常是生成其他变换矩阵的起点。
	geMatrix3d(const geMatrix3d&);
	geMatrix3d(double x0, double y0, double z0, double w0,
		double x1, double y1, double z1, double w1,
		double x2, double y2, double z2, double w2,
		double x3, double y3, double z3, double w3);

	~geMatrix3d();

	geMatrix3d& operator=(const geMatrix3d&);

	static const geMatrix3d kIdintify; // 单位矩阵。

	/// @brief 缩放
	/// @param vec 
	/// @return 
	geMatrix3d& scaleBy(const geVector3d& vec);
	geMatrix3d& scaleBy(double scale);

	/// @brief 平移
	/// @param vec 
	/// @return 
	geMatrix3d& translateBy(const geVector3d& vec);

	/// @brief 旋转
	/// @param angle 弧度
	/// @param axis 旋转轴，建议单位向量。
	/// @return 
	geMatrix3d& rotateBy(double angle, const geVector3d& axis);

	/// @brief 转置
	/// @return 
	geMatrix3d& transposeBy();

	/// @brief 逆矩阵
	/// @return 
	geMatrix3d& inverse();

	static geMatrix3d loakAt(const geVector3d& eye, const geVector3d& center, const geVector3d& up);

	/// @brief 返回正射投影矩阵
	/// @param left 
	/// @param right 
	/// @param bottom 
	/// @param top 
	/// @return 
	static geMatrix3d ortho(double left, double right, double bottom, double top);

	/// @brief 返回透视投影矩阵
	/// @param fieldOfView 表示视野，设置了观察空间的大小。通常是四分之PI。即平截头体某一面的，近平面和远平面对应点的连线的夹角。
	/// @param aspect 宽高比。通常是视口的宽/高。
	/// @param _near 近平面。当near太大时，比如10.0，由于靠近相机的坐标被裁剪掉，导致太过靠近一个物体时，视线直接穿过去。
	/// @param _far 远平面
	/// @return 
	static geMatrix3d perspective(double fieldOfView, double aspect, double _near, double _far);

	static geMatrix3d scale(const geVector3d& vec);
	static geMatrix3d scale(double scale);

	static geMatrix3d translate(const geVector3d& vec);

	/// @brief 旋转
	/// @param angle 弧度
	/// @param axis 旋转轴，建议单位向量。
	/// @return 
	static geMatrix3d rotate(double angle, const geVector3d& axis);

	/// @brief 左乘 xform * vec
	/// 以右手系，旋转矩阵为例：左乘相当于逆时针旋转θ度；右乘相当于顺时针旋转θ度。
	/// @param xform 
	/// @param vec 内部会将3d向量变为(x,y,x,1)的4d向量进行计算。
	/// @return 返回变换后的向量。
	static geVector3d preMult(const geMatrix3d& xform, const geVector3d& vec);
	static gePoint3d preMult(const geMatrix3d& xform, const gePoint3d& vec);

	/// @brief 右乘 vec * xform
	/// 以右手系，旋转矩阵为例：左乘相当于逆时针旋转θ度；右乘相当于顺时针旋转θ度。
	/// @param xform 
	/// @param vec 内部会将3d向量变为(x,y,x,1)的4d向量进行计算。
	/// @return 返回变换后的向量。
	static geVector3d postMult(const geMatrix3d& xform, const geVector3d& vec);
	static gePoint3d postMult(const geMatrix3d& xform, const gePoint3d& vec);

	/// @brief 矩阵相乘。
	/// 在固定坐标系下面，用左乘（变换矩阵乘坐标矩阵）；在非固定的坐标系下面，用右乘（坐标矩阵乘变换矩阵）。
	/// 相对于动坐标系（新坐标系，或者以叫以自身为参考系）右乘。相对于静坐标系（变化过程中参考的坐标系始终不变）左乘。
	/// 矩阵左乘相当于行变换 矩阵右乘相当于列变换。
	/// 从几何角度可以这样理解：左乘结果是 向量旋转 之后相对于原坐标系的位置， 右乘是参考系旋转移动后，向量(并未移动)相对于新参考系的坐标。
	/// @param xform1 
	/// @param xform2 
	/// @return xform1 * xform2
	static geMatrix3d multiply(const geMatrix3d& xform1, const geMatrix3d& xform2);

	/// @brief 矩阵的数乘。矩阵与标量相乘。
	/// @param xform1 
	/// @param scalar 标量。
	/// @return 
	static geMatrix3d Multiply(const geMatrix3d& xform1, double scalar);

private:
	void* m_pxform;
};

using geMatrixs = std::vector<geMatrix3d>;
}