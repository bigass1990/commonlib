#include "pch.h"
#include "geLine3d.h"

namespace jm
{
gePoint3d geLine3d::getOrigin() const
{
	return m_origin;
}

void geLine3d::setOrigin(const gePoint3d& origin)
{
	m_origin = origin;
}

geVector3d geLine3d::getDir() const
{
	return m_dir;
}

void geLine3d::setDir(const geVector3d& dir)
{
	m_dir = dir;
}

geLine3d& geLine3d::transformBy(const geMatrix3d& xform)
{
	// TODO: �ڴ˴����� return ���
	m_origin.transformBy(xform);
	m_dir.transformBy(xform);
	return *this;
}

}