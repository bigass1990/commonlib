#pragma once

#include "geMatrix3d.h"

namespace jm
{
class geVector3d;

class Geometry_LIB_API gePoint3d
{
public:
	gePoint3d() = default;
	gePoint3d(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

	void set(double _x, double _y, double _z);

public:
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;

public:

	double distanceTo(const gePoint3d&) const;

	geVector3d asVector() const;

public:
	gePoint3d& transformBy(const geMatrix3d& xform);

};

using gePoint3ds = std::vector<gePoint3d>;
}