#pragma once

namespace jm
{
class Geometry_LIB_API geVector3d
{
public:
	geVector3d() = default;
	geVector3d(double _x, double _y, double _z) : x(_x), y(_y), z(_z) {}

	void set(double _x, double _y, double _z);
public:
	double x = 0.0;
	double y = 0.0;
	double z = 0.0;
	
	static geVector3d kX;
	static geVector3d kY;
	static geVector3d kZ;

public:

	/// @brief 获取该向量与指定向量的叉积（向量积）。
	/// @param  
	/// @return 
	geVector3d crossProduct(const geVector3d&) const;

	/// @brief 获取该向量与指定向量的点积（标量积）。
	/// @param  
	/// @return 
	double dotProduct(const geVector3d&) const;

	/// @brief 返回标准化向量。
	/// @return 
	geVector3d normal() const;

	/// @brief 标准化
	/// @return 
	geVector3d& normalize();

	geVector3d& operator*(double s);

public:

	double length() const;

	/// @brief 是否正交/垂直
	/// @param  
	/// @param prec 精度
	/// @return 
	bool isOrthogonal(const geVector3d&, double prec = jm::precision) const;

	/// @brief 是否平行
	/// @param  
	/// @param prec 
	/// @return 
	bool isParallel(const geVector3d&, double prec = jm::precision) const;

	/// @brief 是否同向
	/// @param  
	/// @param prec 
	/// @return 
	bool isCodirectional(const geVector3d&, double prec = jm::precision) const;

	geVector3d& transformBy(const geMatrix3d& xform);
};

using geVectors = std::vector<geVector3d>;
}