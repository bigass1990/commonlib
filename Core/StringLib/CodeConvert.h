#pragma once
#include <string>

/*windows平台的std::wstring 就是std::u16string, wchar_t 就是char16_t (utf-16编码)。window平台的终端编码一般是gbk。
 *linux平台的std::wstring就是std::u32string, wchar_t 就是char32_t (utf-32编码)*/
class CodeConvert
{
public:
	// std::string 转为 std::wstring( utf-8 --> wchar )
	// 注意：使用这个函数的时候需要求std::string的编码是utf-8，不然会抛异常。
	STRING_LIB_API static std::wstring utf8_to_wstr(const std::string& src);

	// std::wstring转为std::string(wchar --> utf-8)
	// 注意：转换后的获得的std::string的编码为utf-8，
	// windows下输出是乱码 （windows终端输出中文要正常显示，要转化为GBK编码）
	STRING_LIB_API static std::string wstr_to_utf8(const std::wstring& src);

	// utf-8转gbk
	STRING_LIB_API static std::string utf8_to_gbk(const std::string& str);

	// gbk转utf-8
	STRING_LIB_API static std::string gbk_to_utf8(const std::string& str);

	// gbk转std::wstring
	STRING_LIB_API static std::wstring gbk_to_wstr(const std::string& str);

	// wstring转gbk
	// gbk的代码页为936；gb18030的代码页为54936。大部分情况使用gbk足够，如果需要可使用gb18030.
	STRING_LIB_API static std::string wstr_to_gbk(const std::wstring& str, unsigned int codepage = 936);

	// wstring转默认代码页的ANSI
	STRING_LIB_API static std::string wstr_to_ansi(const std::wstring& str);

	// 默认代码页的ansi字符串转wstring（utf16 LE）
	STRING_LIB_API static std::wstring ansi_to_wstr(const std::string& str);

};



