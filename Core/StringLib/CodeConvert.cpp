#include "pch.h"
#include "CodeConvert.h"

#include<locale>
#include<codecvt>

// std::string ConvertFromUtf16ToUtf8(const std::wstring& wstr)
// {
// 	std::string convertedString;
// 	int requiredSize = WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, 0, 0, 0, 0);
// 	if (requiredSize > 0)
// 	{
// 		std::vector<char> buffer(requiredSize);
// 		WideCharToMultiByte(CP_UTF8, 0, wstr.c_str(), -1, &buffer[0], requiredSize, 0, 0);
// 		convertedString.assign(buffer.begin(), buffer.end() - 1);
// 	}
// 	return convertedString;
// }
// 
// std::wstring ConvertFromUtf8ToUtf16(const std::string& str)
// {
// 	std::wstring convertedString;
// 	int requiredSize = MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, 0, 0);
// 	if (requiredSize > 0)
// 	{
// 		std::vector<wchar_t> buffer(requiredSize);
// 		MultiByteToWideChar(CP_UTF8, 0, str.c_str(), -1, &buffer[0], requiredSize);
// 		convertedString.assign(buffer.begin(), buffer.end() - 1);
// 	}
// 
// 	return convertedString;
// }

/* 
 *https://blog.csdn.net/qq_31175231/article/details/83865059
 *c++11中新增的字符编码转换的功能，可以解决我们平时开发中字符编码转换的需求。主要使用wstring_convert和codecvt相结合进行转换。
 **/

std::wstring CodeConvert::utf8_to_wstr(const std::string& src)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;
	return converter.from_bytes(src);
}

std::string CodeConvert::wstr_to_utf8(const std::wstring& src)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> convert;
	return convert.to_bytes(src);
}

std::string CodeConvert::utf8_to_gbk(const std::string& str)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t> > conv;
	std::wstring tmp_wstr = conv.from_bytes(str);

	//GBK locale name in windows
	// GBK在linux下的locale名可能是"zh_CN.GBK"，而windows下是".936"），因此做跨平台的话仍然要给不同的系统做适配
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>> convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	return convert.to_bytes(tmp_wstr);
}

std::string CodeConvert::gbk_to_utf8(const std::string& str)
{
	//GBK locale name in windows
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>> convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	std::wstring tmp_wstr = convert.from_bytes(str);

	std::wstring_convert<std::codecvt_utf8<wchar_t>> cv2;
	return cv2.to_bytes(tmp_wstr);
}

std::wstring CodeConvert::gbk_to_wstr(const std::string& str)
{
	//GBK locale name in windows
	const char* GBK_LOCALE_NAME = ".936";
	std::wstring_convert<std::codecvt_byname<wchar_t, char, mbstate_t>> convert(new std::codecvt_byname<wchar_t, char, mbstate_t>(GBK_LOCALE_NAME));
	return convert.from_bytes(str);
}

std::string CodeConvert::wstr_to_gbk(const std::wstring& wstr, unsigned int codepage/* = 936*/)
{
	std::string convertedString;
	int requiredSize = WideCharToMultiByte(codepage, 0, wstr.c_str(), -1, 0, 0, 0, 0);
	if (requiredSize > 0)
	{
		convertedString.assign(requiredSize, '\0');
		WideCharToMultiByte(codepage, 0, wstr.c_str(), -1, &*convertedString.begin(), requiredSize, 0, 0);
	}
	return convertedString;
}

std::string CodeConvert::wstr_to_ansi(const std::wstring& wstr)
{
	std::string convertedString;
	int requiredSize = WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, 0, 0, 0, 0);
	if (requiredSize > 0)
	{
		convertedString.assign(requiredSize, '\0');
		WideCharToMultiByte(CP_ACP, 0, wstr.c_str(), -1, &*convertedString.begin(), requiredSize, 0, 0);
	}
	return convertedString;
}

std::wstring CodeConvert::ansi_to_wstr(const std::string& str)
{
	int nwLen = ::MultiByteToWideChar(CP_ACP, 0, str.c_str(), -1, NULL, 0);

	wchar_t* pwBuf = new wchar_t[nwLen + 1];//一定要加1，不然会出现尾巴 
	ZeroMemory(pwBuf, nwLen * 2 + 2);

	::MultiByteToWideChar(CP_ACP, 0, str.c_str(), (int)str.length(), pwBuf, nwLen);

	return std::wstring(std::move(pwBuf));
}
