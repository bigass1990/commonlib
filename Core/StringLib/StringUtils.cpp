#include "pch.h"
#include "StringUtils.h"
#include <sstream>
#include <vector>
#include <stdarg.h>
#include <cctype>
#include <cwctype>

#ifdef _DEBUG
#include <windows.h>
#endif
#include <locale>

#include <cmath>

using namespace std;

namespace StringUtils
{
	bool isnum(wstring s)
	{
		wstringstream sin(s);
		double t;
		wchar_t p;
		if (!(sin >> t))
		{
			/*解释：
				sin>>t表示把sin转换成double的变量（其实对于int和float型的都会接收），如果转换成功，则值为非0，如果转换不成功就返回为0
			*/
			return false;
		}
		if (sin >> p)
		{
			/*解释：此部分用于检测错误输入中，数字加字符串的输入形式（例如：34.f），在上面的的部分（sin>>t）已经接收并转换了输入的数字部分，在stringstream中相应也会把那一部分给清除，如果此时传入字符串是数字加字符串的输入形式，则此部分可以识别并接收字符部分，例如上面所说的，接收的是.f这部分，所以条件成立，返回false;如果剩下的部分不是字符，那么则sin>>p就为0,则进行到下一步else里面
			  */
			return false;
		}
		else
			return true;
	}

	std::string FromDouble(double data, unsigned int precison /*= 2*/)
	{
		if (precison > 0)
		{
			int d = (int)data;
			if (std::fabs(d - data) < 1 / std::pow(10, precison + 1))
			{
				precison = 0;
			}
		}
		
		std::stringstream ss;
		ss.setf(std::ios::fixed);
		ss.precision(precison);
		
		ss << data;

		return ss.str();
	}

	std::wstring FromDoubleW(double data, unsigned int precison/* = 2*/)
	{
		if (precison > 0)
		{
			int d = (int)data;
			if (std::fabs(d - data) < 1 / std::pow(10, precison + 1))
			{
				precison = 0;
			}
		}

		std::wstringstream ss;
		ss.setf(std::ios::fixed);
		ss.precision(precison);
		
		ss << data;

		return ss.str();
	}

	int RFindFirstNotNum(const std::wstring& str)
	{
		for (int i = str.size() - 1; i >= 0; --i)
		{
			wchar_t ch1;
			if (str.copy(&ch1, 1, i) == 1)
			{
				if (ch1 >= __T('0') && ch1 <= __T('9'))
				{
					continue;
				}
				else
				{
					return i;
				}
			}
		}
		return -1;
	}

	inline std::string format_string(const char* format, va_list args) {
		const size_t oldlen = BUFSIZ;
		char buffer[oldlen];  // 默认栈上的缓冲区
		va_list argscopy;
		va_copy(argscopy, args);
		size_t newlen = vsnprintf(&buffer[0], oldlen, format, args) + 1;
		newlen++;  // 算上终止符'\0'
		if (newlen > oldlen) {  // 默认缓冲区不够大，从堆上分配
			std::vector<char> newbuffer(newlen);
			vsnprintf(newbuffer.data(), newlen, format, argscopy);
			return newbuffer.data();
		}
		return buffer;
	}

	inline std::string Format(const char* format, ...) {
		va_list args;
		va_start(args, format);
		auto s = format_string(format, args);
		va_end(args);

		return s;
	}

	char* StringData(const std::string& str, size_t& size)
	{
		try
		{
			size = str.size() + 1;
			char* pBuf = new char[size];
			memcpy(pBuf, str.c_str(), size);
			return pBuf;
		}
		catch (const std::exception&)
		{
			size = 0;
			return nullptr;
		}
	}

	bool StringData(const std::string& str, Buffer& buf)
	{
		buf.m_pBuf = StringData(str, buf.m_size);

		return (buf.m_pBuf != nullptr);
	}

	std::string ToLower(const std::string& str)
	{
		std::string low = str;
		std::transform(str.begin(), str.end(), low.begin(),
			[](unsigned char c) { return std::tolower(c); });
		return low;
	}

	std::wstring ToLower(const std::wstring& str)
	{
		std::wstring low = str;
		std::transform(str.begin(), str.end(), low.begin(),
			[](wchar_t c) { return std::towlower(c); });
		return low;
	}

	std::string ToUpper(const std::string& str)
	{
		std::string up = str;
		std::transform(str.begin(), str.end(), up.begin(),
			[](unsigned char c) { return std::toupper(c); });
		return up;
	}

	std::wstring ToUpper(const std::wstring& str)
	{
		std::wstring up = str;
		std::transform(str.begin(), str.end(), up.begin(),
			[](wchar_t c) { return std::towupper(c); });
		return up;
	}

	bool CompareNoCase(const std::string& str1, const std::string& str2)
	{
		if (str1.size() == str2.size())
		{
			return std::equal(str1.begin(), str1.end(), str2.begin(), str2.end(),
				[](char ch1, char ch2)
				{return ch1 == ch2 || std::tolower(ch1) == std::tolower(ch2); }
			);
		}
		return false;
	}

	bool CompareNoCase(const std::wstring& str1, const std::wstring& str2)
	{
		if (str1.size() == str2.size())
		{
			return std::equal(str1.begin(), str1.end(), str2.begin(), str2.end(),
				[](wchar_t ch1, wchar_t ch2)
				{return ch1 == ch2 || std::towlower(ch1) == std::towlower(ch2); }
			);
		}
		return false;
	}

	size_t FindNoCase(const std::string& str, const std::string& sub)
	{
		std::string lowstr(std::move(StringUtils::ToLower(str)));
		std::string lowsub(std::move(StringUtils::ToLower(sub)));

		return lowstr.find(lowsub);
	}

	size_t FindNoCase(const std::wstring& str, const std::wstring& sub)
	{
		std::wstring lowstr(std::move(StringUtils::ToLower(str)));
		std::wstring lowsub(std::move(StringUtils::ToLower(sub)));

		return lowstr.find(lowsub);
	}

	void OutputDbgString(const char* pOutput)
	{
#ifdef _DEBUG
		OutputDebugStringA(pOutput);
#endif
	}

	void OutputDbgString(const wchar_t* pOutput)
	{
#ifdef _DEBUG
		OutputDebugStringW(pOutput);
#endif
	}

	stringArray Split(const std::string& src, char ch)
	{
		std::stringstream ss(src);
		std::string str;
		stringArray strArr;
		while (std::getline(ss, str, ch))
		{
			strArr.push_back(str);
		}
		return strArr;
	}

	wstringArray Split(const std::wstring& src, wchar_t ch)
	{
		std::wstringstream ss(src);
		std::wstring str;
		wstringArray strArr;
		while (std::getline(ss, str, ch))
		{
			strArr.push_back(str);
		}
		return strArr;
	}

	size_t get_utf8_length(const std::string& input)
	{
		// https://www.cnblogs.com/mengfanrong/p/3785791.html
		size_t length = 0;
		for (size_t i = 0, len = 0; i != input.length(); i += len) 
		{
			unsigned char byte = input[i];
			if (byte >= 0xFC) // lenght 6
				len = 6;
			else if (byte >= 0xF8)
				len = 5;
			else if (byte >= 0xF0)
				len = 4;
			else if (byte >= 0xE0)
				len = 3;
			else if (byte >= 0xC0)
				len = 2;
			else
				len = 1;
			length++;
		}
		return length;
	}

	bool IsNotSpace(const char& ch)
	{
		return !std::isspace(ch);
	}

	bool IsNotSpaceW(const wchar_t& ch)
	{
		return !std::isspace(ch);
	}

	std::string Trim(const std::string& s)
	{
		auto iter1 = std::find_if(s.begin(), s.end(), IsNotSpace);
		auto iter2 = std::find_if(s.rbegin(), s.rend(), IsNotSpace).base();
		return iter1 < iter2 ? std::string(iter1, iter2) : std::string(s);
		/*if (s.empty())
			return s;

		std::string str = s;

		str.erase(0, str.find_first_not_of(" "));
		str.erase(str.find_last_not_of(" ") + 1);
		return str;*/
	}

	std::string LTrim(const std::string& str)
	{
		auto iter = std::find_if(str.begin(), str.end(), IsNotSpace);
		return std::string(iter, str.end());
	}

	std::string RTrim(const std::string& str)
	{
		auto iter = std::find_if(str.rbegin(), str.rend(), IsNotSpace).base();
		return std::string(str.begin(), iter);
	}

	std::wstring Trim(const std::wstring& s)
	{
		auto iter1 = std::find_if(s.begin(), s.end(), IsNotSpaceW);
		auto iter2 = std::find_if(s.rbegin(), s.rend(), IsNotSpaceW).base();
		return iter1 < iter2 ? std::wstring(iter1, iter2) : std::wstring(s);
	}

	std::wstring LTrim(const std::wstring& str)
	{
		auto iter = std::find_if(str.begin(), str.end(), IsNotSpaceW);
		return std::wstring(iter, str.end());
	}

	std::wstring RTrim(const std::wstring& str)
	{
		auto iter = std::find_if(str.rbegin(), str.rend(), IsNotSpaceW).base();
		return std::wstring(str.begin(), iter);
	}

	size_t Length_utf8(const std::string& str)
	{
		return get_utf8_length(str);
	}

}