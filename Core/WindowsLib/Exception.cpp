#include "pch.h"
#include "Exception.h"
#include <stdlib.h>

#pragma comment(lib, "dbghelp.lib")

DumpFileManager* g_dumpManager = nullptr;

BOOL DumpFileManager::DeclarDumpFile(CString strAppPath)
{
	if (strAppPath.GetLength() <= 0)
	{
		return FALSE;
	}

	SYSTEMTIME syt;
	GetLocalTime(&syt);

	CString strDumpFileName;
	strDumpFileName.Format(_T("%s%s"), strAppPath, _T("dump\\"));

	int xFlag = 0;
	CString strNeedDir = _T("");
	do 
	{
		xFlag = strDumpFileName.Find(_T("\\"), xFlag + 1);
		if (xFlag > 3)
		{
			strNeedDir = strDumpFileName.Left(xFlag);
			if (PathIsDirectory(strNeedDir))
			{
				continue;
			}
			if (!(CreateDirectory(strNeedDir, 0)))
			{
				break;
			}
		}

	} while (xFlag >= 0);

	xFlag = syt.wHour * 60 + syt.wMinute;
	m_strDumpFile.Format(_T("%s%04d-%02d-%02d_%d.dmp"),
		strDumpFileName, syt.wYear, syt.wMonth, syt.wDay, xFlag);

	return TRUE;
}

void DumpFileManager::RunCrashHandle()
{
	g_dumpManager = nullptr;
	::SetUnhandledExceptionFilter(UnhandleExceptionFilterEx);
	PreventSetUnhandledExceptionFilter();
}

BOOL DumpFileManager::IsDataSectionNeeded(const WCHAR* pModuleName)
{
	WCHAR szFileName[_MAX_FNAME]{ L"" };
	if (nullptr == pModuleName)
	{
		return FALSE;
	}

	_wsplitpath_s(pModuleName, nullptr, _MAX_DRIVE, nullptr, _MAX_DIR, szFileName, _MAX_FNAME, nullptr, _MAX_EXT);
	if (_wcsicmp(szFileName, L"ntdll") == 0)
	{
		return TRUE;
	}
	return FALSE;
}

BOOL CALLBACK DumpFileManager::MiniDumpCallback(PVOID pParam,
	const PMINIDUMP_CALLBACK_INPUT pInput, PMINIDUMP_CALLBACK_OUTPUT pOutput)
{
	if (pInput == nullptr || pOutput == nullptr)
	{
		return FALSE;
	}

	switch (pInput->CallbackType)
	{
	case ModuleCallback:
	{
		if (pOutput->ModuleWriteFlags & ModuleWriteDataSeg)
		{
			if (!(g_dumpManager->IsDataSectionNeeded(pInput->Module.FullPath)))
			{
				pOutput->ModuleWriteFlags &= (~ModuleWriteDataSeg);
			}
		}
	}
	case IncludeModuleCallback:
	case IncludeThreadCallback:
	case ThreadCallback:
	case ThreadExCallback:
		return TRUE;
	default:
		break;
	}
	return FALSE;
}

BOOL DumpFileManager::CreateMiniDump(EXCEPTION_POINTERS* pEp, LPCTSTR strFileName)
{
	if (pEp == nullptr || strFileName == nullptr)
	{
		return FALSE;
	}

	HANDLE hFile = CreateFile(strFileName, GENERIC_WRITE, 0, nullptr,
		CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, nullptr);
	if (hFile != nullptr && (hFile != INVALID_HANDLE_VALUE))
	{
		MINIDUMP_EXCEPTION_INFORMATION mdei;
		mdei.ThreadId = GetCurrentThreadId();
		mdei.ExceptionPointers = pEp;
		mdei.ClientPointers = FALSE;

		MINIDUMP_CALLBACK_INFORMATION mci;
		mci.CallbackRoutine = (MINIDUMP_CALLBACK_ROUTINE)MiniDumpCallback;
		mci.CallbackParam = 0;

		MINIDUMP_TYPE mdt = (MINIDUMP_TYPE)0x0000ffff;

		MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hFile,
			MiniDumpNormal, &mdei, nullptr, &mci);
		
		CloseHandle(hFile);
		return TRUE;
	}
	return FALSE;
}

LPTOP_LEVEL_EXCEPTION_FILTER DumpFileManager::MyDummySetUnhandleExceptionFilter(LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter)
{
	return nullptr;
}

BOOL DumpFileManager::PreventSetUnhandledExceptionFilter()
{
	HMODULE hKernel32 = LoadLibrary(_T("kernel32.dll"));
	if (hKernel32 == nullptr)
	{
		return FALSE;
	}

	void* pOrgEntry = GetProcAddress(hKernel32, "SetUnhandledExceptionFilter");
	if (pOrgEntry == nullptr)
	{
		return FALSE;
	}

	DWORD dwOrgEntryAddr = (DWORD)pOrgEntry;
	dwOrgEntryAddr += 5; // add 5 for 5 op-codes for jmp far

	void* pNewFunc = &MyDummySetUnhandleExceptionFilter;
	DWORD dwNewEntryAddr = (DWORD)pNewFunc;
	DWORD dwRelativeAddr = dwNewEntryAddr - dwOrgEntryAddr;

	unsigned char newJump[100]{ 0 };
	newJump[0] = 0xE9; // JMP absolute
	memcpy(&newJump[1], &dwRelativeAddr, sizeof(pNewFunc));

	SIZE_T bytesWritten = 0;

	return WriteProcessMemory(GetCurrentProcess(), pOrgEntry, newJump, sizeof(pNewFunc) + 1, &bytesWritten);
}

LONG DumpFileManager::UnhandleExceptionFilterEx(struct _EXCEPTION_POINTERS* pException)
{
	if (pException == nullptr || g_dumpManager == nullptr)
	{
		return EXCEPTION_CONTINUE_SEARCH;
	}

	if (g_dumpManager->CreateMiniDump(pException, g_dumpManager->m_strDumpFile))
	{
		FatalAppExit(-1, _T("程序错误，小转储文件创建成功"));
	}
	else
	{
		FatalAppExit(-1, _T("程序错误，小转储文件创建失败"));
	}
	return EXCEPTION_CONTINUE_SEARCH;
}
