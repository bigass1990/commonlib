#include "pch.h"
#include "EventManual.h"

EventManual::EventManual(bool bCloseIfDestruct)
	:m_bCloseIfDestruct(bCloseIfDestruct)
{

}

EventManual::EventManual()
{

}

EventManual::~EventManual()
{
	if (m_bCloseIfDestruct && m_hEvent != nullptr)
	{
		Close();
	}
}

void EventManual::Creat(bool bInitSignaled, LPCWSTR lpname, bool bCloseIfDestruct)
{
	m_bCloseIfDestruct = bCloseIfDestruct;
	m_hEvent = ::CreateEventW(nullptr, TRUE, bInitSignaled, lpname);
}

void EventManual::SetSignaled(bool bSignaled)
{
	if (bSignaled)
	{
		::SetEvent(m_hEvent);
	}
	else
	{
		::ResetEvent(m_hEvent);
	}
}

void EventManual::Close()
{
	if (m_hEvent)
	{
		::CloseHandle(m_hEvent);
		m_hEvent = nullptr;
	}
}

EventManual::WaitResult EventManual::Wait(DWORD dwMilliseconds)
{
	DWORD rel = ::WaitForSingleObject(m_hEvent, dwMilliseconds);
	return static_cast<WaitResult>(rel);
}
