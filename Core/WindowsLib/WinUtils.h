#pragma once

// 构造时加载指定dll，析构时释放。
// 通过GetProcAddress加载指定函数
class CAccessLib
{
public:
	CAccessLib(const std::wstring& dllPath) { Handle = LoadLibraryW(dllPath.c_str()); }
	~CAccessLib() { if (Handle) ::FreeLibrary(Handle); }
	HINSTANCE Handle;

	// 获取指定名字的变量或函数指针。如果失败，返回NULL；如果成功，转换为自己的类型后使用。
	FARPROC GetProcAddr(LPCSTR lpProcName) { return ::GetProcAddress(Handle, lpProcName); }
};

#ifdef DBG_HELP
#include "DbgHelp.h"
#pragma comment(lib, "DbgHelp.lib")
#endif

namespace WinUtils
{
#ifdef DBG_HELP
	// 去掉MSVC编译的函数名修饰。
	// UnDecorateSymbolName VC6SP6 + PSDK自带, 将DbgHelp.h, DbgHelp.Lib
	// If the function succeeds, the return value is the number of characters in the UnDecoratedName buffer,
	// not including the NULL terminator.
	// 
	// DWORD IMAGEAPI UnDecorateSymbolName(
	// 	PCSTR name, // The decorated C++ symbol name.
	// 				// This name can be identified by the first character of the name, which is always a question mark (?).
	// 	PSTR  outputString, // A pointer to a string buffer that receives the undecorated name.
	// 	DWORD maxStringLength, // The size of the UnDecoratedName buffer, in characters.
	// 	DWORD flags // The options for how the decorated name is undecorated. 
	// );
	inline std::string GetUnDecorateSymbolName(const std::string& name, unsigned int flags = UNDNAME_COMPLETE, std::shared_ptr<DWORD> pError = nullptr)
	{
		constexpr size_t chSize = 512;
		std::string str(chSize, '\0');
		unsigned int size = UnDecorateSymbolName(name.data(), &*str.begin(), str.size(), flags);
		if (size > 0)
		{
			if (size <= chSize)
			{
				str.resize(size);
				return str;
			}
			else
			{
				str.resize(size, '\0');
				size = UnDecorateSymbolName(name.data(), &*str.begin(), str.size(), flags);
				return str;
			}
		}
		else
		{
			DWORD err = GetLastError();
			if (pError == nullptr)
				pError = std::make_shared<DWORD>(err);
			else
				*pError = err;
			return std::string();
		}
	}
#endif
}

