#include "pch.h"
#include "Sys.h"

bool sys::ExecuteCmd(const std::wstring& cmdLine, bool bShowWindow)
{
	unsigned int show = bShowWindow ? SW_SHOWNORMAL : SW_HIDE;
	unsigned int rel = WinExec((LPCSTR)cmdLine.c_str(), show);
	
	return rel > 31;
}

bool sys::ExecuteShell(HWND hwnd, const std::wstring& oper, const std::wstring& file, const std::wstring& parameters, const std::wstring& directory, bool bShowWindow)
{
	unsigned int show = bShowWindow ? SW_SHOWNORMAL : SW_HIDE;
	HINSTANCE rel = ShellExecute(hwnd, oper.c_str(), file.c_str(), parameters.c_str(), directory.c_str(), show);

	return (unsigned int)rel > 32;
}
