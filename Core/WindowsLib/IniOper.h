#pragma once

class WINDOWSLIB_API IniOper
{
public:
	IniOper(const std::wstring& file);
	~IniOper();

public:
	bool FindSection(const std::wstring& section);

	bool FindKey(const std::wstring& section, const std::wstring& key, std::wstring* pValue);

	bool SetKeyValue(const std::wstring& section, const std::wstring& key, const std::wstring& value);

	template <typename T>
	bool SetKeyValue(const std::wstring& section, const std::wstring& key, const T& value)
	{
		return SetKeyValue(section, key, std::to_wstring(value));
	}

private:
	std::wstring m_file;
};

