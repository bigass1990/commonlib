#pragma once

namespace sys
{
	/// <summary>
	/// 运行指定的程序。
	/// </summary>
	/// <param name="cmdLine">要执行程序的命令行。如果不包含路径，按下列顺序搜索可执行文件：
	///		应用程序所在目录、当前目录、win系统目录、win目录及PATH环境变量目录。</param>
	/// <param name="bShowWindow">true：隐藏窗口并激活其他窗口；false：激活并显示一个窗口。</param>
	/// <returns></returns>
	bool ExecuteCmd(const std::wstring& cmdLine, bool bShowWindow);

	/// <summary>
	/// 运行一个外部程序（或打开一个已注册的文件、目录，或打印一个文件等，并对外部程序
	/// 进行一定的控制。
	/// </summary>
	/// <param name="hwnd">用于显示UI或错误消息的父窗口句柄。如果操作不与窗口关联，
	///		可以为NULL。</param>
	/// <param name="oper">本例中称为动词，用于指定要执行的操作。常用的动词有：
	///		edit：启动编辑器并打开文档进行编辑。如果file不是文档，则失败。
	///		explore：探索由file指定的文件。
	///		find：在由directory指定的目录中启动搜索。
	///		open：打开由file指定的项目。可以时文件或文件夹。
	///		print：打印有file指定的文件。如果不是文档，则失败。
	///		NULL：如果可用，使用默认动词。如果不可用，使用“打开”动词。
	///			如果两个动词都不可用，则系统使用注册表中列出的第一个动词。
	/// </param>
	/// <param name="file">在其上执行指定动词的文件或对象。要制定shell名称控件对象，
	///		传递完全限定的解析名称。如果directory使用相对路径，file不能使用相对路径。
	/// </param>
	/// <param name="parameters">如果file指定一个可执行文件，则本参数指定要传递给程序的参数。
	///		如果file指定文档文件，则本参数为NULL。
	/// </param>
	/// <param name="directory">指定操作的默认目录。如果为NULL，使用当前的工作目录。
	///		如果file使用相对路径，本参数不能使用相对路径。
	/// </param>
	/// <param name="bShowWindow">是否显示窗口。</param>
	/// <returns></returns>
	bool ExecuteShell(HWND hwnd, const std::wstring& oper, const std::wstring& file,
		const std::wstring& parameters, const std::wstring& directory, bool bShowWindow);

	
}

