#include "pch.h"
#include "IniOper.h"

static constexpr unsigned int maxSection = 100;
static constexpr unsigned int maxKey = 100;
static constexpr unsigned int maxAllSection = maxSection * maxKey;


//unsigned short m_maxSection = (std::numeric_limits<unsigned short>::max)();

IniOper::IniOper(const std::wstring& file)
	:m_file(file)
{

}

IniOper::~IniOper()
{

}

bool IniOper::FindSection(const std::wstring& section)
{
	std::wstring buffer(L"", maxSection);
	DWORD rel = ::GetPrivateProfileStringW(section.c_str(), nullptr, nullptr,
		&*buffer.begin(), maxSection * sizeof(wchar_t), m_file.c_str());
	
	return rel > 0;
}

bool IniOper::FindKey(const std::wstring& section, const std::wstring& key, std::wstring* pValue)
{
	std::wstring buffer(L"", maxKey);
	DWORD rel = ::GetPrivateProfileStringW(section.c_str(), key.c_str(), nullptr,
		&*buffer.begin(), maxKey * sizeof(wchar_t), m_file.c_str());
	if (rel > 0)
	{
		if (pValue)
		{
			buffer.resize(rel + 1);
			buffer.shrink_to_fit();
			*pValue = buffer;
		}
		
		return true;
	}
	return false;
}

bool IniOper::SetKeyValue(const std::wstring& section, const std::wstring& key, const std::wstring& value)
{
	return ::WritePrivateProfileStringW(section.c_str(), key.c_str(), value.c_str(), m_file.c_str());
}
