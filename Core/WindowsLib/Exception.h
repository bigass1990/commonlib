#pragma once

// SetUnhandledExceptionFilter
/*
 可以注册一个异常处理函数，当一个异常产生且我们的 try - catch（或 try - expect）
 没有处理处理这个异常时，异常会转交给 SetUnhandledExceptionFilter，
 这是我们的应用程序处理异常的最后机会。
 如果有调试器，则不会执行这个函数。
 可以在回调函数中 转储程序崩溃时的内存。
 还可以通过该方法调试Windows崩溃时，源代码定位问题。

 标准C++中有一个类似的函数set_unexpected，区别是，只是当C++ 函数抛出一个
 不在它自己参数列表中的异常时才会触发，局限性比较大。
 */


#include "imagehlp.h"
/*
程序崩溃时，通过转储文件定位源码位置。
使用方法：
1. 在程序开头设置dump文件生成目录，并注册异常回调函数：
	TCHAR strTmp[MAX_PATH]{ 0 };
	TCHAR strAppPath[MAX_PATH]{ 0 };
	if (GetModuleFileName(nullptr, strTmp, MAX_PATH) > 0)
	{
		CString strPath(strTmp);
		int npos = strPath.ReverseFind('\\');
		if (npos > 0)
		{
			_tcscpy_s(strAppPath, strPath.Left(npos + 1));
		}
	}
	DumpFileManager dumpFileMgr;
	dumpFileMgr.DeclarDumpFile(strAppPath);
	dumpFileMgr.RunCrashHandle();
2. release模式下进行debug配置设置：
	C/C++选项 常规 调试信息格式 设置程序数据库/Zi
	C/C++选项 优化 禁用
	连接器选项 调试 生成调试信息 设置为是
3. 将dbghelp.dll以及程序的exe、dll、pdb等文件放在同一目录。
4. 崩溃时，生成转储文件。双击“*.dump”文件，选择“使用仅限本机进行调试”，进行问题定位。
*/
class DumpFileManager
{
public:
	// 指定小转储文件保存路径
	BOOL DeclarDumpFile(CString strAppPath);

	// 注册异常处理函数
	void RunCrashHandle();

private:
	CString m_strDumpFile; // 要生成的小转储文件的名称。
private:
	// 判断是否为需要的数据区域。
	BOOL IsDataSectionNeeded(const WCHAR* pModuleName);

	// 发生错误时的回调函数。
	static BOOL CALLBACK MiniDumpCallback(PVOID pParam, const PMINIDUMP_CALLBACK_INPUT pInput,
		PMINIDUMP_CALLBACK_OUTPUT pOutput);

	// 创建小转储文件。
	BOOL CreateMiniDump(EXCEPTION_POINTERS* pEp, LPCTSTR strFileName);

	// 钩子函数
	static LPTOP_LEVEL_EXCEPTION_FILTER WINAPI MyDummySetUnhandleExceptionFilter(
		LPTOP_LEVEL_EXCEPTION_FILTER lpTopLevelExceptionFilter);

	// 钩子函数
	BOOL PreventSetUnhandledExceptionFilter();

	// 回调函数
	static LONG WINAPI UnhandleExceptionFilterEx(struct _EXCEPTION_POINTERS* pException);
};