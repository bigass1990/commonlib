#pragma once

// 手动事件
class EventManual
{
public:
	// 构造时仅初始化数据成员，没有创建/打开事件。
	EventManual();
	EventManual(bool bCloseIfDestruct);
	~EventManual();

	enum class WaitResult : unsigned long
	{
		Object_0 = 0x00000000L, // 等待时间内变为有信号了。
		TimeOut = 0x00000102L, // 等待超时了。
		Failed = 0xFFFFFFFF, // 出现错误。
		Abandoned = 0x00000080L, // 不会使用。
	};
public:
	
	// 创建事件。
	void Creat(bool bInitSignaled, LPCWSTR lpname, bool bCloseIfDestruct);
	
	// 设置有信号/无信号
	void SetSignaled(bool bSignaled);

	// 关闭信号
	void Close();

	/// <summary>
	/// 等待事件的信号状态。被调用时，线程暂时挂起，如果挂起的dwMillliseconds毫秒内，
	/// 变为有信号状态，则该函数立即返回；如果已达到dwMillliseconds毫秒，
	/// 但仍然没有变成有信号状态，函数同样返回。
	/// </summary>
	/// <param name="dwMilliseconds">
	/// 若为0，立即返回；若为INFINITE，线程一直挂起，直到变为有信号状态为止。
	/// </param>
	WaitResult Wait(DWORD dwMilliseconds);

	// 返回事件句柄
	HANDLE GetHandle() { return m_hEvent; }

private:
	bool m_bCloseIfDestruct = true; // 析构时是否关闭事件。
	HANDLE m_hEvent = nullptr;
};

