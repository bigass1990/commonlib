﻿// WindowsLib.cpp : 定义 DLL 的导出函数。
//

#include "pch.h"
#include "framework.h"
#include "WindowsLib.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// 唯一的应用程序对象

CWinApp theApp;

using namespace std;

int main()
{
    int nRetCode = 0;

    HMODULE hModule = ::GetModuleHandle(nullptr);

    if (hModule != nullptr)
    {
        // 初始化 MFC 并在失败时显示错误
        if (!AfxWinInit(hModule, nullptr, ::GetCommandLine(), 0))
        {
            // TODO: 在此处为应用程序的行为编写代码。
            wprintf(L"错误: MFC 初始化失败\n");
            nRetCode = 1;
        }
        else
        {
            // TODO: 在此处为应用程序的行为编写代码。
        }
    }
    else
    {
        // TODO: 更改错误代码以符合需要
        wprintf(L"错误: GetModuleHandle 失败\n");
        nRetCode = 1;
    }

    return nRetCode;
}

bool IsProgressAlreadRun(const std::wstring& mutexName)
{
    /*
        通常进程时相互独立的。同一个进程可以重复运行。但特殊情况下，
    程序在系统中需要只保存一份进程实例。
        微软提供了CreateMutex函数创建或打开一个已命名或未命名的互斥对象。
    程序每次运行时，通过判断系统中是否存在相同命名的互斥对象来确定程序是否
    重复运行。    
    */

    HANDLE hMutex = CreateMutex(NULL, FALSE, mutexName.c_str());
    if (hMutex)
    {
        if (::GetLastError()== ERROR_ALREADY_EXISTS)
        {
            return TRUE;
        }
    }

    // 这里不能关闭互斥对象。
    //CloseHandle(hMutex);
    return FALSE;
}
