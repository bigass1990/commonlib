#pragma once

using ulong = unsigned long;

enum class RegRoot : ulong
{
	ClassRoot = (ulong)HKEY_CLASSES_ROOT,
	CurrentUser = (ulong)HKEY_CURRENT_USER,
	LocalMachine = (ulong)HKEY_LOCAL_MACHINE,
	User = (ulong)HKEY_USERS,
	CurrentConfig = (ulong)HKEY_CURRENT_CONFIG,
};

enum class RegAccess : ulong
{
	CreateLink = (ulong)KEY_CREATE_LINK, // 准许生成符号键
	CreateSubKey = (ulong)KEY_CREATE_SUB_KEY, // 准许生成子健
	EnumSubKeys = (ulong)KEY_ENUMERATE_SUB_KEYS, // 准许生成枚举子健
	Execute = (ulong)KEY_EXECUTE, // 准许进行读操作
	Notify = (ulong)KEY_NOTIFY, // 准许更换通告
	QueryValue = (ulong)KEY_QUERY_VALUE, // 准许查询子健

	AllAccess = (ulong)KEY_ALL_ACCESS, // 提供完全访问，上面键值的组合

	SetValue = (ulong)KEY_SET_VALUE, // 准许设置子健的数值
	Read = (ulong)KEY_READ, // 上面Query、Enum、Notify的组合
	Write = (ulong)KEY_WRITE, // SetValue、CreateSubKey的组合
	Wow64_32Key = (ulong)KEY_WOW64_32KEY,// 64位win系统中程序应该在32位注册表上运行。32位win系统忽略该标志
	Wow64_64Key = (ulong)KEY_WOW64_64KEY, // 64位win系统中程序应该在64位注册表上运行。32位win系统忽略该标志
};

enum class RegDataType
{
	Binary = REG_BINARY, // 任何形式的二进制数
	Dword= REG_DWORD,// 一个32位数字
	DwordLE = REG_DWORD_LITTLE_ENDIAN,// 一个格式为“低字节在前”的32为数字
	DwordBE = REG_DWORD_BIG_ENDIAN,// 一个格式为“高字节在前”的32为数字
	ExpandSZ = REG_EXPAND_SZ,// 以0结尾的字符串，该字符串包含环境变量（如"PATH"）。
	Link = REG_LINK, // 一个Unicode格式的带符号链接
	MultiSZ = REG_MULTI_SZ, // 以0结尾的字符串，该数组以连续两个0作为终止符
	//One = REG_ONE,
	ResourceList = REG_RESOURCE_LIST,// 设备驱动器资源列表
	SZ= REG_SZ, // 一个以0结尾的字符串。
};

class WINDOWSLIB_API RegisterOper
{
public:

	RegisterOper();
	RegisterOper(RegRoot root, const std::wstring& subKey, RegAccess access = RegAccess::Write);
	~RegisterOper();

	bool isValid() { return m_hKey != nullptr; }

	bool SetValue(const std::wstring& valueName, int data);
	bool SetValue(const std::wstring& valueName, double data);
	bool SetValue(const std::wstring& valueName, const std::wstring& data);
	//bool SetValue(LPCTSTR lpValueName, const std::wstring& data);

public:
	/// <summary>
	/// 打开一个指定的注册表键
	/// </summary>
	/// <param name="root"></param>
	/// <param name="subKey"></param>
	/// <param name="access">对指定键希望得到的访问权限标记</param>
	/// <param name="phkResult">保存打开注册表键的句柄。如果不再使用，需要RegCloseKey来关闭。</param>
	/// <returns>成功则返回0；否则失败。</returns>
	static long OpenKey(RegRoot root, const std::wstring& subKey, RegAccess access, PHKEY phkResult, std::wstring* pErr = nullptr);

	/// <summary>
	/// 在指定的注册表项下设置指定值的数据和类型。
	/// </summary>
	/// <param name="hKey">已打开项的句柄或标准项名</param>
	/// <param name="valueName">欲设置值的名称。如果不存在于指定的注册表中，则会加入该项。</param>
	/// <param name="dataType">指定存储的数据类型。</param>
	/// <param name="data">指定存储数据。</param>
	/// <returns>成功则返回0；否则失败。</returns>
	static long SetValue(HKEY hKey, const std::wstring& valueName, RegDataType dataType, const std::wstring& data, std::wstring* pErr = nullptr);

private:
	HKEY m_hKey = nullptr;
};

namespace sys
{
	/*
		windows提供了开机自启动注册表。每次开机完成后，都会在这个注册表键值下遍历键值，
		以获取键值中的程序路径，并创建进程启动程序。
		HKEY_CURRENT_USER\Software\Microsoft\Windows\CurrentVersion\Run
		HKEY_LOCAL_MACHINE\Software\Microsoft\Windows\CurrentVersion\Run
		CurrentUser键值需要用户默认权限；LocalMachine需要管理员权限。
	*/

	WINDOWSLIB_API bool RegCurrentUser(const std::wstring& fileName, const std::wstring& valueName);
}