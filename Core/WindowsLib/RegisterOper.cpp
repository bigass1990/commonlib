#include "pch.h"
#include "RegisterOper.h"

RegisterOper::RegisterOper()
{

}

RegisterOper::RegisterOper(RegRoot root, const std::wstring& subKey, RegAccess access /*= (RegAccess)((ulong)RegAccess::Read | (ulong)RegAccess::Write | (ulong)RegAccess::Wow64_64Key)*/)
{
	PHKEY pKey = nullptr;
	if (OpenKey(root, subKey, access, pKey) == 0)
	{
		m_hKey = *pKey;
	}
}

RegisterOper::~RegisterOper()
{
	if (m_hKey)
	{
		RegCloseKey(m_hKey);
	}
}

void GetErrorMessage(long relustId, std::wstring& buffer)
{
	//std::wstring buffer(L"", 256);
	// FormatMessageW返回值不包含末尾的'\0'
	DWORD dSize = FormatMessageW(FORMAT_MESSAGE_FROM_SYSTEM, NULL, relustId, NULL, &*buffer.begin(), 256, NULL);
	if (dSize > 0 && dSize < 256)
	{
		buffer.resize(dSize + 1);
		buffer.shrink_to_fit();
		//*pErr = buffer;
	}
}

long RegisterOper::OpenKey(RegRoot root, const std::wstring& subKey, RegAccess access, PHKEY phkResult, std::wstring* pErr/* = nullptr*/)
{
	LONG rel = ::RegOpenKeyExW((HKEY)root, subKey.c_str(), 0, (REGSAM)access, phkResult);
	if (rel != 0)
	{
		if (pErr)
		{
			GetErrorMessage(rel, *pErr);
		}
	}
	return rel;
}

long RegisterOper::SetValue(HKEY hKey, const std::wstring& valueName,
	RegDataType dataType, const std::wstring& data, std::wstring* pErr/* = nullptr*/)
{
	LONG rel = ::RegSetValueExW(hKey, valueName.c_str(), 0, (DWORD)dataType, (const BYTE*)data.c_str(), (data.size()+1)*sizeof(wchar_t));
	if (pErr)
	{
		GetErrorMessage(rel, *pErr);
	}
	return rel;
}

bool RegisterOper::SetValue(const std::wstring& valueName, int data)
{
	if (isValid())
	{
		return RegisterOper::SetValue(m_hKey, valueName, RegDataType::Dword, std::to_wstring(data));
	}
	return false;
}

bool RegisterOper::SetValue(const std::wstring& valueName, double data)
{
	if (isValid())
	{
		return RegisterOper::SetValue(m_hKey, valueName, RegDataType::Dword, std::to_wstring(data));
	}
	return false;
}

bool RegisterOper::SetValue(const std::wstring& valueName, const std::wstring& data)
{
	if (isValid())
	{
		return RegisterOper::SetValue(m_hKey, valueName, RegDataType::Dword, data);
	}
	return false;
}

bool sys::RegCurrentUser(const std::wstring& fileName, const std::wstring& valueName)
{
	LPCTSTR lpSub = _T("\\Software\\Microsoft\\Windows\\CurrentVersion\\Run");
	RegisterOper oper(RegRoot::CurrentUser, lpSub);
	if (oper.isValid())
	{
		//std::string fileName = CodeConvert::wstr_to_gbk(std::wstring(lpszFileName));
		return oper.SetValue(valueName, fileName);
	}
	return false;
}
