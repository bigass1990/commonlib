#pragma once

#include "ObjectFactory.h"
#include "BaseObject.h"
#include "DbObjectId.h"


template <class _Ty>
class DbObjectPtr : public std::shared_ptr<_Ty>
{
public:

	DbObjectPtr(std::nullptr_t nullptr_t) noexcept
		:std::shared_ptr<_Ty>(nullptr_t) {}

	DbObjectPtr(const std::shared_ptr<_Ty>& _other) noexcept
		:std::shared_ptr<_Ty>(_other) {}

	~DbObjectPtr() noexcept
	{
		if (this->get()->IsAddedToDb() && this->get()->IsClosed())
		{
			this->get()->Close();
		}

		//this->_Decref();
	}

	DbObjectPtr() = delete;

	DbObjectPtr(const DbObjectPtr& _Other) noexcept
	{
		this->_Copy_construct_from(_Other);
	}

	DbObjectPtr& operator=(const DbObjectPtr& _Other) noexcept
	{
		this->_Copy_construct_from(_Other);
		return *this;
	}

	DbObjectPtr(DbObjectPtr&& _Other) noexcept
	{
		this->_Move_construct_from(std::move(_Other));
	}

	DbObjectPtr& operator=(DbObjectPtr&& _Other) noexcept
	{
		this->_Move_construct_from(std::move(_Other));
		return *this;
	}

	std::shared_ptr<_Ty> toShared() noexcept
	{
		DbObjectPtr<_Ty> tmp = *this;
		return tmp;
	}

};


class CDatabase;

class CORE_DB_ENTITY CDbObject : public CBaseObject, public DynamicCreator<CDbObject, CDatabase*>
{
	friend CDatabase;
public:
	CDbObject(CDatabase* pDb = nullptr);
	virtual ~CDbObject();

	bool IsAddedToDb() const;

	bool IsClosed() const;

	void Close();

	CDbObjectId GetID() const;

private:
	CDbObjectId m_dbId;
	unsigned char m_readSize = 0;
	unsigned char m_writeSize = 0;
};


using CDbObjectPtr = DbObjectPtr<CDbObject>;

/// <summary>
/// 根据类名字符串，创建同名类实例。
/// </summary>
/// <typeparam name="T">类名字符串对应的类名</typeparam>
/// <typeparam name="...Targs">类构造函数的参数</typeparam>
/// <param name="strTypeName">类名字符串</param>
/// <returns></returns>
template<typename T, typename...Targs>
DbObjectPtr<T> CreateDbObject(const std::string& strTypeName, Targs... args)
{
	return DbObjectPtr<T>(dynamic_cast<T*>(CObjectFactory<Targs...>::Instance()->Create(strTypeName, std::forward<Targs>(args)...)));
}