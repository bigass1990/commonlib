#include "pch.h"
#include "DbObject.h"
#include "Database.h"

CDbObject::CDbObject(CDatabase* pDb)
{

}


CDbObject::~CDbObject()
{

}

bool CDbObject::IsAddedToDb() const
{
	return m_dbId.IsValid();
}

bool CDbObject::IsClosed() const
{
	return false;
}

void CDbObject::Close()
{
	
}

CDbObjectId CDbObject::GetID() const
{
	return m_dbId;
}
