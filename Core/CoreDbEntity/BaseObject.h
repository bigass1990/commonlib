#pragma once
#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

class CORE_DB_ENTITY CBaseObject : public std::enable_shared_from_this<CBaseObject>
{
public:
	CBaseObject();


};

using CBaseObjectPtr = std::shared_ptr<CBaseObject>;

#pragma warning(pop)