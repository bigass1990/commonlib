#pragma once


class CORE_DB_ENTITY CDbObjectId
{
public:
	CDbObjectId();

	CDbObjectId(unsigned long id);

public:
	bool IsValid() const;

	explicit operator bool() const;

	int operator==(const CDbObjectId&) const;
	bool operator<(const CDbObjectId&) const;

public:
	static CDbObjectId kInvalid;
private:
	unsigned long m_uId;
};

