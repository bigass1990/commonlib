#pragma once


enum class DbOpen
{
	eForRead,
	eForWrite,
};


enum class ResultType
{
	eOK,
	eInvalidInput,
	eInvalidKey,
	eOutOfMemory,
	eNotYetImplement,
	eNotApplication,
	eHasOpenForWrite,
	eHasOpenMaxReadCount,
};
