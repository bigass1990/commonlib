#include "pch.h"
#include "DbObjectId.h"

bool CDbObjectId::operator<(const CDbObjectId& other) const
{
	return m_uId < other.m_uId;
}

CDbObjectId CDbObjectId::kInvalid = 0;

CDbObjectId::CDbObjectId(unsigned long id)
	:m_uId(id)
{
	
}

CDbObjectId::CDbObjectId()
	:m_uId(0)
{

}

bool CDbObjectId::IsValid() const
{
	return m_uId > 0;
}

CDbObjectId::operator bool() const
{
	return IsValid();
}

int CDbObjectId::operator==(const CDbObjectId& other) const
{
	if (m_uId == other.m_uId)
		return 0;

	return (m_uId > other.m_uId ? 1 : -1);
}


