#pragma once
#include "DbObject.h"
#include "EnumUtils.h"

#pragma warning(push)
#pragma warning(disable:4251)

namespace std
{
	template<>
	struct less<CDbObjectId>
	{
		bool operator()(const CDbObjectId& _Left, const CDbObjectId& _Right) const 
		{
			return _Left < _Right;
		}
	};
}

class CORE_DB_ENTITY CDatabase
{
	
public:

	CDbObjectId AddToDb(CDbObjectPtr pDbObj, bool bClosedIfAdded);

	ResultType Open(const CDbObjectId& id, DbOpen type, CDbObjectPtr& ptr);
	CDbObjectPtr Open(const CDbObjectId& id, DbOpen type);


private:
	std::map < CDbObjectId, CDbObjectPtr, std::less<CDbObjectId> > m_mapIdToDbobj;
};

#pragma warning(pop)