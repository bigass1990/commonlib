#pragma once
#include <thread>
#include <numeric>

namespace ThreadUtils
{
	// 量产线程，等待它们结束
	void do_work(unsigned id) {}
	void test_f()
	{
		std::vector<std::thread> threads;
		for (unsigned i = 0; i < 20; ++i)
		{
			threads.push_back(std::thread(do_work, i));
		}
		
		// 对每个线程调用join()。
		std::for_each(threads.begin(), threads.end(),
			std::mem_fn(&std::thread::join));
	}

	// 并行版std::accumulate
	// 计算累计值。C++17 std::reduce
	template<typename Iterator, typename T>
	struct accumulate_block
	{
		void operator()(Iterator first, Iterator last, T& result)
		{
			result = std::accumulate(first, last, result);
		}
	};

	template<typename Iterator, typename T>
	T acumulate(Iterator first, Iterator last, T init)
	{
		unsigned long const length = std::distance(first, last);
		if (!length)
			return init;

		unsigned long const min_per_thread = 25;
		unsigned long const max_threads = (length + min_per_thread - 1) / min_per_thread;
		unsigned long const hardware_threads = std::thread::hardware_concurrency();
		unsigned long const num_threads = min(hardware_threads != 0 ? hardware_threads : 2, max_threads);
		unsigned long const block_size = length / num_threads;

		std::vector<T> result(num_threads);
		std::vector<std::thread> threads(num_threads - 1);

		Iterator block_start = first;
		for (unsigned long i = 0; i < (num_threads - 1); ++i)
		{
			Iterator block_end = block_start;
			std::advance(block_end, block_size); // 将某个迭代器前进到指定的位置上

			threads[i] = std::thread(accumulate_block<Iterator, T>(),
				block_start, block_end, std::ref(result[i]));

			block_start = block_end;
		}

		// 计算最后剩余的值
		accumulate_block<Iterator, T>()(block_start, last, result[num_threads - 1]);

		std::for_each(threads.begin(), threads.end(), std::mem_fn(&std::thread::join));
		// 计算所有累积的值
		return std::accumulate(result.begin(), result.end(), init);
	}
}
