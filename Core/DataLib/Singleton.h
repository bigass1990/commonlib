#pragma once

/// <summary>
/// 线程安全的单例模式模板。
/// </summary>
/// <typeparam name="T"></typeparam>
template<typename T>
class Singleton
{
public:
	static T* Instance() 
	{
		static T ins;
		return &ins;
	}
};

// 示例
class TmpMgr : public Singleton<TmpMgr>
{
	friend class Singleton<TmpMgr>;

private:
	TmpMgr() = default;
	TmpMgr(const TmpMgr&) = default;

public:
	int GetTmp() { return 10; }
};

TmpMgr* pMgr = TmpMgr::Instance();
//ColorMgr mgr(*pMgr);
int a = pMgr->GetTmp();
