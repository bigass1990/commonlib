#pragma once
#include <deque>
#include <mutex>


// 线程安全队列。封装std::queue

template<class T>
class ThreadSafeQueue
{
	using Container = std::deque<T>;

public:
	ThreadSafeQueue() = default;

public:

	template<class Element>
	void Push(Element&& element)
	{
		std::lock_guard<std::mutex> lock(m_mutex);
		m_queue.push(std::forward<Element>(element));
		m_notEmptyCV.notify_one();
	}

	void WaitAndPop(T& t)
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_notEmptyCV.wait(lock,
			[]() {return !m_queue.empty(); });

		t = std::move(m_queue.front());
		m_queue.pop();
	}

	std::shared_ptr<T> WaitAndPop()
	{
		std::unique_lock<std::mutex> lock(m_mutex);
		m_notEmptyCV.wait(lock,
			[this]() {return !m_queue.empty(); });

		std::shared_ptr<T> p = std::make_shared<T>(m_queue.front());
		m_queue.pop();
		return p;
	}

private:
	ThreadSafeQueue(const ThreadSafeQueue&) = delete;
	ThreadSafeQueue(ThreadSafeQueue&&) = delete;

	ThreadSafeQueue& operator = (const ThreadSafeQueue&) = delete;
	ThreadSafeQueue& operator = (ThreadSafeQueue&&) = delete;

private:
	Container m_queue;

	std::condition_variable m_notEmptyCV;
	mutable std::mutex m_mutex;
};

