#pragma once

// https://www.jianshu.com/p/24b1a7045bea
// https://blog.csdn.net/mvp_Dawn/article/details/110591053
// 判断指定类是否有某个函数。
#define HAS_MEMBER_FUNC(XXX) \
template<typename T, typename... Args>\
struct has_member_func_##XXX \
{ \
private:  \
  template<typename U> static auto Check(int) -> decltype(std::declval<U>().XXX(std::declval<Args>()...), std::true_type());  \
  template<typename U> static std::false_type Check(...); \
public: \
  static constexpr auto value = decltype(Check<T>(0))::value; \
}

// 判断指定类是否有某个成员变量。
#define HAS_MEMEBER_VARIABLE(xxx) \
typename <typename T> \
struct has_member_variable \
{ \
private: \
  template <typename U> static auto Check(int) -> decltype(std::declval<U>().xxx, std::true_type()); \
  template <typename U> static std::false_type Check(...); \
public: \
  static constexpr auto value = decltype(Check<T>(0))::value; \
}

// https://zhuanlan.zhihu.com/p/26155469
// std::void_t是c++17引入的，之前的版本使用自定义的
// 判断类型T是否有type成员类型
template<typename T, typename = void>
struct has_type : std::false_type {};

template<typename T>
struct has_type<T, std::void_t<typename T::type>> : std::true_type {};