#include "pch.h"
#include "ThreadAutoJoin.h"


std::thread t([] {return; });
//ThreadAutoJoin athread(t);

ThreadAutoJoin athreada(std::thread([] {return; }));

void a()
{
	std::thread t([] {return; });
	//ThreadAutoJoin athread(t); // error

	ThreadAutoJoin athreada(std::thread([] {return; })); // ok

	std::vector<ThreadAutoJoin> v;
	v.push_back(ThreadAutoJoin(std::thread([] {return; }))); // ok
}