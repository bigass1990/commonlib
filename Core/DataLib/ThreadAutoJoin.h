#pragma once

#pragma warning(push)
#pragma warning(disable:4251)

#include <thread>

/*
	析构时自动join的线程封装。

	使用示例：

	std::thread t([] {return; });
	//ThreadAutoJoin a(t); // error

	ThreadAutoJoin b(std::thread([] {return; })); // ok

	std::vector<ThreadAutoJoin> v;
	v.push_back(ThreadAutoJoin(std::thread([] {return; }))); // ok
*/
class Data_API ThreadAutoJoin
{
	std::thread m_t;
public:
	explicit ThreadAutoJoin(std::thread _t)
		: m_t(std::move(_t)) {}

	ThreadAutoJoin(ThreadAutoJoin&& other) noexcept
		:m_t(std::move(other.m_t))
	{
	}

	ThreadAutoJoin& operator=(ThreadAutoJoin&& other) noexcept
	{
		m_t = std::move(other.m_t);
		return *this;
	}

	~ThreadAutoJoin()
	{
		if (m_t.joinable())
		{
			m_t.join();
		}
	}

	ThreadAutoJoin(ThreadAutoJoin const&) = delete;
	ThreadAutoJoin& operator=(ThreadAutoJoin const&) = delete;
};

#pragma warning(pop)