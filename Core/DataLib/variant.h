#pragma once
#include <typeindex>
#include <functional>
#include "function_traits.h"

namespace ex
{
	//// 找到最大的typesize
	//template<typename T, typename... Args>
	//struct MaxType : std::integral_constant<int,
	//	(sizeof(T) > MaxType<Args...>::value ? sizeof(T) : MaxType<Args...>::value)>
	//{
	//};

	//template <typename T>
	//struct MaxType<T> : std::integral_constant<int, sizeof(T)>
	//{
	//};

	//// 判断类型列表中是否包含某种类型
	//template <typename T, typename... List>
	//struct Contains : std::true_type
	//{
	//};

	//template <typename T, typename Head, typename... Rest>
	//struct Contains<T, Head, Rest...>
	//	: std::conditional_t<std::is_same_v<T, Head>, std::true_type,
	//	Contains<T, Rest...>>
	//{
	//};

	//template<typename T>
	//struct Contains<T> : std::false_type
	//{
	//};

	//// 通过placement new 在缓冲区创建对象。
	//template<typename... Args>
	//struct VariantHelper;

	//template<typename T, typename...Args>
	//struct VariantHelper<T, Args...>
	//{
	//	inline static void Destory(std::type_index id, void* data)
	//	{
	//		if (id == type_index(typeid(T)))
	//		{
	//			((T*)(data))->~T();
	//		}
	//		else
	//		{
	//			VariantHelper<Args...>::Destroy(id, data);
	//		}
	//	}
	//};

	//template<>
	//struct VariantHelper<>
	//{
	//	inline static void Destroy(std::type_index id, void* data) {}
	//};

	//template<typename... Types>
	//class Variant
	//{
	//	using Helper_t = VariantHelper<Types...>;
	//public:
	//	Variant() : m_typeIndex(typeid(void)) {}
	//	~Variant() { Helper_t::Destory(m_typeIndex, &m_data); }
	//
	//	template<typename T>
	//	bool Is() { return m_typeIndex == std::type_index((typeid(T))); }

	//	template<typename T>
	//	T& Get()
	//	{
	//		if (!Is<T>())
	//		{
	//			std::string err = str_format("%s is not defined. current type is %s", typeid(T).name(), m_typeIndex.name());
	//			throw std::bad_cast(err.data());
	//		}
	//		return *(T*)(&m_data);
	//	}

	//	template<class T, class = typename std::enable_if_t<
	//		Contains<typename std::remove_reference_t<T>, Type...>>>
	//		Variant(T&& value)
	//		: m_typeIndex(std::type_index(typeid(void)))
	//	{
	//		Helper_t::Destory(m_typeIndex, &m_data);
	//		using U = typename std::remove_reference_t<T>;
	//		new (m_data) U(std::forward<T>(value));
	//		m_typeIndex = std::type_index(typeid(T));
	//	}

	//	template<typename F>
	//	void Visit(F&& f)
	//	{
	//		using T = typename function_traits<F>::arg<0>::type;
	//		if (Is<T>())
	//			f(Get<T>());
	//	}

	//	template<typename F, typename... Rest>
	//	void Visit(F&& f, Rest&&... rest)
	//	{
	//		using T = typename function_traits<F>::arg<0>::type;
	//		if (Is<T>())
	//			Visit(std::forward<F>(f));
	//		else
	//			Visit(std::forward<Rest>(rest)...);
	//	}
	//
	//private:
	//	char m_data[MaxType<Types...>::value];
	//	std::type_index m_typeIndex;
	//};
}


