﻿#ifndef SRC_UTILS_AES_ENCRYPTOR_H
#define SRC_UTILS_AES_ENCRYPTOR_H

// https://www.cnblogs.com/xumaojun/p/8528995.html


class AES;

class ENCRYPT_ALGORITHM_API AesEncryptor
{
public:
    AesEncryptor(unsigned char* key);
    ~AesEncryptor(void);

    std::string EncryptString(std::string strInfor);
    std::string DecryptString(std::string strMessage);
    // 加密
    void EncryptTxtFile(const char* inputFileName, const char* outputFileName);
    // 解密
	void DecryptTxtFile(const char* inputFileName, const char* outputFileName);
	void DecryptTxtFile(const char* inputFileName, std::ofstream& of);

private:
    void Byte2Hex(const unsigned char* src, int len, char* dest);
    void Hex2Byte(const char* src, int len, unsigned char* dest);
    int  Char2Int(char c);

private:
    AES* m_pEncryptor;
};

#endif        // SRC_UTILS_AES_ENCRYPTOR_H