﻿#pragma once

#include <string>

class Base64
{
public:
	// ---------------------------cocoa-start-----------------------------/
	//  base64.h
	//  CPPWork
	//  from http://stackoverflow.com/questions/180947/base64-decode-snippet-in-c
	//  Created by cocoa on 16/8/5.
	//  Copyright © 2016年 cc. All rights reserved.

	// 转化为base64编码
	ENCRYPT_ALGORITHM_API static std::string Encode(unsigned char const* bytes_to_encode, unsigned int in_len);
	ENCRYPT_ALGORITHM_API static std::string Encode(const std::string& bytes_to_encode);
	
	// 从base64编码转化为字符串。
	ENCRYPT_ALGORITHM_API static std::string Decode(const std::string& encoded_string);
	// ---------------------------cocoa-end-------------------------------/


};

