#pragma once
#include <chrono>

// 耗时
class  TimeConsuming
{
public:
	TimeConsuming() :m_curTimePoint(std::chrono::steady_clock::now()) {}
	
	// 重新开始计时
	void Reset() { m_curTimePoint = std::chrono::steady_clock::now(); }

	// 已过去的时间。毫秒。
	long long Elapsed() const
	{
		auto curTime = std::chrono::steady_clock::now();
		auto elapse = std::chrono::duration_cast<std::chrono::milliseconds>(curTime - m_curTimePoint);
		return elapse.count();
	}
private:
	std::chrono::steady_clock::time_point m_curTimePoint;
};

namespace MathUtils
{

	extern const double g_dTol/* = 1e-20*/; // double 比较时的误差

	// 比较d1是否等于d2
	MATH_LIB_API bool Equal(const double& d1, const double& d2, const double& dTol = g_dTol);

	// 比较d1是否大于d2
	MATH_LIB_API bool LargerThan(const double& d1, const double& d2, const double& dTol = g_dTol);

	// 比较d1是否大于或等于d2
	MATH_LIB_API bool LargerThanOrEqual(const double& d1, const double& d2, const double& dTol = g_dTol);

	// 比较d1是否小于d2
	MATH_LIB_API bool LessThan(const double& d1, const double& d2, const double& dTol = g_dTol);

	// 比较d1是否小于或等于d2
	MATH_LIB_API bool LessThanOrEqual(const double& d1, const double& d2, const double& dTol = g_dTol);

	// 线性插值。
	// x0和x1不能相等。内部没有判断。
	MATH_LIB_API double LinearInterp(double x0, double y0, double x1, double y1, double x);

	// 伪随机数
	enum class RandomType : unsigned char
	{
		Clock_Now = 0, // 以当前系统时间作为种子 std::chrono::system_clock::now().time_since_epoch().count()
		Device = 1, // 以设备作为种子。std::random_device
	};
	MATH_LIB_API unsigned int Random(RandomType type = RandomType::Clock_Now);
	MATH_LIB_API unsigned long long Random_64(RandomType type = RandomType::Clock_Now);

	// uuid guid 32个16进制数。
	// 如 "63dc162d866f4a2c7dfe3208756e70f6"
	MATH_LIB_API std::string guid();
	MATH_LIB_API std::string uuid();

	// 判断std::vector是否有重复数据
	template<typename Vector>
	bool HasDuplicatedData(const Vector& vec, bool bIsSorted = false)
	{
		Vector tmp(vec);
		if (!bIsSorted)
		{
			std::sort(tmp.begin(), tmp.end());
		}
		return (std::unique(tmp.begin(), tmp.end()) != tmp.end());
	}

	template<typename Vector, typename Iter = Vector::iterator>
	Iter GetDuplicatedData(Vector& vec, bool bIsSorted = false)
	{
		if (!bIsSorted)
		{
			std::sort(vec.begin(), vec.end());
		}
		return std::unique(vec.begin(), vec.end());
	}
}
