#include "pch.h"
#include "MathUtils.h"
#include <random>

namespace MathUtils
{

	static const double g_dTol = 1e-20;

	bool Equal(const double& d1, const double& d2, const double& dTol/* = g_dTol*/)
	{
		return (fabs(d1 - d2) < fabs(dTol));
	}

	bool LargerThan(const double& d1, const double& d2, const double& dTol/* = g_dTol*/)
	{
		if (Equal(d1, d2, dTol))
			return false;

		return d1 > d2;
	}

	bool LargerThanOrEqual(const double& d1, const double& d2, const double& dTol/* = g_dTol*/)
	{
		if (Equal(d1, d2, dTol))
			return true;

		return d1 > d2;
	}

	bool LessThan(const double& d1, const double& d2, const double& dTol/* = g_dTol*/)
	{
		if (Equal(d1, d2, dTol))
			return false;

		return d1 < d2;
	}

	bool LessThanOrEqual(const double& d1, const double& d2, const double& dTol/* = g_dTol*/)
	{
		if (Equal(d1, d2, dTol))
			return true;

		return d1 < d2;
	}

	double LinearInterp(double x0, double y0, double x1, double y1, double x)
	{
		double y = y0 + (x - x0) * ((y1 - y0) / (x1 - x0));
		return y;
	}

	unsigned int Random(RandomType type/* = RandomType::Clock_Now*/)
	{
		// 伪随机生成32位数字
		if (type == RandomType::Clock_Now)
		{
			auto time = std::chrono::system_clock::now().time_since_epoch().count();
			std::mt19937 rand((unsigned int)time);

			return rand();
		}
		else
		{
			std::random_device rd;
			std::mt19937 gen(rd());
			return gen();
		}
	}

	unsigned long long Random_64(RandomType type/* = RandomType::Clock_Now*/)
	{
		// 伪随机生成64位数字
		if (type == RandomType::Clock_Now)
		{
			std::mt19937_64 rand(std::chrono::system_clock::now().time_since_epoch().count());

			return rand();
		}
		else
		{
			std::random_device rd;
			std::mt19937_64 gen(rd());
			return gen();
		}
	}

	std::string guid()
	{
		// https://www.cnblogs.com/0523jy/p/11399578.html
		std::stringstream ss;
		ss.setf(std::ios_base::hex, std::ios_base::basefield);
		for (uint32_t i = 0; i < 8; ++i)
		{
			std::random_device rd;
			std::mt19937 gen(rd());
			std::uniform_int_distribution<uint16_t> dis(4096, std::numeric_limits<uint16_t>::max());
			uint32_t value = dis(gen);

			ss << /*std::hex << */value;
		}
		return ss.str();
	}

	std::string uuid()
	{
		return guid();
	}

}