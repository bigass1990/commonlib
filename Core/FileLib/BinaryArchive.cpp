#include "pch.h"
#include "BinaryArchive.h"

using namespace std;

namespace FileUtils
{

	CBinaryArchive::CBinaryArchive()
		:ss(new std::stringstream(std::ios_base::in | std::ios_base::out | std::ios_base::binary | std::ios_base::trunc))
	{
		DbgView("���� pos:%d", (int)ss->tellg());
	}

	CBinaryArchive::~CBinaryArchive()
	{
	}

	bool CBinaryArchive::ReadBool(bool& v)
	{
		unsigned char b = 0;
		bool rel = ReadUChar(b);
		if (rel)
		{
			v = b;
		}
		return rel;
	}

	bool CBinaryArchive::WriteBool(bool v) const
	{
		return WriteUchar(v);
	}

	bool CBinaryArchive::ReadUChar(unsigned char& v)
	{
		return Read<unsigned char>(v);
	}

	bool CBinaryArchive::WriteUchar(unsigned char v) const
	{
		return Write<unsigned char>(v);
	}

	bool CBinaryArchive::ReadInt8(int8_t& v)
	{
		return Read<int8_t>(v);
	}

	bool CBinaryArchive::WriteInt8(int8_t v) const
	{
		return Write<int8_t>(v);
	}

	bool CBinaryArchive::ReadUInt8(uint8_t& v)
	{
		return Read<uint8_t>(v);
	}

	bool CBinaryArchive::WriteUInt8(uint8_t v) const
	{
		return Write<uint8_t>(v);
	}

	bool CBinaryArchive::ReadInt16(int16_t& v)
	{
		return Read<int16_t>(v);
	}

	bool CBinaryArchive::WriteInt16(int16_t v) const
	{
		return Write<int16_t>(v);
	}

	bool CBinaryArchive::ReadUInt16(uint16_t& v)
	{
		return Read<uint16_t>(v);
	}

	bool CBinaryArchive::WriteUInt16(uint16_t v) const
	{
		return Write<uint16_t>(v);
	}

	bool CBinaryArchive::ReadInt32(int32_t& v)
	{
		return Read<int32_t>(v);
	}

	bool CBinaryArchive::WriteInt32(int32_t v) const
	{
		return Write<int32_t>(v);
	}

	bool CBinaryArchive::ReadUInt32(uint32_t& v)
	{
		return Read<uint32_t>(v);
	}

	bool CBinaryArchive::WriteUInt32(uint32_t v) const
	{
		return Write<uint32_t>(v);
	}

	bool CBinaryArchive::ReadInt64(int64_t& v)
	{
		return Read<int64_t>(v);
	}

	bool CBinaryArchive::WriteInt64(int64_t v) const
	{
		return Write<int64_t>(v);
	}

	bool CBinaryArchive::ReadUInt64(uint64_t& v)
	{
		return Read<uint64_t>(v);
	}

	bool CBinaryArchive::WriteUInt64(uint64_t v) const
	{
		return Write<uint64_t>(v);
	}

	bool CBinaryArchive::ReadChar(char& v)
	{
		return Read<char>(v);
	}

	bool CBinaryArchive::WriteChar(char v) const
	{
		return Write<char>(v);
	}

	bool CBinaryArchive::ReadChar(wchar_t& v)
	{
		return Read<wchar_t>(v);
	}

	bool CBinaryArchive::WriteChar(wchar_t v) const
	{
		return Write<wchar_t>(v);
	}

	bool CBinaryArchive::ReadChar(char* v, size_t& size)
	{
		uint64_t len = 0;
		if (!ReadUInt64(len))
			return false;
		size = (size_t)len;
		if (size > 0)
		{
			ss->read(v, size);
		}
		return true;
	}

	bool CBinaryArchive::WriteChar(const char* v, size_t size) const
	{
		if (!WriteUInt64(size))
			return false;

		if (size > 0)
		{
			ss->write(v, size);
		}
		return true;
	}

	bool CBinaryArchive::ReadChar(wchar_t* v, size_t& size)
	{
		uint64_t len = 0;
		if (!ReadUInt64(len))
			return false;

		if (len > 0)
		{
			ss->read((char*)v, len);
		}

		uint8_t byteSize = sizeof(wchar_t);
		size = (size_t)(len / byteSize);

		return true;
	}

	bool CBinaryArchive::WriteChar(const wchar_t* t, size_t size) const
	{
		uint8_t byteSize = sizeof(wchar_t);
		size_t len = (size_t)(size * byteSize);
		if (!WriteUInt64(len))
			return false;

		if (len > 0)
		{
			ss->write((const char*)t, len);
		}
		return true;
	}

	bool CBinaryArchive::ReadString(std::string& v)
	{
		uint64_t count = 0;
		if (!ReadUInt64(count))
		{
			return false;
		}

		if (count > 0)
		{
			v.resize((size_t)count);
			ss->read(&*v.begin(), count);
		}
		
		return true;
	}

	bool CBinaryArchive::WriteString(const std::string& v) const
	{
		size_t len = v.size();
		if (!WriteUInt64(len))
			return false;

		if (len > 0)
		{
			ss->write(&*v.begin(), len);
		}
		return true;
	}

	bool CBinaryArchive::ReadWString(std::wstring& v)
	{
		uint64_t count = 0;
		if (!ReadUInt64(count))
		{
			return false;
		}
		if (count > 0)
		{
			uint8_t byteSize = sizeof(wchar_t);
			v.resize((size_t)(count / byteSize));
			ss->read((char*)&*v.begin(), count);
		}
		
		return true;
	}

	bool CBinaryArchive::WriteWString(const std::wstring& v) const
	{
		uint8_t byteSize = sizeof(wchar_t);
		size_t len = (size_t)(v.size() * byteSize);
		if (!WriteUInt64(len))
			return false;

		if (len > 0)
		{
			ss->write((const char*)&*v.cbegin(), len);
		}
		return true;
	}

	bool CBinaryArchive::ReadFloat(float& v)
	{
		return Read<float>(v);
	}

	bool CBinaryArchive::WriteFloat(float v) const
	{
		return Write<float>(v);
	}

	bool CBinaryArchive::ReadDouble(double& v)
	{
		return Read<double>(v);
	}

	bool CBinaryArchive::WriteDouble(double v) const
	{
		return Write<double>(v);
	}

	std::string CBinaryArchive::ToString() const
	{
		return ss->str();
	}

	std::stringbuf* CBinaryArchive::ToBuf() const
	{
		auto buf = ss->rdbuf();
		return buf;
	}

	void CBinaryArchive::FromBuf(std::streambuf* buf)
	{
		*ss << buf;
	}

	bool CBinaryArchive::ReadFromFile(const std::string& file)
	{
		std::ifstream fs(file,
			std::ios_base::binary
			| std::ios_base::in
		);
		try
		{
			if (fs.is_open())
			{
				FromBuf(fs.rdbuf());
				fs.close();
				return true;
			}
		}
		catch (const std::exception&)
		{
			if (fs.is_open())
			{
				fs.close();
			}
		}
		return false;
	}

	bool CBinaryArchive::WriteToFile(const std::string& file) const
	{
		std::ofstream fs(file,
			std::ios_base::binary
			| std::ios_base::out
			| std::ios_base::trunc
		);
		try
		{
			fs << ToBuf();
			fs.close();
			return true;
		}
		catch (const std::exception&)
		{
			if (fs.is_open())
			{
				fs.close();
			}
		}
		return false;
	}

}