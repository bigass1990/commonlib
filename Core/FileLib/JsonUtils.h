#pragma once

namespace Json { class Value; }

namespace JsonUtils
{
	// 从文件加载解析json
	FILE_UTILS_API bool Load(const std::string& file, Json::Value* root);
	
	// 从字符串加载解析json
	FILE_UTILS_API bool LoadFromString(const std::string& context, Json::Value* root);

	// 写入json文件
	FILE_UTILS_API bool Save(const std::string& file, const Json::Value* root);

	// 写入json字符串
	FILE_UTILS_API bool SaveToString(std::string& context, const Json::Value* root);
}
