#pragma once

#include "common.h"

#include "StringLib.h"

#include <json/json.h>

#ifndef FILE_UTILS_API
#define	FILE_UTILS_API __declspec(dllexport)
#endif