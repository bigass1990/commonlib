#include "pch.h"
#include "FileUtils.h"
#include "windows.h"

using namespace std;

/*
作者：黄兢成
链接：https://www.zhihu.com/question/52359180/answer/130202972
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
*/

static size_t getFileSize(FILE* file) 
{
	fseek(file, 0, SEEK_END);
	size_t read_len = ftell(file);
	fseek(file, 0, SEEK_SET);
	return read_len;
}

static size_t getFileSize(const char* filePath) 
{
	FILE* file = NULL;
	if (fopen_s(&file, filePath, "rb") != 0 || file == NULL)
	{
		return 0;
	}
	//FILE* file = fopen(filePath, "rb");
// 	if (file == nullptr) 
// 	{
// 		return 0;
// 	}
	size_t size = getFileSize(file);
	fclose(file);
	return size;
}

// 按 https://www.zhihu.com/question/52359180/answer/130202972 所述，该方法读取文件速度较快。
std::vector<unsigned char> readFromFile(const char* filePath)
{
	std::vector<unsigned char> result;

	FILE* file = NULL;
	if (fopen_s(&file, filePath, "rb") != 0 || file == NULL)
	{
		return result;
	}

	//FILE* file = fopen(filePath, "rb");
	
// 	if (file == nullptr)
// 	{
// 		return result;
// 	}

	// 获取文件大小，尽量一次读完
	size_t fileSize = getFileSize(file);
	if (fileSize != 0) 
	{
		result.resize(fileSize);
		size_t n = fread(&result[0], 1, fileSize, file);
		//assert(n <= fileSize);
		if (n != fileSize)
		{
			result.resize(n);
		}
	}

	// 在读取过程当中，有可能文件大小有变化，再尝试读取
	const size_t read_len = 1024;
	char buf[read_len];
	for (;;) {
		size_t n = fread(buf, 1, read_len, file);
		result.insert(result.end(), buf, buf + n);
		if (n < read_len) {
			break;
		}
	}
	fclose(file);
	return result;
}

std::vector<unsigned char> FileUtils::ReadFile(const char* filePath)
{
	return readFromFile(filePath);
}

std::wstring FileUtils::ReadFile_Utf8(const std::string& filePath)
{
	std::wifstream wif(filePath);
	wif.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));
	
	// apply BOM-sensitive UTF-16 facet
	//wif.imbue(std::locale(wif.getloc(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>));

	std::wstringstream wss;
	wss << wif.rdbuf();
	return wss.str();
}

std::wstring FileUtils::ReadFile_Utf8(const std::wstring& filePath)
{
	std::wifstream wif(filePath);
	wif.imbue(std::locale(std::locale::empty(), new std::codecvt_utf8<wchar_t>));

	// apply BOM-sensitive UTF-16 facet
	//wif.imbue(std::locale(wif.getloc(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>));

	std::wstringstream wss;
	wss << wif.rdbuf();
	return wss.str();
}

std::wstring ReadFile_Utf16(const std::wstring& filePath)
{
	std::wifstream wif(filePath);
	// apply BOM-sensitive UTF-16 facet
	wif.imbue(std::locale(wif.getloc(), new std::codecvt_utf16<wchar_t, 0x10ffff, std::consume_header>));

	std::wstringstream wss;
	wss << wif.rdbuf();
	return wss.str();
}

size_t FileUtils::GetFileSize(const std::wstring& file)
{
	return (size_t)FileSystem::file_size(file);

	ifstream gFile(file);

	//获取文件大小
	gFile.seekg(0, ios_base::end);
	std::streampos pos = gFile.tellg();

	gFile.close();

	return (size_t)pos;
}

size_t FileUtils::GetFileSize(const char* file)
{
	if (file == nullptr)
	{
		return 0;
	}
	struct stat buf;
	stat(file, &buf);
	return buf.st_size;
}

size_t FileUtils::ReadFile(const std::wstring& file, std::string& buffer, iosopenmode mode/* = std::ios_base::binary*/)
{
	try
	{
		ifstream gFile(file, mode);

		//获取文件大小
		gFile.seekg(0, ios_base::end);
		std::streampos pos = gFile.tellg();
		gFile.seekg(0, ios_base::beg);

		buffer.reserve((size_t)pos);

		char* buf = &*buffer.begin();
		gFile.read(buf, pos);
		gFile.close();

		return (size_t)pos;
	}
	catch (const std::exception&)
	{
		return 0;
	}
}

Encoding FileUtils::GetFileEncoding(const std::string& file)
{
	std::ifstream ifstr(file, std::ifstream::binary);
	if (ifstr.is_open())
	{
		{
			constexpr unsigned char size = 4;
			unsigned char buf[size]{ 0 };
			ifstr.read((char*)buf, size);
			if (!ifstr.eof())
			{
				return CodeEncoding::BOMCheck(buf, size);
			}
		}

		{
			ifstr.seekg(0, ios_base::beg);
			constexpr unsigned char size = 3;
			unsigned char buf[size]{ 0 };
			ifstr.read((char*)buf, size);
			if (!ifstr.eof())
			{
				return CodeEncoding::BOMCheck(buf, size);
			}
		}

		{
			ifstr.seekg(0, ios_base::beg);
			constexpr unsigned char size = 2;
			unsigned char buf[size]{ 0 };
			ifstr.read((char*)buf, size);
			if (!ifstr.eof())
			{
				return CodeEncoding::BOMCheck(buf, size);
			}
		}
	}

	return Encoding::Undefined;
}

std::string FileUtils::GetRootDir()
{
	char buf[MAX_PATH];
	ZeroMemory(buf, MAX_PATH);

	long length = ::GetModuleFileNameA(NULL, buf, MAX_PATH);
	if (length > 0)
	{
		std::string dir(buf, length);
		size_t index = dir.rfind('\\');
		dir = dir.substr(0, index + 1);

		return dir;
	}
	return "";
}

std::string FileUtils::GetRootDirU8()
{
	return CodeConvert::gbk_to_utf8(GetRootDir());
}

std::wstring FileUtils::GetRootDirW()
{
	wchar_t buf[MAX_PATH];
	ZeroMemory(buf, MAX_PATH);

	long length = ::GetModuleFileNameW(NULL, buf, MAX_PATH);
	if (length > 0)
	{
		std::wstring dir(buf, length);
		size_t index = dir.rfind(L'\\');
		dir = dir.substr(0, index+1);

		return dir;
	}

	return L"";
}

FILE_UTILS_API std::wstring FileUtils::GetTmpDirW()
{
	return FileSystem::temp_directory_path().wstring();

	std::wstring path(L"");

	wchar_t lpPath[MAX_PATH];
	DWORD rel = ::GetTempPathW(MAX_PATH, lpPath);
	if (rel == 0 && rel > MAX_PATH)
		return path;

	path.assign(lpPath);
	return path;
}

FILE_UTILS_API bool FileUtils::IsFileExist(const std::wstring& filePath)
{
	if (filePath.empty())
		return false;

	FileSystem::path path(filePath);
	return FileSystem::exists(path);
}

FILE_UTILS_API bool FileUtils::IsFolderExist(const std::wstring& dirPath)
{
	return IsFileExist(dirPath);
}

FILE_UTILS_API bool FileUtils::IsDirectory(const std::wstring& path)
{
	return FileSystem::is_directory(path);
}
