#pragma once
#include <string>
#include <fstream>

// 文件进度缓冲区类。
template<typename Elem, typename Tr = std::char_traits<Elem>>
class progress_streambuf : public std::basic_filebuf<Elem, Tr>
{
public:
	using base_type = std::basic_filebuf<Elem, Tr>;

	// 不要隐式转换
	explicit progress_streambuf(const std::string& filename)
		:base_type(), count_(0), prev_perc_(0)
	{
		if (open(filename.c_str(), std::ios_base::in | std::ios_base::binary))
		{
			// 得到文件大小
			size_ = static_cast<int>(pubseekoff(0, std::ios_base::end, std::ios_base::in));

			// 文件指针移到开头
			pubseekoff(0, std::ios_base::beg, std::ios_base::in);
		}
	}

protected:
	// 将文件读入缓冲
	virtual int_type uflow() override
	{
		// 读入缓冲
		int_type v = base_type::uflow();

		/* 当前读取的大小
		 * egptr 用于返回当前流缓冲结束指针
		 * gptr  用于流缓冲当前位置
		 **/
		count_ += egptr() - gptr();

		int p = count_ * 100 / size_;

		if (p > prev_perc_)
		{
			std::cout << p << " ";
			prev_perc_ = p;
		}
		return v;
	}


private:
	int count_; // 计数
	int size_; // 文件大小
	int prev_perc_; // 当前进度
};

using progbuf = progress_streambuf<char>;


// 示例
void test()
{
	progbuf pb("c:\\a.txt");
	if (!pb.is_open())
	{
		return;
	}

	std::istream is(&pb);

}