#pragma once
#include <sstream>
#include <list>
#include <map>

#pragma warning(push)
#pragma warning(disable:4251)

// https://www.jianshu.com/p/24b1a7045bea
// https://blog.csdn.net/mvp_Dawn/article/details/110591053
// 判断指定类是否有某个函数。
#define HAS_MEMBER_FUNC(XXX) \
template<typename T, typename... Args>\
struct has_member_func_##XXX \
{ \
private:  \
  template<typename U> static auto Check(int) -> decltype(std::declval<U>().XXX(std::declval<Args>()...), std::true_type());  \
  template<typename U> static std::false_type Check(...); \
public: \
  static constexpr auto value = decltype(Check<T>(0))::value; \
}

// 判断指定类是否有某个成员变量。
#define HAS_MEMEBER_VARIABLE(xxx) \
typename <typename T> \
struct has_member_variable \
{ \
private: \
  template <typename U> static auto Check(int) -> decltype(std::declval<U>().xxx, std::true_type()); \
  template <typename U> static std::false_type Check(...); \
public: \
  static constexpr auto value = decltype(Check<T>(0))::value; \
}

namespace FileUtils
{
	/// <summary>
	/// 二进制编档。
	///	Remark：
	/// 字符串的读写，不涉及编码，统一按照char*缓冲区直接读写。
	/// </summary>
	class FILE_UTILS_API CBinaryArchive
	{
	public:
		CBinaryArchive();
		~CBinaryArchive();

	public:
		bool ReadBool(bool& v);
		bool WriteBool(bool v) const;

		bool ReadUChar(unsigned char& v);
		bool WriteUchar(unsigned char v) const;

		bool ReadInt8(int8_t& v);
		bool WriteInt8(int8_t v) const;

		bool ReadUInt8(uint8_t& v);
		bool WriteUInt8(uint8_t v) const;

		bool ReadInt16(int16_t& v);
		bool WriteInt16(int16_t v) const;

		bool ReadUInt16(uint16_t& v);
		bool WriteUInt16(uint16_t v) const;

		bool ReadInt32(int32_t& v);
		bool WriteInt32(int32_t v) const;

		bool ReadUInt32(uint32_t& v);
		bool WriteUInt32(uint32_t v) const;

		bool ReadInt64(int64_t& v);
		bool WriteInt64(int64_t v) const;

		bool ReadUInt64(uint64_t& v);
		bool WriteUInt64(uint64_t v) const;

		bool ReadChar(char& v);
		bool WriteChar(char v) const;
		bool ReadChar(wchar_t& v);
		bool WriteChar(wchar_t v) const;

		/// <summary>
		/// 读出size个char，放入v指定的内存中。
		/// </summary>
		/// <param name="v">调用者管理v的内存。</param>
		/// <param name="size"></param>
		/// <returns></returns>
		bool ReadChar(char* v, size_t& size);
		bool WriteChar(const char* v, size_t size) const;

		bool ReadChar(wchar_t* v, size_t& size);
		bool WriteChar(const wchar_t* t, size_t size) const;

		/// <summary>
		/// 先读取一个int32类型的size，后读取size个char。
		/// 读取size个char，并存入v中。
		/// </summary>
		/// <param name="v"></param>
		/// <returns></returns>
		bool ReadString(std::string& v);
		bool WriteString(const std::string& v) const;

		bool ReadWString(std::wstring& v);
		bool WriteWString(const std::wstring& v) const;

		bool ReadFloat(float& v);
		bool WriteFloat(float v) const;

		bool ReadDouble(double& v);
		bool WriteDouble(double v) const;

	public:
		/// <summary>
		/// 读写类型T的数组。
		/// </summary>
		/// <typeparam name="T">T如果是非POD类型，应该有ReadData、WriteData方法。</typeparam>
		/// <param name="v">数组首地址</param>
		/// <param name="size">数组大小</param>
		/// <returns></returns>
		template<typename T>
		bool ReadVector(T* v, size_t& size)
		{
			uint64_t len = 0;
			if (!ReadUInt64(len))
				return false;
			size = len;
			for (size_t i = 0; i < len; ++i)
			{
				if (!ReadData<T>(v[i]))
					return false;
			}
			return true;
		}

		template<typename T>
		bool WriteVector(const T* v, size_t size) const
		{
			if (!WriteUInt64(size))
				return false;

			for (size_t i = 0; i < size; ++i)
			{
				if (!WriteData<T>(v[i]))
					return false;
			}
			return true;
		}

		/// <summary>
		/// 读写类型T的二维数组。
		/// 例如：double a[4][4]
		/// ar->WriteVector<double>(*a, 4, 4)
		/// ar->ReadVector<double>(*a, size1, size2)
		/// </summary>
		/// <typeparam name="T">T如果是非POD类型，应该有ReadData、WriteData方法。</typeparam>
		/// <param name="v"></param>
		/// <param name="dim1Size">第一维大小</param>
		/// <param name="dim2Size">第二维大小</param>
		/// <returns></returns>
		template<typename T>
		bool ReadVector(T* v, size_t& dim1Size, size_t& dim2Size)
		{
			uint64_t len1 = 0, len2 = 0;
			if (!ReadUInt64(len1) || !ReadUInt64(len2))
				return false;
			dim1Size = len1;
			dim2Size = len2;
			for (size_t i = 0; i < len1; ++i)
			{
				for (size_t j = 0; j < len2; ++j)
				{
					if (!ReadData<T>(*((v + i) + j)))
						return false;
				}
			}
			return true;
		}

		template<typename T>
		bool WriteVector(const T* v, size_t dim1Size, size_t dim2Size) const
		{
			if (!WriteUInt64(dim1Size)
				|| !WriteUInt64(dim2Size))
				return false;

			for (size_t i = 0; i < dim1Size; ++i)
			{
				for (size_t j = 0; j < dim2Size; ++j)
				{
					if (!WriteData<T>(*((v + i) + j)))
						return false;
				}
			}
			return true;
		}

		/// <summary>
		/// 先读取一个uint64_t的size，后读取size个T。
		/// </summary>
		/// <typeparam name="T">T如果是非POD类型，应该有ReadData、WriteData方法。</typeparam>
		/// <param name="v"></param>
		/// <returns></returns>
		template<typename Vector, typename T = Vector::value_type>
		bool ReadVector(Vector& v)
		{
			uint64_t size = 0;
			if (!ReadUInt64(size))
				return false;

			for (size_t i = 0; i < size; ++i)
			{
				T t = {};
				if (!ReadData<T>(t))
				{
					return false;
				}
				v.push_back(std::move(t));
			}
			return true;
		}

		template<typename Vector, typename T = Vector::value_type>
		bool WriteVector(const Vector& v) const
		{
			if (!WriteUInt64(v.size()))
			{
				return false;
			}

			for (size_t i = 0; i < v.size(); ++i)
			{
				if (!WriteData<T>(v.at(i)))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// 先读取一个uint64_t类型的size，后读取size个T。
		/// </summary>
		/// <typeparam name="T">T如果是非POD类型，应该有ReadData、WriteData方法。</typeparam>
		/// <param name="v"></param>
		/// <returns></returns>
		template<typename List, typename T = List::value_type>
		bool ReadList(List& v)
		{
			uint64_t size = 0;
			if (!ReadUInt64(size))
				return false;

			for (size_t i = 0; i < size; ++i)
			{
				T t = {};
				if (!ReadData<T>(t))
				{
					return false;
				}
				v.push_back(std::move(t));
			}
			return true;
		}

		template<typename List, typename T = List::value_type>
		bool WriteList(const List& v) const
		{
			if (!WriteUInt64(v.size()))
			{
				return false;
			}

			for (uint64_t i = 0; i < v.size(); ++i)
			{
				if (!WriteData<T>(v.at(i)))
				{
					return false;
				}
			}
			return true;
		}

		/// <summary>
		/// 先读取一个uint64_t类型的size，后读取size个T。
		/// </summary>
		/// <typeparam name="T">T如果是非POD类型，应该有ReadData、WriteData方法。</typeparam>
		/// <param name="v"></param>
		/// <returns></returns>
		template<typename Map, typename Key = Map::key_type, typename Value = Map::mapped_type>
		bool ReadMap(Map& v)
		{
			size_t size = 0;
			ReadUInt64(size);
			for (size_t i = 0; i < size; ++i)
			{
				Key tmpK = {};
				ReadData<Key>(tmpK);
				Value tmpV = {};
				ReadData<Value>(tmpV);
				v.insert(std::move(std::make_pair(tmpK, tmpV)));
			}
			return true;
		}

		template<typename Map, typename Key = Map::key_type, typename Value = Map::mapped_type>
		bool WriteMap(const Map& v) const
		{
			WriteUInt64(v.size());
			for (auto& it : v)
			{
				WriteData<Key>(it.first);
				WriteData<Value>(it.second);
			}
			return true;
		}

	public:
		/// <summary>
		/// 扩展读写自定义类型。自定义类型必须实现ReadData、WriteData方法。
		/// bool ReadData(FileUtils::CBinaryArchive* ar);
		/// bool WriteData(const FileUtils::CBinaryArchive* ar) const;
		/// 判断模板类型T是否包含有ReadData、WriteData函数。
		HAS_MEMBER_FUNC(ReadData);
		HAS_MEMBER_FUNC(WriteData);

		template<typename T, typename std::enable_if<has_member_func_ReadData<T, FileUtils::CBinaryArchive*>::value, int>::type n = 0>
		bool ReadData(T& t) { return t.ReadData(this); }

		template<typename T>
		bool ReadData(...)
		{  
			std::string err = str_format("%s can not read!", typeid(T).name());
			DbgView(err);
			throw std::exception(err.data());
		}

		template<typename T, typename std::enable_if<has_member_func_WriteData<T, FileUtils::CBinaryArchive*>::value, int>::type n = 0>
		bool WriteData(const T& t) const { return t.WriteData(this); }

		template<typename T>
		bool WriteData(...) const
		{
			std::string err = str_format("%s can not write!", typeid(T).name()).data();
			DbgView(err);
			throw std::exception(err.data());
		}

	public: // 平凡可复制类型
		template <typename T, typename std::enable_if_t<std::is_trivially_copyable_v<T>
			&& !has_member_func_ReadData<T, FileUtils::CBinaryArchive*>::value, int> n = 3>
		bool ReadData(T& v)
		{
			return Read<T>(v);
		}

		template <typename T, typename std::enable_if_t<std::is_trivially_copyable_v<T>
			&& !has_member_func_WriteData<T, FileUtils::CBinaryArchive*>::value, int> n = 3>
		bool WriteData(const T& v) const
		{
			return Write<T>(v);
		}

	public: // 有get()返回其类型的指针的智能指针。
		template <typename T, typename Value = T::element_type, typename std::enable_if_t< std::is_same_v<T, decltype(*(std::declval<T>().get()))>, int> n = 4>
		bool ReadData(T& v)
		{
			return ReadData<Value>(*(v.get()));
		}

		template <typename T, typename Value = T::element_type, typename std::enable_if_t< std::is_same_v<T, decltype(*(std::declval<T>().get()))>, int> n = 4>
		bool WriteData(const T& v) const
		{
			return WriteData<Value>(*(v.get()));
		}

	public: // std::pair读写。
		template <typename Pair, typename First = Pair::first_type, typename Second = Pair::second_type>
		bool ReadData(Pair& v)
		{
			ReadData<First>(v.first);
			ReadData<Second>(v.second);
			return true;
		}
		template <typename Pair, typename First = Pair::first_type, typename Second = Pair::second_type>
		bool WriteData(const Pair& v) const
		{
			WriteData<First>(v.first);
			WriteData<Second>(v.second);
			return true;
		}

	public: // 判断是否是stl容器。
		HAS_MEMBER_FUNC(resize);

		// 不能用于 map
		template <class T, class Value = T::value_type, typename std::enable_if_t<
			std::is_same_v<typename T::iterator, decltype(std::declval<T>().begin())>
			&& std::is_same_v<typename T::iterator, decltype(std::declval<T>().end())>
			&& has_member_func_resize<T, size_t>::value,
			int> N = 1>
			bool ReadData(T& v)
		{
			uint64_t size = 0;
			this->ReadUInt64(size);
			v.resize(static_cast<size_t>(size));
			//for (auto iter = v.begin(); iter != v.end(); ++iter)
			for (auto& item : v)
			{
				ReadData<Value>(item);
			}
			return true;
		}

		// 不能用于 map
		template <class T, class Value = T::value_type, typename std::enable_if_t<
			std::is_same_v<typename T::iterator, decltype(std::declval<T>().begin())>
			&& std::is_same_v<typename T::iterator, decltype(std::declval<T>().end())>
			&& has_member_func_resize<T, size_t>::value,
			int> N = 1>
			bool WriteData(const T& v) const
		{
			this->WriteUInt64(v.size());
			//for (auto iter = v.begin(); iter != v.end(); ++iter)
			for (auto& item : v)
			{
				WriteData<Value>(item);
			}
			return true;
		}

		// map 读写
		template <typename Map, typename Key = Map::key_type, typename Value = Map::mapped_type,
			typename std::enable_if_t<
			std::is_same_v<typename Map::iterator, decltype(std::declval<Map>().begin())>
			&& std::is_same_v<typename Map::iterator, decltype(std::declval<Map>().end())>,
			int> N = 2>
			bool ReadData(Map& v)
		{
			uint64_t size = 0;
			ReadUInt64(size);
			for (uint64_t i = 0; i < size; ++i)
			{
				Key tmpK;
				ReadData<Key>(tmpK);
				Value tmpV;
				ReadData<Value>(tmpV);
				v.insert(std::move(std::make_pair(tmpK, tmpV)));
			}
			return true;
		}

		// map 读写
		template <typename Map, typename Key = Map::key_type, typename Value = Map::mapped_type,
			typename std::enable_if_t<
			std::is_same_v<typename Map::iterator, decltype(std::declval<Map>().begin())>
			&& std::is_same_v<typename Map::iterator, decltype(std::declval<Map>().end())>,
			int> N = 2>
			bool WriteData(const Map& v) const
		{
			WriteUInt64(v.size());
			for (auto& it : v)
			{
				WriteData<Key>(it.first);
				WriteData<Value>(it.second);
			}
			return true;
		}

	public:

		/// <summary>
		/// 返回字符串。使用std::string作为缓冲区。返回值是复制出来的一份。
		/// </summary>
		/// <returns></returns>
		std::string ToString() const;

		// 缓冲区。一般用于文件读写。
		/* 写文件：
		 *	std::ofstream fs("d:\\a", std::ios_base::binary);
			fs << ac.ToBuf();
			fs.close();
		 * 读文件：
		 *  std::ifstream fs("d:\\a", std::ios_base::binary);
			FileUtils::Archive ac;
			ac.FromBuf(fs.rdbuf());
			fs.close();
		 **/
		std::stringbuf* ToBuf() const;
		void FromBuf(std::streambuf*);

	public:
		// 将文件内容以二进制形式读入std::stringstream
		// file - ansi编码路径。
		bool ReadFromFile(const std::string& file);

		// 将std::stringstream写入文件。
		// file - ansi编码路径。
		bool WriteToFile(const std::string& file) const;

	protected:
		/// <summary>
		/// 读写POD类型。
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="v"></param>
		/// <returns></returns>
		template<typename T, typename std::enable_if_t<std::is_trivially_copyable_v<T>, int> n = 5>
		bool Read(T& v) noexcept
		{
			try
			{
				size_t count = sizeof(T);
				std::unique_ptr<char[]> pc(new char[count]());
				ss->read(pc.get(), count);
				//v = *(T*)(pc.get());
				T* t = (T*)(pc.get());
				v = *t;
				DbgView("read type:%s 字节：%d  pos:%d", typeid(T).name(), count, (int)ss->tellg());
				return true;
			}
			catch (std::exception& ex)
			{
				DbgView("FileUtils.CBinaryArchive.Read exception:%s", ex.what());
				return false;
			}
			catch (...)
			{
				DbgView("FileUtils.CBinaryArchive.Read exception can not catch!");
				return false;
			}
		}

		template<typename T, typename std::enable_if_t<std::is_trivially_copyable_v<T>, int> n = 5>
		bool Write(const T& v) const noexcept
		{
			try
			{
				std::streamsize count = sizeof(T);
				ss->write((const char*)&v, count);
				bool good = ss->good();
				DbgView("writ type:%s 字节：%d  pos:%d  state:%d  good:%d", typeid(T).name(), count, (int)ss->tellg(), ss->rdstate(), (int)good);
				return true;
			}
			catch (std::exception& ex)
			{
				DbgView("FileUtils.CBinaryArchive.Write exception:%s", ex.what());
				return false;
			}
			catch (...)
			{
				DbgView("FileUtils.CBinaryArchive.Read exception can not catch!");
				return false;
			}
		}

	protected:
		std::unique_ptr<std::stringstream> ss;
	};
}

#pragma warning(pop)