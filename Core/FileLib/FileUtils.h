#pragma once

#include <xiosbase>
#include <vector>


using iosopenmode = std::ios_base::openmode;

namespace FileUtils
{
	/// <summary>
	/// 获取文件大小。
	/// </summary>
	/// <param name="file"></param>
	/// <returns></returns>
	FILE_UTILS_API size_t GetFileSize(const std::wstring& file); // 使用c++17的filesystem的file_size获取。
	FILE_UTILS_API size_t GetFileSize(const char* file); // 使用c的stat方法获取，直接得到文件信息，不用将加载到内存。
	

	/// <summary>
	/// 读文件
	/// </summary>
	/// <param name="file"></param>
	/// <param name="buffer">返回文件内容</param>
	/// <returns>返回文件大小</returns>
	FILE_UTILS_API size_t ReadFile(const std::wstring&, std::string& buffer, iosopenmode mode = std::ios_base::binary);

	// 读文件。以fopen，二进制读取。速度较快。
	FILE_UTILS_API std::vector<unsigned char> ReadFile(const char* filePath);

	// 将utf-8编码的文件读入wstring中。
	FILE_UTILS_API std::wstring ReadFile_Utf8(const std::string& filePath);
	FILE_UTILS_API std::wstring ReadFile_Utf8(const std::wstring& filePath);

	// 将utf-16编码的文件读入wstring中。
	FILE_UTILS_API std::wstring ReadFile_Utf16(const std::wstring& filePath);

	/// <summary>
	/// 判断文件编码方式。
	/// </summary>
	/// <param name="file">文件路径</param>
	/// <returns>编码方式</returns>
	//FILE_UTILS_API Encoding GetFileEncoding(const std::wstring& file);
	FILE_UTILS_API Encoding GetFileEncoding(const std::string& file);


	/// <summary>
	/// 获取当前程序运行根目录。
	/// 例如：D:\SDKs\CommonLib\bin\v142\x86\Debug\
	/// </summary>
	/// <returns>返回ANSI编码字符串。失败时，返回空字符串。</returns>
	FILE_UTILS_API std::string GetRootDir();

	/// <summary>
	/// 获取当前程序运行根目录。
	/// 例如：D:\SDKs\CommonLib\bin\v142\x86\Debug\
	/// </summary>
	/// <returns>返回Utf8编码字符串。失败时，返回空字符串。</returns>
	FILE_UTILS_API std::string GetRootDirU8();


	/// <summary>
	/// 获取当前程序运行根目录。
	/// 例如：D:\SDKs\CommonLib\bin\v142\x86\Debug\
	/// </summary>
	/// <returns>返回utf16编码字符串。失败时，返回空字符串。</returns>
	FILE_UTILS_API std::wstring GetRootDirW();

	/// <summary>
	/// 获取系统的临时目录。
	/// </summary>
	/// <returns>返回系统临时目录路径。如果失败，返回空字符串。</returns>
	FILE_UTILS_API std::wstring GetTmpDirW();

	/// <summary>
	/// 判断文件、文件夹是否存在。
	/// </summary>
	/// <param name="path">文件/文件夹路径。</param>
	/// <returns>返回是否存在。</returns>
	FILE_UTILS_API bool IsFileExist(const std::wstring& path);
	FILE_UTILS_API bool IsFolderExist(const std::wstring& path);

	/// <summary>
	/// 判断是否为文件夹。
	/// </summary>
	/// <param name="path"></param>
	/// <returns></returns>
	FILE_UTILS_API bool IsDirectory(const std::wstring& path);
};

template<typename StdStream>
class AutoClosePtr
{
public:
	AutoClosePtr(StdStream&& stream)
		: m_stream(stream)
	{
	}

	~AutoClosePtr()
	{
		m_stream.close();
	}

	StdStream* operator->()
	{
		return &m_stream;
	}

private:
	StdStream& m_stream;
};