#include "pch.h"
#include "JsonUtils.h"


bool JsonUtils::Load(const std::string& file, Json::Value* root)
{
	try
	{
		std::ifstream ifs(file);
		if (!ifs.is_open())
			return false;

		ifs.seekg(0, std::ios::end);
		std::streampos pos = ifs.tellg();
		ifs.seekg(0, std::ios::beg);

		std::string context((size_t)pos, '0');
		ifs.read(&*context.begin(), pos);

		return LoadFromString(context, root);
	}
	catch (std::exception& ex)
	{
		DbgView("JsonUtils::Load-path:%s - exception:%s", file, ex.what());
		return false;
	}
}

bool JsonUtils::LoadFromString(const std::string& context, Json::Value* root)
{
	try
	{
		Json::CharReaderBuilder readerBuilder;
		readerBuilder["collectComments"] = true;
		readerBuilder["allowComments"] = true;
		readerBuilder["allowDroppedNullPlaceholders"] = true;

		JSONCPP_STRING errs;
		const std::unique_ptr<Json::CharReader> jsonReader(readerBuilder.newCharReader());

		return jsonReader->parse(&*context.begin(), &*context.begin() + context.size(), root, &errs);
	}
	catch (const std::exception& ex)
	{
		DbgView("JsonUtils::LoadFromString-exception:%s", ex.what());
		return false;
	}
}

bool JsonUtils::Save(const std::string& file, const Json::Value* root)
{
	try
	{
		Json::StreamWriterBuilder jswBuilder;
		jswBuilder["emitUTF8"] = true; // 直接输出 UTF-8 字符
		jswBuilder["indentation"] = " "; // 压缩格式，没有换行和不必要的空白字符
		jswBuilder["dropNullPlaceholders"] = true;
		std::unique_ptr<Json::StreamWriter>jsWriter(jswBuilder.newStreamWriter());
		std::ostringstream os;
		jsWriter->write(*root, &os);

		std::ofstream ofs((file));
		ofs << os.str();
		ofs.close();

		return true;
	}
	catch (std::exception& ex)
	{
		DbgView("JsonUtils::Save-path:%s - exception:%s", file, ex.what());
		return false;
	}
}

bool JsonUtils::SaveToString(std::string& context, const Json::Value* root)
{
	try
	{
		Json::StreamWriterBuilder jswBuilder;
		jswBuilder["emitUTF8"] = true; // 直接输出 UTF-8 字符
		jswBuilder["indentation"] = ""; // 压缩格式，没有换行和不必要的空白字符
		jswBuilder["dropNullPlaceholders"] = true;
		std::unique_ptr<Json::StreamWriter>jsWriter(jswBuilder.newStreamWriter());
		std::ostringstream os;
		jsWriter->write(*root, &os);

		context = std::move(os.str());
	}
	catch (const std::exception& ex)
	{
		DbgView("JsonUtils::SaveToString-exception:%s", ex.what());
		return false;
	}

	return true;
}